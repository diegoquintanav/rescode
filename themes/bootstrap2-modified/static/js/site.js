/*! For license information please see home.js.LICENSE.txt */
!function(e, t) {
    if ("object" == typeof exports && "object" == typeof module)
        module.exports = t();
    else if ("function" == typeof define && define.amd)
        define([], t);
    else {
        var i = t();
        for (var s in i)
            ("object" == typeof exports ? exports : e)[s] = i[s]
    }
}(this, (() => (() => {
    var e = {
            473: (e, t) => {
                var i,
                    s,
                    n;
                !function(o, a, r) {
                    "use strict";
                    s = [],
                    void 0 === (n = "function" == typeof (i = function() {
                        var e,
                            t = window.document;
                        return (e = function(t, i, s, n) {
                            if (!(this instanceof e))
                                return new e(t);
                            this.cookieTimeout = 33696e6,
                            this.bots = /bot|crawler|spider|crawling/i,
                            this.cookieName = "hasConsent",
                            this.trackingCookiesNames = ["__utma", "__utmb", "__utmc", "__utmt", "__utmv", "__utmz", "_ga", "_gat", "_gid"],
                            this.launchFunction = t,
                            this.waitAccept = i || !1,
                            this.useLocalStorage = s || !1,
                            this.init()
                        }).prototype = {
                            init: function() {
                                var e = this.bots.test(navigator.userAgent),
                                    t = navigator.doNotTrack || navigator.msDoNotTrack || window.doNotTrack;
                                return e || !(null == t || t && "yes" !== t && 1 !== t && "1" !== t) || !1 === this.hasConsent() ? (this.removeBanner(0), !1) : !0 === this.hasConsent() ? (this.launchFunction(), !0) : (this.showBanner(), void (this.waitAccept || this.setConsent(!0)))
                            },
                            showBanner: function() {
                                var e = this,
                                    i = t.getElementById.bind(t),
                                    s = i("cookies-eu-banner"),
                                    n = i("cookies-eu-reject"),
                                    o = i("cookies-eu-accept"),
                                    a = i("cookies-eu-more"),
                                    r = void 0 === s.dataset.waitRemove ? 0 : parseInt(s.dataset.waitRemove),
                                    l = this.addClickListener,
                                    d = e.removeBanner.bind(e, r);
                                s.style.display = "block",
                                a && l(a, (function() {
                                    e.deleteCookie(e.cookieName)
                                })),
                                o && l(o, (function() {
                                    d(),
                                    e.setConsent(!0),
                                    e.launchFunction()
                                })),
                                n && l(n, (function() {
                                    d(),
                                    e.setConsent(!1),
                                    e.trackingCookiesNames.map(e.deleteCookie)
                                }))
                            },
                            setConsent: function(e) {
                                if (this.useLocalStorage)
                                    return localStorage.setItem(this.cookieName, e);
                                this.setCookie(this.cookieName, e)
                            },
                            hasConsent: function() {
                                var e = this.cookieName,
                                    i = function(i) {
                                        return t.cookie.indexOf(e + "=" + i) > -1 || localStorage.getItem(e) === i
                                    };
                                return !!i("true") || !i("false") && null
                            },
                            setCookie: function(e, i) {
                                var s = new Date;
                                s.setTime(s.getTime() + this.cookieTimeout),
                                t.cookie = e + "=" + i + ";expires=" + s.toGMTString() + ";path=/"
                            },
                            deleteCookie: function(e) {
                                var i = t.location.hostname.replace(/^www\./, ""),
                                    s = "; expires=Thu, 01-Jan-1970 00:00:01 GMT; path=/";
                                t.cookie = e + "=; domain=." + i + s,
                                t.cookie = e + "=" + s
                            },
                            addClickListener: function(e, t) {
                                if (e.attachEvent)
                                    return e.attachEvent("onclick", t);
                                e.addEventListener("click", t)
                            },
                            removeBanner: function(e) {
                                setTimeout((function() {
                                    var e = t.getElementById("cookies-eu-banner");
                                    e && e.parentNode && e.parentNode.removeChild(e)
                                }), e)
                            }
                        }, e
                    }) ? i.apply(t, s) : i) || (e.exports = n)
                }(window)
            },
            631: function(e) {
                e.exports = function() {
                    "use strict";
                    function e() {
                        return "undefined" != typeof window
                    }
                    function t(e) {
                        return (o = e) && o.document && function(e) {
                            return 9 === e.nodeType
                        }(o.document) ? (i = (t = e).document, s = i.body, n = i.documentElement, {
                            scrollHeight: function() {
                                return Math.max(s.scrollHeight, n.scrollHeight, s.offsetHeight, n.offsetHeight, s.clientHeight, n.clientHeight)
                            },
                            height: function() {
                                return t.innerHeight || n.clientHeight || s.clientHeight
                            },
                            scrollY: function() {
                                return void 0 !== t.pageYOffset ? t.pageYOffset : (n || s.parentNode || s).scrollTop
                            }
                        }) : function(e) {
                            return {
                                scrollHeight: function() {
                                    return Math.max(e.scrollHeight, e.offsetHeight, e.clientHeight)
                                },
                                height: function() {
                                    return Math.max(e.offsetHeight, e.clientHeight)
                                },
                                scrollY: function() {
                                    return e.scrollTop
                                }
                            }
                        }(e);
                        var t,
                            i,
                            s,
                            n,
                            o
                    }
                    function i(e, i, s) {
                        var n,
                            o = function() {
                                var e = !1;
                                try {
                                    var t = {
                                        get passive() {
                                            e = !0
                                        }
                                    };
                                    window.addEventListener("test", t, t),
                                    window.removeEventListener("test", t, t)
                                } catch (t) {
                                    e = !1
                                }
                                return e
                            }(),
                            a = !1,
                            r = t(e),
                            l = r.scrollY(),
                            d = {};
                        function h() {
                            var e = Math.round(r.scrollY()),
                                t = r.height(),
                                n = r.scrollHeight();
                            d.scrollY = e,
                            d.lastScrollY = l,
                            d.direction = e > l ? "down" : "up",
                            d.distance = Math.abs(e - l),
                            d.isOutOfBounds = e < 0 || e + t > n,
                            d.top = e <= i.offset[d.direction],
                            d.bottom = e + t >= n,
                            d.toleranceExceeded = d.distance > i.tolerance[d.direction],
                            s(d),
                            l = e,
                            a = !1
                        }
                        function c() {
                            a || (a = !0, n = requestAnimationFrame(h))
                        }
                        var p = !!o && {
                            passive: !0,
                            capture: !1
                        };
                        return e.addEventListener("scroll", c, p), h(), {
                            destroy: function() {
                                cancelAnimationFrame(n),
                                e.removeEventListener("scroll", c, p)
                            }
                        }
                    }
                    function s(e) {
                        return e === Object(e) ? e : {
                            down: e,
                            up: e
                        }
                    }
                    function n(e, t) {
                        t = t || {},
                        Object.assign(this, n.options, t),
                        this.classes = Object.assign({}, n.options.classes, t.classes),
                        this.elem = e,
                        this.tolerance = s(this.tolerance),
                        this.offset = s(this.offset),
                        this.initialised = !1,
                        this.frozen = !1
                    }
                    return n.prototype = {
                        constructor: n,
                        init: function() {
                            return n.cutsTheMustard && !this.initialised && (this.addClass("initial"), this.initialised = !0, setTimeout((function(e) {
                                e.scrollTracker = i(e.scroller, {
                                    offset: e.offset,
                                    tolerance: e.tolerance
                                }, e.update.bind(e))
                            }), 100, this)), this
                        },
                        destroy: function() {
                            this.initialised = !1,
                            Object.keys(this.classes).forEach(this.removeClass, this),
                            this.scrollTracker.destroy()
                        },
                        unpin: function() {
                            !this.hasClass("pinned") && this.hasClass("unpinned") || (this.addClass("unpinned"), this.removeClass("pinned"), this.onUnpin && this.onUnpin.call(this))
                        },
                        pin: function() {
                            this.hasClass("unpinned") && (this.addClass("pinned"), this.removeClass("unpinned"), this.onPin && this.onPin.call(this))
                        },
                        freeze: function() {
                            this.frozen = !0,
                            this.addClass("frozen")
                        },
                        unfreeze: function() {
                            this.frozen = !1,
                            this.removeClass("frozen")
                        },
                        top: function() {
                            this.hasClass("top") || (this.addClass("top"), this.removeClass("notTop"), this.onTop && this.onTop.call(this))
                        },
                        notTop: function() {
                            this.hasClass("notTop") || (this.addClass("notTop"), this.removeClass("top"), this.onNotTop && this.onNotTop.call(this))
                        },
                        bottom: function() {
                            this.hasClass("bottom") || (this.addClass("bottom"), this.removeClass("notBottom"), this.onBottom && this.onBottom.call(this))
                        },
                        notBottom: function() {
                            this.hasClass("notBottom") || (this.addClass("notBottom"), this.removeClass("bottom"), this.onNotBottom && this.onNotBottom.call(this))
                        },
                        shouldUnpin: function(e) {
                            return "down" === e.direction && !e.top && e.toleranceExceeded
                        },
                        shouldPin: function(e) {
                            return "up" === e.direction && e.toleranceExceeded || e.top
                        },
                        addClass: function(e) {
                            this.elem.classList.add.apply(this.elem.classList, this.classes[e].split(" "))
                        },
                        removeClass: function(e) {
                            this.elem.classList.remove.apply(this.elem.classList, this.classes[e].split(" "))
                        },
                        hasClass: function(e) {
                            return this.classes[e].split(" ").every((function(e) {
                                return this.classList.contains(e)
                            }), this.elem)
                        },
                        update: function(e) {
                            e.isOutOfBounds || !0 !== this.frozen && (e.top ? this.top() : this.notTop(), e.bottom ? this.bottom() : this.notBottom(), this.shouldUnpin(e) ? this.unpin() : this.shouldPin(e) && this.pin())
                        }
                    }, n.options = {
                        tolerance: {
                            up: 0,
                            down: 0
                        },
                        offset: 0,
                        scroller: e() ? window : null,
                        classes: {
                            frozen: "headroom--frozen",
                            pinned: "headroom--pinned",
                            unpinned: "headroom--unpinned",
                            top: "headroom--top",
                            notTop: "headroom--not-top",
                            bottom: "headroom--bottom",
                            notBottom: "headroom--not-bottom",
                            initial: "headroom"
                        }
                    }, n.cutsTheMustard = !!(e() && function() {}.bind && "classList" in document.documentElement && Object.assign && Object.keys && requestAnimationFrame), n
                }()
            }
        },
        t = {};
    function i(s) {
        var n = t[s];
        if (void 0 !== n)
            return n.exports;
        var o = t[s] = {
            exports: {}
        };
        return e[s].call(o.exports, o, o.exports, i), o.exports
    }
    i.n = e => {
        var t = e && e.__esModule ? () => e.default : () => e;
        return i.d(t, {
            a: t
        }), t
    },
    i.d = (e, t) => {
        for (var s in t)
            i.o(t, s) && !i.o(e, s) && Object.defineProperty(e, s, {
                enumerable: !0,
                get: t[s]
            })
    },
    i.o = (e, t) => Object.prototype.hasOwnProperty.call(e, t),
    i.r = e => {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }),
        Object.defineProperty(e, "__esModule", {
            value: !0
        })
    };
    var s = {};
    return (() => {
        "use strict";
        i.r(s);
        var e = i(631),
            t = i.n(e);
        function n(e, t, i) {
            const s = document.createElement(t || "div");
            return e && (s.className = e), i && i.appendChild(s), s
        }
        function o(e, t) {
            return e.x = t.x, e.y = t.y, void 0 !== t.id && (e.id = t.id), e
        }
        function a(e) {
            e.x = Math.round(e.x),
            e.y = Math.round(e.y)
        }
        function r(e, t) {
            const i = Math.abs(e.x - t.x),
                s = Math.abs(e.y - t.y);
            return Math.sqrt(i * i + s * s)
        }
        function l(e, t) {
            return e.x === t.x && e.y === t.y
        }
        function d(e, t, i) {
            return Math.min(Math.max(e, t), i)
        }
        function h(e, t, i) {
            let s = "translate3d(" + e + "px," + (t || 0) + "px,0)";
            return void 0 !== i && (s += " scale3d(" + i + "," + i + ",1)"), s
        }
        function c(e, t, i, s) {
            e.style.transform = h(t, i, s)
        }
        function p(e, t, i, s) {
            e.style.transition = t ? t + " " + i + "ms " + (s || "cubic-bezier(.4,0,.22,1)") : "none"
        }
        function u(e, t, i) {
            e.style.width = "number" == typeof t ? t + "px" : t,
            e.style.height = "number" == typeof i ? i + "px" : i
        }
        const m = "loading",
            f = "loaded",
            g = "error";
        let v = !1;
        try {
            window.addEventListener("test", null, Object.defineProperty({}, "passive", {
                get: () => {
                    v = !0
                }
            }))
        } catch (e) {}
        class w {
            constructor()
            {
                this._pool = []
            }
            add(e, t, i, s)
            {
                this._toggleListener(e, t, i, s)
            }
            remove(e, t, i, s)
            {
                this._toggleListener(e, t, i, s, !0)
            }
            removeAll()
            {
                this._pool.forEach((e => {
                    this._toggleListener(e.target, e.type, e.listener, e.passive, !0, !0)
                })),
                this._pool = []
            }
            _toggleListener(e, t, i, s, n, o)
            {
                if (!e)
                    return;
                const a = n ? "removeEventListener" : "addEventListener";
                t.split(" ").forEach((t => {
                    if (t) {
                        o || (n ? this._pool = this._pool.filter((s => s.type !== t || s.listener !== i || s.target !== e)) : this._pool.push({
                            target: e,
                            type: t,
                            listener: i,
                            passive: s
                        }));
                        const r = !!v && {
                            passive: s || !1
                        };
                        e[a](t, i, r)
                    }
                }))
            }
        }
        function y(e, t) {
            if (e.getViewportSizeFn) {
                const i = e.getViewportSizeFn(e, t);
                if (i)
                    return i
            }
            return {
                x: document.documentElement.clientWidth,
                y: window.innerHeight
            }
        }
        function _(e, t, i, s, n) {
            let o;
            if (t.paddingFn)
                o = t.paddingFn(i, s, n)[e];
            else if (t.padding)
                o = t.padding[e];
            else {
                const i = "padding" + e[0].toUpperCase() + e.slice(1);
                t[i] && (o = t[i])
            }
            return o || 0
        }
        function b(e, t, i, s) {
            return {
                x: t.x - _("left", e, t, i, s) - _("right", e, t, i, s),
                y: t.y - _("top", e, t, i, s) - _("bottom", e, t, i, s)
            }
        }
        class S {
            constructor(e)
            {
                this.slide = e,
                this.currZoomLevel = 1,
                this.center = {},
                this.max = {},
                this.min = {},
                this.reset()
            }
            update(e)
            {
                this.currZoomLevel = e,
                this.slide.width ? (this._updateAxis("x"), this._updateAxis("y"), this.slide.pswp.dispatch("calcBounds", {
                    slide: this.slide
                })) : this.reset()
            }
            _updateAxis(e)
            {
                const {pswp: t} = this.slide,
                    i = this.slide["x" === e ? "width" : "height"] * this.currZoomLevel,
                    s = _("x" === e ? "left" : "top", t.options, t.viewportSize, this.slide.data, this.slide.index),
                    n = this.slide.panAreaSize[e];
                this.center[e] = Math.round((n - i) / 2) + s,
                this.max[e] = i > n ? Math.round(n - i) + s : this.center[e],
                this.min[e] = i > n ? s : this.center[e]
            }
            reset()
            {
                this.center.x = 0,
                this.center.y = 0,
                this.max.x = 0,
                this.max.y = 0,
                this.min.x = 0,
                this.min.y = 0
            }
            correctPan(e, t)
            {
                return d(t, this.max[e], this.min[e])
            }
        }
        class x {
            constructor(e, t, i, s)
            {
                this.pswp = s,
                this.options = e,
                this.itemData = t,
                this.index = i
            }
            update(e, t, i)
            {
                this.elementSize = {
                    x: e,
                    y: t
                },
                this.panAreaSize = i;
                const s = this.panAreaSize.x / this.elementSize.x,
                    n = this.panAreaSize.y / this.elementSize.y;
                this.fit = Math.min(1, s < n ? s : n),
                this.fill = Math.min(1, s > n ? s : n),
                this.vFill = Math.min(1, n),
                this.initial = this._getInitial(),
                this.secondary = this._getSecondary(),
                this.max = Math.max(this.initial, this.secondary, this._getMax()),
                this.min = Math.min(this.fit, this.initial, this.secondary),
                this.pswp && this.pswp.dispatch("zoomLevelsUpdate", {
                    zoomLevels: this,
                    slideData: this.itemData
                })
            }
            _parseZoomLevelOption(e)
            {
                const t = e + "ZoomLevel",
                    i = this.options[t];
                if (i)
                    return "function" == typeof i ? i(this) : "fill" === i ? this.fill : "fit" === i ? this.fit : Number(i)
            }
            _getSecondary()
            {
                let e = this._parseZoomLevelOption("secondary");
                return e || (e = Math.min(1, 3 * this.fit), e * this.elementSize.x > 4e3 && (e = 4e3 / this.elementSize.x), e)
            }
            _getInitial()
            {
                return this._parseZoomLevelOption("initial") || this.fit
            }
            _getMax()
            {
                return this._parseZoomLevelOption("max") || Math.max(1, 4 * this.fit)
            }
        }
        class T {
            constructor(e, t, i)
            {
                this.data = e,
                this.index = t,
                this.pswp = i,
                this.isActive = t === i.currIndex,
                this.currentResolution = 0,
                this.panAreaSize = {},
                this.isFirstSlide = this.isActive && !i.opener.isOpen,
                this.zoomLevels = new x(i.options, e, t, i),
                this.pswp.dispatch("gettingData", {
                    slide: this,
                    data: this.data,
                    index: t
                }),
                this.pan = {
                    x: 0,
                    y: 0
                },
                this.content = this.pswp.contentLoader.getContentBySlide(this),
                this.container = n("pswp__zoom-wrap"),
                this.currZoomLevel = 1,
                this.width = this.content.width,
                this.height = this.content.height,
                this.bounds = new S(this),
                this.prevDisplayedWidth = -1,
                this.prevDisplayedHeight = -1,
                this.pswp.dispatch("slideInit", {
                    slide: this
                })
            }
            setIsActive(e)
            {
                e && !this.isActive ? this.activate() : !e && this.isActive && this.deactivate()
            }
            append(e)
            {
                this.holderElement = e,
                this.data ? (this.calculateSize(), this.container.style.transformOrigin = "0 0", this.load(), this.appendHeavy(), this.updateContentSize(), this.holderElement.innerHTML = "", this.holderElement.appendChild(this.container), this.zoomAndPanToInitial(), this.pswp.dispatch("firstZoomPan", {
                    slide: this
                }), this.applyCurrentZoomPan(), this.pswp.dispatch("afterSetContent", {
                    slide: this
                }), this.isActive && this.activate()) : this.holderElement.innerHTML = ""
            }
            load()
            {
                this.content.load(),
                this.pswp.dispatch("slideLoad", {
                    slide: this
                })
            }
            appendHeavy()
            {
                const {pswp: e} = this;
                !this.heavyAppended && e.opener.isOpen && !e.mainScroll.isShifted() && (this.isActive, 1) && (this.pswp.dispatch("appendHeavy", {
                    slide: this
                }).defaultPrevented || (this.heavyAppended = !0, this.content.append(), this.pswp.dispatch("appendHeavyContent", {
                    slide: this
                })))
            }
            activate()
            {
                this.isActive = !0,
                this.appendHeavy(),
                this.content.activate(),
                this.pswp.dispatch("slideActivate", {
                    slide: this
                })
            }
            deactivate()
            {
                this.isActive = !1,
                this.content.deactivate(),
                this.currentResolution = 0,
                this.zoomAndPanToInitial(),
                this.applyCurrentZoomPan(),
                this.updateContentSize(),
                this.pswp.dispatch("slideDeactivate", {
                    slide: this
                })
            }
            destroy()
            {
                this.content.hasSlide = !1,
                this.content.remove(),
                this.pswp.dispatch("slideDestroy", {
                    slide: this
                })
            }
            resize()
            {
                this.currZoomLevel !== this.zoomLevels.initial && this.isActive ? (this.calculateSize(), this.bounds.update(this.currZoomLevel), this.panTo(this.pan.x, this.pan.y)) : (this.calculateSize(), this.currentResolution = 0, this.zoomAndPanToInitial(), this.applyCurrentZoomPan(), this.updateContentSize())
            }
            updateContentSize(e)
            {
                const t = this.currentResolution || this.zoomLevels.initial;
                if (!t)
                    return;
                const i = Math.round(this.width * t) || this.pswp.viewportSize.x,
                    s = Math.round(this.height * t) || this.pswp.viewportSize.y;
                (this.sizeChanged(i, s) || e) && this.content.setDisplayedSize(i, s)
            }
            sizeChanged(e, t)
            {
                return (e !== this.prevDisplayedWidth || t !== this.prevDisplayedHeight) && (this.prevDisplayedWidth = e, this.prevDisplayedHeight = t, !0)
            }
            getPlaceholderElement()
            {
                if (this.content.placeholder)
                    return this.content.placeholder.element
            }
            zoomTo(e, t, i, s)
            {
                const {pswp: n} = this;
                if (!this.isZoomable() || n.mainScroll.isShifted())
                    return;
                n.dispatch("beforeZoomTo", {
                    destZoomLevel: e,
                    centerPoint: t,
                    transitionDuration: i
                }),
                n.animations.stopAllPan();
                const o = this.currZoomLevel;
                s || (e = d(e, this.zoomLevels.min, this.zoomLevels.max)),
                this.setZoomLevel(e),
                this.pan.x = this.calculateZoomToPanOffset("x", t, o),
                this.pan.y = this.calculateZoomToPanOffset("y", t, o),
                a(this.pan);
                const r = () => {
                    this._setResolution(e),
                    this.applyCurrentZoomPan()
                };
                i ? n.animations.startTransition({
                    isPan: !0,
                    name: "zoomTo",
                    target: this.container,
                    transform: this.getCurrentTransform(),
                    onComplete: r,
                    duration: i,
                    easing: n.options.easing
                }) : r()
            }
            toggleZoom(e)
            {
                this.zoomTo(this.currZoomLevel === this.zoomLevels.initial ? this.zoomLevels.secondary : this.zoomLevels.initial, e, this.pswp.options.zoomAnimationDuration)
            }
            setZoomLevel(e)
            {
                this.currZoomLevel = e,
                this.bounds.update(this.currZoomLevel)
            }
            calculateZoomToPanOffset(e, t, i)
            {
                if (0 == this.bounds.max[e] - this.bounds.min[e])
                    return this.bounds.center[e];
                t || (t = this.pswp.getViewportCenterPoint());
                const s = this.currZoomLevel / i;
                return this.bounds.correctPan(e, (this.pan[e] - t[e]) * s + t[e])
            }
            panTo(e, t)
            {
                this.pan.x = this.bounds.correctPan("x", e),
                this.pan.y = this.bounds.correctPan("y", t),
                this.applyCurrentZoomPan()
            }
            isPannable()
            {
                return this.width && this.currZoomLevel > this.zoomLevels.fit
            }
            isZoomable()
            {
                return this.width && this.content.isZoomable()
            }
            applyCurrentZoomPan()
            {
                this._applyZoomTransform(this.pan.x, this.pan.y, this.currZoomLevel),
                this === this.pswp.currSlide && this.pswp.dispatch("zoomPanUpdate", {
                    slide: this
                })
            }
            zoomAndPanToInitial()
            {
                this.currZoomLevel = this.zoomLevels.initial,
                this.bounds.update(this.currZoomLevel),
                o(this.pan, this.bounds.center),
                this.pswp.dispatch("initialZoomPan", {
                    slide: this
                })
            }
            _applyZoomTransform(e, t, i)
            {
                i /= this.currentResolution || this.zoomLevels.initial,
                c(this.container, e, t, i)
            }
            calculateSize()
            {
                const {pswp: e} = this;
                o(this.panAreaSize, b(e.options, e.viewportSize, this.data, this.index)),
                this.zoomLevels.update(this.width, this.height, this.panAreaSize),
                e.dispatch("calcSlideSize", {
                    slide: this
                })
            }
            getCurrentTransform()
            {
                const e = this.currZoomLevel / (this.currentResolution || this.zoomLevels.initial);
                return h(this.pan.x, this.pan.y, e)
            }
            _setResolution(e)
            {
                e !== this.currentResolution && (this.currentResolution = e, this.updateContentSize(), this.pswp.dispatch("resolutionChanged"))
            }
        }
        class C {
            constructor(e)
            {
                this.gestures = e,
                this.pswp = e.pswp,
                this.startPan = {}
            }
            start()
            {
                o(this.startPan, this.pswp.currSlide.pan),
                this.pswp.animations.stopAll()
            }
            change()
            {
                const {p1: e, prevP1: t, dragAxis: i, pswp: s} = this.gestures,
                    {currSlide: n} = s;
                if ("y" === i && s.options.closeOnVerticalDrag && n.currZoomLevel <= n.zoomLevels.fit && !this.gestures.isMultitouch) {
                    const i = n.pan.y + (e.y - t.y);
                    if (!s.dispatch("verticalDrag", {
                        panY: i
                    }).defaultPrevented) {
                        this._setPanWithFriction("y", i, .6);
                        const e = 1 - Math.abs(this._getVerticalDragRatio(n.pan.y));
                        s.applyBgOpacity(e),
                        n.applyCurrentZoomPan()
                    }
                } else
                    this._panOrMoveMainScroll("x") || (this._panOrMoveMainScroll("y"), a(n.pan), n.applyCurrentZoomPan())
            }
            end()
            {
                const {pswp: e, velocity: t} = this.gestures,
                    {mainScroll: i} = e;
                let s = 0;
                if (e.animations.stopAll(), i.isShifted()) {
                    const n = (i.x - i.getCurrSlideX()) / e.viewportSize.x;
                    t.x < -.5 && n < 0 || t.x < .1 && n < -.5 ? (s = 1, t.x = Math.min(t.x, 0)) : (t.x > .5 && n > 0 || t.x > -.1 && n > .5) && (s = -1, t.x = Math.max(t.x, 0)),
                    i.moveIndexBy(s, !0, t.x)
                }
                e.currSlide.currZoomLevel > e.currSlide.zoomLevels.max || this.gestures.isMultitouch ? this.gestures.zoomLevels.correctZoomPan(!0) : (this._finishPanGestureForAxis("x"), this._finishPanGestureForAxis("y"))
            }
            _finishPanGestureForAxis(e)
            {
                const {pswp: t} = this,
                    {currSlide: i} = t,
                    {velocity: s} = this.gestures,
                    {pan: n, bounds: o} = i,
                    a = n[e],
                    r = t.bgOpacity < 1 && "y" === e,
                    l = a + .995 * s[e] / (1 - .995);
                if (r) {
                    const e = this._getVerticalDragRatio(a),
                        i = this._getVerticalDragRatio(l);
                    if (e < 0 && i < -.4 || e > 0 && i > .4)
                        return void t.close()
                }
                const h = o.correctPan(e, l);
                if (a === h)
                    return;
                const c = h === l ? 1 : .82,
                    p = t.bgOpacity,
                    u = h - a;
                t.animations.startSpring({
                    name: "panGesture" + e,
                    isPan: !0,
                    start: a,
                    end: h,
                    velocity: s[e],
                    dampingRatio: c,
                    onUpdate: s => {
                        if (r && t.bgOpacity < 1) {
                            const e = 1 - (h - s) / u;
                            t.applyBgOpacity(d(p + (1 - p) * e, 0, 1))
                        }
                        n[e] = Math.floor(s),
                        i.applyCurrentZoomPan()
                    }
                })
            }
            _panOrMoveMainScroll(e)
            {
                const {p1: t, pswp: i, dragAxis: s, prevP1: n, isMultitouch: o} = this.gestures,
                    {currSlide: a, mainScroll: r} = i,
                    l = t[e] - n[e],
                    d = r.x + l;
                if (!l)
                    return;
                if ("x" === e && !a.isPannable() && !o)
                    return r.moveTo(d, !0), !0;
                const {bounds: h} = a,
                    c = a.pan[e] + l;
                if (i.options.allowPanToNext && "x" === s && "x" === e && !o) {
                    const t = r.getCurrSlideX(),
                        i = r.x - t,
                        s = l > 0,
                        n = !s;
                    if (c > h.min[e] && s) {
                        if (h.min[e] <= this.startPan[e])
                            return r.moveTo(d, !0), !0;
                        this._setPanWithFriction(e, c)
                    } else if (c < h.max[e] && n) {
                        if (this.startPan[e] <= h.max[e])
                            return r.moveTo(d, !0), !0;
                        this._setPanWithFriction(e, c)
                    } else if (0 !== i) {
                        if (i > 0)
                            return r.moveTo(Math.max(d, t), !0), !0;
                        if (i < 0)
                            return r.moveTo(Math.min(d, t), !0), !0
                    } else
                        this._setPanWithFriction(e, c)
                } else
                    "y" === e && (r.isShifted() || h.min.y === h.max.y) || this._setPanWithFriction(e, c)
            }
            _getVerticalDragRatio(e)
            {
                return (e - this.pswp.currSlide.bounds.center.y) / (this.pswp.viewportSize.y / 3)
            }
            _setPanWithFriction(e, t, i)
            {
                const {pan: s, bounds: n} = this.pswp.currSlide;
                if (n.correctPan(e, t) !== t || i) {
                    const n = Math.round(t - s[e]);
                    s[e] += n * (i || .35)
                } else
                    s[e] = t
            }
        }
        function P(e, t, i) {
            return e.x = (t.x + i.x) / 2, e.y = (t.y + i.y) / 2, e
        }
        class E {
            constructor(e)
            {
                this.gestures = e,
                this.pswp = this.gestures.pswp,
                this._startPan = {},
                this._startZoomPoint = {},
                this._zoomPoint = {}
            }
            start()
            {
                this._startZoomLevel = this.pswp.currSlide.currZoomLevel,
                o(this._startPan, this.pswp.currSlide.pan),
                this.pswp.animations.stopAllPan(),
                this._wasOverFitZoomLevel = !1
            }
            change()
            {
                const {p1: e, startP1: t, p2: i, startP2: s, pswp: n} = this.gestures,
                    {currSlide: o} = n,
                    a = o.zoomLevels.min,
                    l = o.zoomLevels.max;
                if (!o.isZoomable() || n.mainScroll.isShifted())
                    return;
                P(this._startZoomPoint, t, s),
                P(this._zoomPoint, e, i);
                let d = 1 / r(t, s) * r(e, i) * this._startZoomLevel;
                if (d > o.zoomLevels.initial + o.zoomLevels.initial / 15 && (this._wasOverFitZoomLevel = !0), d < a)
                    if (n.options.pinchToClose && !this._wasOverFitZoomLevel && this._startZoomLevel <= o.zoomLevels.initial) {
                        const e = 1 - (a - d) / (a / 1.2);
                        n.dispatch("pinchClose", {
                            bgOpacity: e
                        }).defaultPrevented || n.applyBgOpacity(e)
                    } else
                        d = a - .15 * (a - d);
                else
                    d > l && (d = l + .05 * (d - l));
                o.pan.x = this._calculatePanForZoomLevel("x", d),
                o.pan.y = this._calculatePanForZoomLevel("y", d),
                o.setZoomLevel(d),
                o.applyCurrentZoomPan()
            }
            end()
            {
                const {pswp: e} = this,
                    {currSlide: t} = e;
                t.currZoomLevel < t.zoomLevels.initial && !this._wasOverFitZoomLevel && e.options.pinchToClose ? e.close() : this.correctZoomPan()
            }
            _calculatePanForZoomLevel(e, t)
            {
                const i = t / this._startZoomLevel;
                return this._zoomPoint[e] - (this._startZoomPoint[e] - this._startPan[e]) * i
            }
            correctZoomPan(e)
            {
                const {pswp: t} = this,
                    {currSlide: i} = t;
                if (!i.isZoomable())
                    return;
                void 0 === this._zoomPoint.x && (e = !0);
                const s = i.currZoomLevel;
                let n,
                    a = !0;
                s < i.zoomLevels.initial ? n = i.zoomLevels.initial : s > i.zoomLevels.max ? n = i.zoomLevels.max : (a = !1, n = s);
                const r = t.bgOpacity,
                    h = t.bgOpacity < 1,
                    c = o({}, i.pan);
                let p = o({}, c);
                e && (this._zoomPoint.x = 0, this._zoomPoint.y = 0, this._startZoomPoint.x = 0, this._startZoomPoint.y = 0, this._startZoomLevel = s, o(this._startPan, c)),
                a && (p = {
                    x: this._calculatePanForZoomLevel("x", n),
                    y: this._calculatePanForZoomLevel("y", n)
                }),
                i.setZoomLevel(n),
                p = {
                    x: i.bounds.correctPan("x", p.x),
                    y: i.bounds.correctPan("y", p.y)
                },
                i.setZoomLevel(s);
                let u = !0;
                if (l(p, c) && (u = !1), !u && !a && !h)
                    return i._setResolution(n), void i.applyCurrentZoomPan();
                t.animations.stopAllPan(),
                t.animations.startSpring({
                    isPan: !0,
                    start: 0,
                    end: 1e3,
                    velocity: 0,
                    dampingRatio: 1,
                    naturalFrequency: 40,
                    onUpdate: e => {
                        if (e /= 1e3, u || a) {
                            if (u && (i.pan.x = c.x + (p.x - c.x) * e, i.pan.y = c.y + (p.y - c.y) * e), a) {
                                const t = s + (n - s) * e;
                                i.setZoomLevel(t)
                            }
                            i.applyCurrentZoomPan()
                        }
                        h && t.bgOpacity < 1 && t.applyBgOpacity(d(r + (1 - r) * e, 0, 1))
                    },
                    onComplete: () => {
                        i._setResolution(n),
                        i.applyCurrentZoomPan()
                    }
                })
            }
        }
        function L(e) {
            return !!e.target.closest(".pswp__container")
        }
        class I {
            constructor(e)
            {
                this.gestures = e
            }
            click(e, t)
            {
                const i = t.target.classList,
                    s = i.contains("pswp__img"),
                    n = i.contains("pswp__item") || i.contains("pswp__zoom-wrap");
                s ? this._doClickOrTapAction("imageClick", e, t) : n && this._doClickOrTapAction("bgClick", e, t)
            }
            tap(e, t)
            {
                L(t) && this._doClickOrTapAction("tap", e, t)
            }
            doubleTap(e, t)
            {
                L(t) && this._doClickOrTapAction("doubleTap", e, t)
            }
            _doClickOrTapAction(e, t, i)
            {
                const {pswp: s} = this.gestures,
                    {currSlide: n} = s,
                    o = e + "Action",
                    a = s.options[o];
                if (!s.dispatch(o, {
                    point: t,
                    originalEvent: i
                }).defaultPrevented)
                    if ("function" != typeof a)
                        switch (a) {
                        case "close":
                        case "next":
                            s[a]();
                            break;
                        case "zoom":
                            n.toggleZoom(t);
                            break;
                        case "zoom-or-close":
                            n.isZoomable() && n.zoomLevels.secondary !== n.zoomLevels.initial ? n.toggleZoom(t) : s.options.clickToCloseNonZoomable && s.close();
                            break;
                        case "toggle-controls":
                            this.gestures.pswp.element.classList.toggle("pswp--ui-visible")
                        }
                    else
                        a.call(s, t, i)
            }
        }
        class z {
            constructor(e)
            {
                this.pswp = e,
                this.dragAxis = void 0,
                this.p1 = {},
                this.p2 = {},
                this.prevP1 = {},
                this.prevP2 = {},
                this.startP1 = {},
                this.startP2 = {},
                this.velocity = {},
                this._lastStartP1 = {},
                this._intervalP1 = {},
                this._numActivePoints = 0,
                this._ongoingPointers = [],
                this._touchEventEnabled = "ontouchstart" in window,
                this._pointerEventEnabled = !!window.PointerEvent,
                this.supportsTouch = this._touchEventEnabled || this._pointerEventEnabled && navigator.maxTouchPoints > 1,
                this.supportsTouch || (e.options.allowPanToNext = !1),
                this.drag = new C(this),
                this.zoomLevels = new E(this),
                this.tapHandler = new I(this),
                e.on("bindEvents", (() => {
                    e.events.add(e.scrollWrap, "click", (e => this._onClick(e))),
                    this._pointerEventEnabled ? this._bindEvents("pointer", "down", "up", "cancel") : this._touchEventEnabled ? (this._bindEvents("touch", "start", "end", "cancel"), e.scrollWrap.ontouchmove = () => {}, e.scrollWrap.ontouchend = () => {}) : this._bindEvents("mouse", "down", "up")
                }))
            }
            _bindEvents(e, t, i, s)
            {
                const {pswp: n} = this,
                    {events: o} = n,
                    a = s ? e + s : "";
                o.add(n.scrollWrap, e + t, this.onPointerDown.bind(this)),
                o.add(window, e + "move", this.onPointerMove.bind(this)),
                o.add(window, e + i, this.onPointerUp.bind(this)),
                a && o.add(n.scrollWrap, a, this.onPointerUp.bind(this))
            }
            onPointerDown(e)
            {
                let t;
                if ("mousedown" !== e.type && "mouse" !== e.pointerType || (t = !0), t && e.button > 0)
                    return;
                const {pswp: i} = this;
                i.opener.isOpen ? i.dispatch("pointerDown", {
                    originalEvent: e
                }).defaultPrevented || (t && (i.mouseDetected(), this._preventPointerEventBehaviour(e)), i.animations.stopAll(), this._updatePoints(e, "down"), this.pointerDown = !0, 1 === this._numActivePoints && (this.dragAxis = null, o(this.startP1, this.p1)), this._numActivePoints > 1 ? (this._clearTapTimer(), this.isMultitouch = !0) : this.isMultitouch = !1) : e.preventDefault()
            }
            onPointerMove(e)
            {
                e.preventDefault(),
                this._numActivePoints && (this._updatePoints(e, "move"), this.pswp.dispatch("pointerMove", {
                    originalEvent: e
                }).defaultPrevented || (1 !== this._numActivePoints || this.isDragging ? this._numActivePoints > 1 && !this.isZooming && (this._finishDrag(), this.isZooming = !0, this._updateStartPoints(), this.zoomLevels.start(), this._rafStopLoop(), this._rafRenderLoop()) : (this.dragAxis || this._calculateDragDirection(), this.dragAxis && !this.isDragging && (this.isZooming && (this.isZooming = !1, this.zoomLevels.end()), this.isDragging = !0, this._clearTapTimer(), this._updateStartPoints(), this._intervalTime = Date.now(), this._velocityCalculated = !1, o(this._intervalP1, this.p1), this.velocity.x = 0, this.velocity.y = 0, this.drag.start(), this._rafStopLoop(), this._rafRenderLoop()))))
            }
            _finishDrag()
            {
                this.isDragging && (this.isDragging = !1, this._velocityCalculated || this._updateVelocity(!0), this.drag.end(), this.dragAxis = null)
            }
            onPointerUp(e)
            {
                this._numActivePoints && (this._updatePoints(e, "up"), this.pswp.dispatch("pointerUp", {
                    originalEvent: e
                }).defaultPrevented || (0 === this._numActivePoints && (this.pointerDown = !1, this._rafStopLoop(), this.isDragging ? this._finishDrag() : this.isZooming || this.isMultitouch || this._finishTap(e)), this._numActivePoints < 2 && this.isZooming && (this.isZooming = !1, this.zoomLevels.end(), 1 === this._numActivePoints && (this.dragAxis = null, this._updateStartPoints()))))
            }
            _rafRenderLoop()
            {
                (this.isDragging || this.isZooming) && (this._updateVelocity(), this.isDragging ? l(this.p1, this.prevP1) || this.drag.change() : l(this.p1, this.prevP1) && l(this.p2, this.prevP2) || this.zoomLevels.change(), this._updatePrevPoints(), this.raf = requestAnimationFrame(this._rafRenderLoop.bind(this)))
            }
            _updateVelocity(e)
            {
                const t = Date.now(),
                    i = t - this._intervalTime;
                i < 50 && !e || (this.velocity.x = this._getVelocity("x", i), this.velocity.y = this._getVelocity("y", i), this._intervalTime = t, o(this._intervalP1, this.p1), this._velocityCalculated = !0)
            }
            _finishTap(e)
            {
                const {mainScroll: t} = this.pswp;
                if (t.isShifted())
                    return void t.moveIndexBy(0, !0);
                if (e.type.indexOf("cancel") > 0)
                    return;
                if ("mouseup" === e.type || "mouse" === e.pointerType)
                    return void this.tapHandler.click(this.startP1, e);
                const i = this.pswp.options.doubleTapAction ? 300 : 0;
                this._tapTimer ? (this._clearTapTimer(), r(this._lastStartP1, this.startP1) < 25 && this.tapHandler.doubleTap(this.startP1, e)) : (o(this._lastStartP1, this.startP1), this._tapTimer = setTimeout((() => {
                    this.tapHandler.tap(this.startP1, e),
                    this._clearTapTimer()
                }), i))
            }
            _clearTapTimer()
            {
                this._tapTimer && (clearTimeout(this._tapTimer), this._tapTimer = null)
            }
            _getVelocity(e, t)
            {
                const i = this.p1[e] - this._intervalP1[e];
                return Math.abs(i) > 1 && t > 5 ? i / t : 0
            }
            _rafStopLoop()
            {
                this.raf && (cancelAnimationFrame(this.raf), this.raf = null)
            }
            _preventPointerEventBehaviour(e)
            {
                return e.preventDefault(), !0
            }
            _updatePoints(e, t)
            {
                if (this._pointerEventEnabled) {
                    const i = e,
                        s = this._ongoingPointers.findIndex((e => e.id === i.pointerId));
                    "up" === t && s > -1 ? this._ongoingPointers.splice(s, 1) : "down" === t && -1 === s ? this._ongoingPointers.push(this._convertEventPosToPoint(i, {})) : s > -1 && this._convertEventPosToPoint(i, this._ongoingPointers[s]),
                    this._numActivePoints = this._ongoingPointers.length,
                    this._numActivePoints > 0 && o(this.p1, this._ongoingPointers[0]),
                    this._numActivePoints > 1 && o(this.p2, this._ongoingPointers[1])
                } else {
                    const i = e;
                    this._numActivePoints = 0,
                    i.type.indexOf("touch") > -1 ? i.touches && i.touches.length > 0 && (this._convertEventPosToPoint(i.touches[0], this.p1), this._numActivePoints++, i.touches.length > 1 && (this._convertEventPosToPoint(i.touches[1], this.p2), this._numActivePoints++)) : (this._convertEventPosToPoint(e, this.p1), "up" === t ? this._numActivePoints = 0 : this._numActivePoints++)
                }
            }
            _updatePrevPoints()
            {
                o(this.prevP1, this.p1),
                o(this.prevP2, this.p2)
            }
            _updateStartPoints()
            {
                o(this.startP1, this.p1),
                o(this.startP2, this.p2),
                this._updatePrevPoints()
            }
            _calculateDragDirection()
            {
                if (this.pswp.mainScroll.isShifted())
                    this.dragAxis = "x";
                else {
                    const e = Math.abs(this.p1.x - this.startP1.x) - Math.abs(this.p1.y - this.startP1.y);
                    if (0 !== e) {
                        const t = e > 0 ? "x" : "y";
                        Math.abs(this.p1[t] - this.startP1[t]) >= 10 && (this.dragAxis = t)
                    }
                }
            }
            _convertEventPosToPoint(e, t)
            {
                return t.x = e.pageX - this.pswp.offset.x, t.y = e.pageY - this.pswp.offset.y, "pointerId" in e ? t.id = e.pointerId : void 0 !== e.identifier && (t.id = e.identifier), t
            }
            _onClick(e)
            {
                this.pswp.mainScroll.isShifted() && (e.preventDefault(), e.stopPropagation())
            }
        }
        class M {
            constructor(e)
            {
                this.pswp = e,
                this.x = 0,
                this.slideWidth = void 0,
                this.itemHolders = void 0,
                this.resetPosition()
            }
            resize(e)
            {
                const {pswp: t} = this,
                    i = Math.round(t.viewportSize.x + t.viewportSize.x * t.options.spacing),
                    s = i !== this.slideWidth;
                s && (this.slideWidth = i, this.moveTo(this.getCurrSlideX())),
                this.itemHolders.forEach(((t, i) => {
                    s && c(t.el, (i + this._containerShiftIndex) * this.slideWidth),
                    e && t.slide && t.slide.resize()
                }))
            }
            resetPosition()
            {
                this._currPositionIndex = 0,
                this._prevPositionIndex = 0,
                this.slideWidth = 0,
                this._containerShiftIndex = -1
            }
            appendHolders()
            {
                this.itemHolders = [];
                for (let e = 0; e < 3; e++) {
                    const t = n("pswp__item", !1, this.pswp.container);
                    t.style.display = 1 === e ? "block" : "none",
                    this.itemHolders.push({
                        el: t
                    })
                }
            }
            canBeSwiped()
            {
                return this.pswp.getNumItems() > 1
            }
            moveIndexBy(e, t, i)
            {
                const {pswp: s} = this;
                let n = s.potentialIndex + e;
                const o = s.getNumItems();
                if (s.canLoop()) {
                    n = s.getLoopedIndex(n);
                    const t = (e + o) % o;
                    e = t <= o / 2 ? t : t - o
                } else
                    n < 0 ? n = 0 : n >= o && (n = o - 1),
                    e = n - s.potentialIndex;
                s.potentialIndex = n,
                this._currPositionIndex -= e,
                s.animations.stopMainScroll();
                const a = this.getCurrSlideX();
                if (t) {
                    s.animations.startSpring({
                        isMainScroll: !0,
                        start: this.x,
                        end: a,
                        velocity: i || 0,
                        naturalFrequency: 30,
                        dampingRatio: 1,
                        onUpdate: e => {
                            this.moveTo(e)
                        },
                        onComplete: () => {
                            this.updateCurrItem(),
                            s.appendHeavy()
                        }
                    });
                    let e = s.potentialIndex - s.currIndex;
                    if (s.canLoop()) {
                        const t = (e + o) % o;
                        e = t <= o / 2 ? t : t - o
                    }
                    Math.abs(e) > 1 && this.updateCurrItem()
                } else
                    this.moveTo(a),
                    this.updateCurrItem();
                if (e)
                    return !0
            }
            getCurrSlideX()
            {
                return this.slideWidth * this._currPositionIndex
            }
            isShifted()
            {
                return this.x !== this.getCurrSlideX()
            }
            updateCurrItem()
            {
                const {pswp: e} = this,
                    t = this._prevPositionIndex - this._currPositionIndex;
                if (!t)
                    return;
                this._prevPositionIndex = this._currPositionIndex,
                e.currIndex = e.potentialIndex;
                let i,
                    s = Math.abs(t);
                s >= 3 && (this._containerShiftIndex += t + (t > 0 ? -3 : 3), s = 3);
                for (let n = 0; n < s; n++)
                    t > 0 ? (i = this.itemHolders.shift(), this.itemHolders[2] = i, this._containerShiftIndex++, c(i.el, (this._containerShiftIndex + 2) * this.slideWidth), e.setContent(i, e.currIndex - s + n + 2)) : (i = this.itemHolders.pop(), this.itemHolders.unshift(i), this._containerShiftIndex--, c(i.el, this._containerShiftIndex * this.slideWidth), e.setContent(i, e.currIndex + s - n - 2));
                Math.abs(this._containerShiftIndex) > 50 && !this.isShifted() && (this.resetPosition(), this.resize()),
                e.animations.stopAllPan(),
                this.itemHolders.forEach(((e, t) => {
                    e.slide && e.slide.setIsActive(1 === t)
                })),
                e.currSlide = this.itemHolders[1].slide,
                e.contentLoader.updateLazy(t),
                e.currSlide.applyCurrentZoomPan(),
                e.dispatch("change")
            }
            moveTo(e, t)
            {
                let i,
                    s;
                !this.pswp.canLoop() && t && (i = (this.slideWidth * this._currPositionIndex - e) / this.slideWidth, i += this.pswp.currIndex, s = Math.round(e - this.x), (i < 0 && s > 0 || i >= this.pswp.getNumItems() - 1 && s < 0) && (e = this.x + .35 * s)),
                this.x = e,
                c(this.pswp.container, e),
                this.pswp.dispatch("moveMainScroll", {
                    x: e,
                    dragging: t
                })
            }
        }
        class A {
            constructor(e)
            {
                this.pswp = e,
                e.on("bindEvents", (() => {
                    e.options.initialPointerPos || this._focusRoot(),
                    e.events.add(document, "focusin", this._onFocusIn.bind(this)),
                    e.events.add(document, "keydown", this._onKeyDown.bind(this))
                }));
                const t = document.activeElement;
                e.on("destroy", (() => {
                    e.options.returnFocus && t && this._wasFocused && t.focus()
                }))
            }
            _focusRoot()
            {
                this._wasFocused || (this.pswp.element.focus(), this._wasFocused = !0)
            }
            _onKeyDown(e)
            {
                const {pswp: t} = this;
                if (t.dispatch("keydown", {
                    originalEvent: e
                }).defaultPrevented)
                    return;
                if (function(e) {
                    if (2 === e.which || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey)
                        return !0
                }(e))
                    return;
                let i,
                    s,
                    n;
                switch (e.keyCode) {
                case 27:
                    t.options.escKey && (i = "close");
                    break;
                case 90:
                    i = "toggleZoom";
                    break;
                case 37:
                    s = "x";
                    break;
                case 38:
                    s = "y";
                    break;
                case 39:
                    s = "x",
                    n = !0;
                    break;
                case 40:
                    n = !0,
                    s = "y";
                    break;
                case 9:
                    this._focusRoot()
                }
                if (s) {
                    e.preventDefault();
                    const {currSlide: o} = t;
                    t.options.arrowKeys && "x" === s && t.getNumItems() > 1 ? i = n ? "next" : "prev" : o && o.currZoomLevel > o.zoomLevels.fit && (o.pan[s] += n ? -80 : 80, o.panTo(o.pan.x, o.pan.y))
                }
                i && (e.preventDefault(), t[i]())
            }
            _onFocusIn(e)
            {
                const {template: t} = this.pswp;
                document === e.target || t === e.target || t.contains(e.target) || t.focus()
            }
        }
        class O {
            constructor(e)
            {
                this.props = e;
                const {target: t, onComplete: i, transform: s, onFinish: n} = e;
                let {duration: o, easing: a} = e;
                this.onFinish = n;
                const r = s ? "transform" : "opacity",
                    l = e[r];
                this._target = t,
                this._onComplete = i,
                o = o || 333,
                a = a || "cubic-bezier(.4,0,.22,1)",
                this._onTransitionEnd = this._onTransitionEnd.bind(this),
                this._firstFrameTimeout = setTimeout((() => {
                    p(t, r, o, a),
                    this._firstFrameTimeout = setTimeout((() => {
                        t.addEventListener("transitionend", this._onTransitionEnd, !1),
                        t.addEventListener("transitioncancel", this._onTransitionEnd, !1),
                        t.style[r] = l
                    }), 30)
                }), 0)
            }
            _onTransitionEnd(e)
            {
                e.target === this._target && this._finalizeAnimation()
            }
            _finalizeAnimation()
            {
                this._finished || (this._finished = !0, this.onFinish(), this._onComplete && this._onComplete())
            }
            destroy()
            {
                this._firstFrameTimeout && clearTimeout(this._firstFrameTimeout),
                p(this._target),
                this._target.removeEventListener("transitionend", this._onTransitionEnd, !1),
                this._target.removeEventListener("transitioncancel", this._onTransitionEnd, !1),
                this._finished || this._finalizeAnimation()
            }
        }
        class k {
            constructor(e, t, i)
            {
                this.velocity = 1e3 * e,
                this._dampingRatio = t || .75,
                this._naturalFrequency = i || 12,
                this._dampingRatio < 1 && (this._dampedFrequency = this._naturalFrequency * Math.sqrt(1 - this._dampingRatio * this._dampingRatio))
            }
            easeFrame(e, t)
            {
                let i,
                    s = 0;
                t /= 1e3;
                const n = Math.E ** (-this._dampingRatio * this._naturalFrequency * t);
                if (1 === this._dampingRatio)
                    i = this.velocity + this._naturalFrequency * e,
                    s = (e + i * t) * n,
                    this.velocity = s * -this._naturalFrequency + i * n;
                else if (this._dampingRatio < 1) {
                    i = 1 / this._dampedFrequency * (this._dampingRatio * this._naturalFrequency * e + this.velocity);
                    const o = Math.cos(this._dampedFrequency * t),
                        a = Math.sin(this._dampedFrequency * t);
                    s = n * (e * o + i * a),
                    this.velocity = s * -this._naturalFrequency * this._dampingRatio + n * (-this._dampedFrequency * e * a + this._dampedFrequency * i * o)
                }
                return s
            }
        }
        class D {
            constructor(e)
            {
                this.props = e;
                const {start: t, end: i, velocity: s, onUpdate: n, onComplete: o, onFinish: a, dampingRatio: r, naturalFrequency: l} = e;
                this.onFinish = a;
                const d = new k(s, r, l);
                let h = Date.now(),
                    c = t - i;
                const p = () => {
                    this._raf && (c = d.easeFrame(c, Date.now() - h), Math.abs(c) < 1 && Math.abs(d.velocity) < 50 ? (n(i), o && o(), this.onFinish()) : (h = Date.now(), n(c + i), this._raf = requestAnimationFrame(p)))
                };
                this._raf = requestAnimationFrame(p)
            }
            destroy()
            {
                this._raf >= 0 && cancelAnimationFrame(this._raf),
                this._raf = null
            }
        }
        class B {
            constructor()
            {
                this.activeAnimations = []
            }
            startSpring(e)
            {
                this._start(e, !0)
            }
            startTransition(e)
            {
                this._start(e)
            }
            _start(e, t)
            {
                let i;
                return i = t ? new D(e) : new O(e), this.activeAnimations.push(i), i.onFinish = () => this.stop(i), i
            }
            stop(e)
            {
                e.destroy();
                const t = this.activeAnimations.indexOf(e);
                t > -1 && this.activeAnimations.splice(t, 1)
            }
            stopAll()
            {
                this.activeAnimations.forEach((e => {
                    e.destroy()
                })),
                this.activeAnimations = []
            }
            stopAllPan()
            {
                this.activeAnimations = this.activeAnimations.filter((e => !e.props.isPan || (e.destroy(), !1)))
            }
            stopMainScroll()
            {
                this.activeAnimations = this.activeAnimations.filter((e => !e.props.isMainScroll || (e.destroy(), !1)))
            }
            isPanRunning()
            {
                return this.activeAnimations.some((e => e.props.isPan))
            }
        }
        class F {
            constructor(e)
            {
                this.pswp = e,
                e.events.add(e.element, "wheel", this._onWheel.bind(this))
            }
            _onWheel(e)
            {
                e.preventDefault();
                const {currSlide: t} = this.pswp;
                let {deltaX: i, deltaY: s} = e;
                if (t && !this.pswp.dispatch("wheel", {
                    originalEvent: e
                }).defaultPrevented)
                    if (e.ctrlKey || this.pswp.options.wheelToZoom) {
                        if (t.isZoomable()) {
                            let i = -s;
                            1 === e.deltaMode ? i *= .05 : i *= e.deltaMode ? 1 : .002,
                            i = 2 ** i;
                            const n = t.currZoomLevel * i;
                            t.zoomTo(n, {
                                x: e.clientX,
                                y: e.clientY
                            })
                        }
                    } else
                        t.isPannable() && (1 === e.deltaMode && (i *= 18, s *= 18), t.panTo(t.pan.x - i, t.pan.y - s))
            }
        }
        class Z {
            constructor(e, t)
            {
                const i = t.name || t.className;
                let s = t.html;
                if (!1 === e.options[i])
                    return;
                "string" == typeof e.options[i + "SVG"] && (s = e.options[i + "SVG"]),
                e.dispatch("uiElementCreate", {
                    data: t
                });
                let o,
                    a = "";
                t.isButton ? (a += "pswp__button ", a += t.className || `pswp__button--${t.name}`) : a += t.className || `pswp__${t.name}`;
                let r = t.isButton ? t.tagName || "button" : t.tagName || "div";
                if (r = r.toLowerCase(), o = n(a, r), t.isButton) {
                    o = n(a, r),
                    "button" === r && (o.type = "button");
                    let {title: s} = t;
                    const {ariaLabel: l} = t;
                    "string" == typeof e.options[i + "Title"] && (s = e.options[i + "Title"]),
                    s && (o.title = s),
                    (l || s) && o.setAttribute("aria-label", l || s)
                }
                o.innerHTML = function(e) {
                    if ("string" == typeof e)
                        return e;
                    if (!e || !e.isCustomSVG)
                        return "";
                    const t = e;
                    let i = '<svg aria-hidden="true" class="pswp__icn" viewBox="0 0 %d %d" width="%d" height="%d">';
                    return i = i.split("%d").join(t.size || 32), t.outlineID && (i += '<use class="pswp__icn-shadow" xlink:href="#' + t.outlineID + '"/>'), i += t.inner, i += "</svg>", i
                }(s),
                t.onInit && t.onInit(o, e),
                t.onClick && (o.onclick = i => {
                    "string" == typeof t.onClick ? e[t.onClick]() : t.onClick(i, o, e)
                });
                const l = t.appendTo || "bar";
                let d;
                "bar" === l ? (e.topBar || (e.topBar = n("pswp__top-bar pswp__hide-on-close", "div", e.scrollWrap)), d = e.topBar) : (o.classList.add("pswp__hide-on-close"), d = "wrapper" === l ? e.scrollWrap : e.element),
                d.appendChild(e.applyFilters("uiElement", o, t))
            }
        }
        function N(e, t, i) {
            e.classList.add("pswp__button--arrow"),
            t.on("change", (() => {
                t.options.loop || (e.disabled = i ? !(t.currIndex < t.getNumItems() - 1) : !(t.currIndex > 0))
            }))
        }
        const H = {
                name: "arrowPrev",
                className: "pswp__button--arrow--prev",
                title: "Previous",
                order: 10,
                isButton: !0,
                appendTo: "wrapper",
                html: {
                    isCustomSVG: !0,
                    size: 60,
                    inner: '<path d="M29 43l-3 3-16-16 16-16 3 3-13 13 13 13z" id="pswp__icn-arrow"/>',
                    outlineID: "pswp__icn-arrow"
                },
                onClick: "prev",
                onInit: N
            },
            G = {
                name: "arrowNext",
                className: "pswp__button--arrow--next",
                title: "Next",
                order: 11,
                isButton: !0,
                appendTo: "wrapper",
                html: {
                    isCustomSVG: !0,
                    size: 60,
                    inner: '<use xlink:href="#pswp__icn-arrow"/>',
                    outlineID: "pswp__icn-arrow"
                },
                onClick: "next",
                onInit: (e, t) => {
                    N(e, t, !0)
                }
            },
            R = {
                name: "close",
                title: "Close",
                order: 20,
                isButton: !0,
                html: {
                    isCustomSVG: !0,
                    inner: '<path d="M24 10l-2-2-6 6-6-6-2 2 6 6-6 6 2 2 6-6 6 6 2-2-6-6z" id="pswp__icn-close"/>',
                    outlineID: "pswp__icn-close"
                },
                onClick: "close"
            },
            V = {
                name: "zoom",
                title: "Zoom",
                order: 10,
                isButton: !0,
                html: {
                    isCustomSVG: !0,
                    inner: '<path d="M17.426 19.926a6 6 0 1 1 1.5-1.5L23 22.5 21.5 24l-4.074-4.074z" id="pswp__icn-zoom"/><path fill="currentColor" class="pswp__zoom-icn-bar-h" d="M11 16v-2h6v2z"/><path fill="currentColor" class="pswp__zoom-icn-bar-v" d="M13 12h2v6h-2z"/>',
                    outlineID: "pswp__icn-zoom"
                },
                onClick: "toggleZoom"
            },
            $ = {
                name: "preloader",
                appendTo: "bar",
                order: 7,
                html: {
                    isCustomSVG: !0,
                    inner: '<path fill-rule="evenodd" clip-rule="evenodd" d="M21.2 16a5.2 5.2 0 1 1-5.2-5.2V8a8 8 0 1 0 8 8h-2.8Z" id="pswp__icn-loading"/>',
                    outlineID: "pswp__icn-loading"
                },
                onInit: (e, t) => {
                    let i,
                        s;
                    const n = t => {
                            var s;
                            i !== t && (i = t, "active", s = t, e.classList[s ? "add" : "remove"]("pswp__preloader--active"))
                        },
                        o = () => {
                            if (!t.currSlide.content.isLoading())
                                return n(!1), void (s && (clearTimeout(s), s = null));
                            s || (s = setTimeout((() => {
                                n(t.currSlide.content.isLoading()),
                                s = null
                            }), t.options.preloaderDelay))
                        };
                    t.on("change", o),
                    t.on("loadComplete", (e => {
                        t.currSlide === e.slide && o()
                    })),
                    t.ui.updatePreloaderVisibility = o
                }
            },
            q = {
                name: "counter",
                order: 5,
                onInit: (e, t) => {
                    t.on("change", (() => {
                        e.innerText = t.currIndex + 1 + t.options.indexIndicatorSep + t.getNumItems()
                    }))
                }
            };
        function W(e, t) {
            e.classList[t ? "add" : "remove"]("pswp--zoomed-in")
        }
        class j {
            constructor(e)
            {
                this.pswp = e,
                this.updatePreloaderVisibility = void 0,
                this._lastUpdatedZoomLevel = void 0
            }
            init()
            {
                const {pswp: e} = this;
                this.isRegistered = !1,
                this.uiElementsData = [R, H, G, V, $, q],
                e.dispatch("uiRegister"),
                this.uiElementsData.sort(((e, t) => (e.order || 0) - (t.order || 0))),
                this.items = [],
                this.isRegistered = !0,
                this.uiElementsData.forEach((e => {
                    this.registerElement(e)
                })),
                e.on("change", (() => {
                    e.element.classList[1 === e.getNumItems() ? "add" : "remove"]("pswp--one-slide")
                })),
                e.on("zoomPanUpdate", (() => this._onZoomPanUpdate()))
            }
            registerElement(e)
            {
                this.isRegistered ? this.items.push(new Z(this.pswp, e)) : this.uiElementsData.push(e)
            }
            _onZoomPanUpdate()
            {
                const {template: e, currSlide: t, options: i} = this.pswp;
                let {currZoomLevel: s} = t;
                if (this.pswp.opener.isClosing)
                    return;
                if (this.pswp.opener.isOpen || (s = t.zoomLevels.initial), s === this._lastUpdatedZoomLevel)
                    return;
                this._lastUpdatedZoomLevel = s;
                const n = t.zoomLevels.initial - t.zoomLevels.secondary;
                if (Math.abs(n) < .01 || !t.isZoomable())
                    return W(e, !1), void e.classList.remove("pswp--zoom-allowed");
                e.classList.add("pswp--zoom-allowed");
                const o = n < 0;
                s === t.zoomLevels.secondary ? W(e, o) : s > t.zoomLevels.secondary ? W(e, !0) : W(e, !1),
                "zoom" !== i.imageClickAction && "zoom-or-close" !== i.imageClickAction || e.classList.add("pswp--click-to-zoom")
            }
        }
        class Y {
            constructor(e, t)
            {
                this.type = e,
                t && Object.assign(this, t)
            }
            preventDefault()
            {
                this.defaultPrevented = !0
            }
        }
        class X {
            constructor(e, t)
            {
                this.element = n("pswp__img pswp__img--placeholder", e ? "img" : "", t),
                e && (this.element.decoding = "async", this.element.alt = "", this.element.src = e, this.element.setAttribute("role", "presentation")),
                this.element.setAttribute("aria-hiden", "true")
            }
            setDisplayedSize(e, t)
            {
                this.element && ("IMG" === this.element.tagName ? (u(this.element, 250, "auto"), this.element.style.transformOrigin = "0 0", this.element.style.transform = h(0, 0, e / 250)) : u(this.element, e, t))
            }
            destroy()
            {
                this.element.parentNode && this.element.remove(),
                this.element = null
            }
        }
        class U {
            constructor(e, t, i)
            {
                this.instance = t,
                this.data = e,
                this.index = i,
                this.element = void 0,
                this.width = Number(this.data.w) || Number(this.data.width) || 0,
                this.height = Number(this.data.h) || Number(this.data.height) || 0,
                this.isAttached = !1,
                this.hasSlide = !1,
                this.state = "idle",
                this.data.type ? this.type = this.data.type : this.data.src ? this.type = "image" : this.type = "html",
                this.instance.dispatch("contentInit", {
                    content: this
                })
            }
            removePlaceholder()
            {
                this.placeholder && !this.keepPlaceholder() && setTimeout((() => {
                    this.placeholder && (this.placeholder.destroy(), this.placeholder = null)
                }), 500)
            }
            load(e, t)
            {
                if (!this.placeholder && this.slide && this.usePlaceholder()) {
                    const e = this.instance.applyFilters("placeholderSrc", !(!this.data.msrc || !this.slide.isFirstSlide) && this.data.msrc, this);
                    this.placeholder = new X(e, this.slide.container)
                }
                this.element && !t || this.instance.dispatch("contentLoad", {
                    content: this,
                    isLazy: e
                }).defaultPrevented || (this.isImageContent() ? this.loadImage(e) : (this.element = n("pswp__content"), this.element.innerHTML = this.data.html || ""), t && this.slide && this.slide.updateContentSize(!0))
            }
            loadImage(e)
            {
                const t = n("pswp__img", "img");
                this.element = t,
                this.instance.dispatch("contentLoadImage", {
                    content: this,
                    isLazy: e
                }).defaultPrevented || (this.data.srcset && (t.srcset = this.data.srcset), t.src = this.data.src, t.alt = this.data.alt || "", this.state = m, t.complete ? this.onLoaded() : (t.onload = () => {
                    this.onLoaded()
                }, t.onerror = () => {
                    this.onError()
                }))
            }
            setSlide(e)
            {
                this.slide = e,
                this.hasSlide = !0,
                this.instance = e.pswp
            }
            onLoaded()
            {
                this.state = f,
                this.slide && (this.instance.dispatch("loadComplete", {
                    slide: this.slide,
                    content: this
                }), this.slide.isActive && this.slide.heavyAppended && !this.element.parentNode && (this.slide.container.innerHTML = "", this.append(), this.slide.updateContentSize(!0)))
            }
            onError()
            {
                this.state = g,
                this.slide && (this.displayError(), this.instance.dispatch("loadComplete", {
                    slide: this.slide,
                    isError: !0,
                    content: this
                }), this.instance.dispatch("loadError", {
                    slide: this.slide,
                    content: this
                }))
            }
            isLoading()
            {
                return this.instance.applyFilters("isContentLoading", this.state === m, this)
            }
            isError()
            {
                return this.state === g
            }
            isImageContent()
            {
                return "image" === this.type
            }
            setDisplayedSize(e, t)
            {
                if (this.element && (this.placeholder && this.placeholder.setDisplayedSize(e, t), !this.instance.dispatch("contentResize", {
                    content: this,
                    width: e,
                    height: t
                }).defaultPrevented && (u(this.element, e, t), this.isImageContent() && !this.isError()))) {
                    const i = this.element;
                    i.srcset && (!i.dataset.largestUsedSize || e > parseInt(i.dataset.largestUsedSize, 10)) && (i.sizes = e + "px", i.dataset.largestUsedSize = String(e)),
                    this.slide && this.instance.dispatch("imageSizeChange", {
                        slide: this.slide,
                        width: e,
                        height: t,
                        content: this
                    })
                }
            }
            isZoomable()
            {
                return this.instance.applyFilters("isContentZoomable", this.isImageContent() && this.state !== g, this)
            }
            usePlaceholder()
            {
                return this.instance.applyFilters("useContentPlaceholder", this.isImageContent(), this)
            }
            lazyLoad()
            {
                this.instance.dispatch("contentLazyLoad", {
                    content: this
                }).defaultPrevented || this.load(!0)
            }
            keepPlaceholder()
            {
                return this.instance.applyFilters("isKeepingPlaceholder", this.isLoading(), this)
            }
            destroy()
            {
                this.hasSlide = !1,
                this.slide = null,
                this.instance.dispatch("contentDestroy", {
                    content: this
                }).defaultPrevented || (this.remove(), this.isImageContent() && this.element && (this.element.onload = null, this.element.onerror = null, this.element = null))
            }
            displayError()
            {
                if (this.slide) {
                    let e = n("pswp__error-msg");
                    e.innerText = this.instance.options.errorMsg,
                    e = this.instance.applyFilters("contentErrorElement", e, this),
                    this.element = n("pswp__content pswp__error-msg-container"),
                    this.element.appendChild(e),
                    this.slide.container.innerHTML = "",
                    this.slide.container.appendChild(this.element),
                    this.slide.updateContentSize(!0),
                    this.removePlaceholder()
                }
            }
            append()
            {
                this.isAttached = !0,
                this.state !== g ? this.instance.dispatch("contentAppend", {
                    content: this
                }).defaultPrevented || (this.isImageContent() ? this.slide && !this.slide.isActive && "decode" in this.element ? (this.isDecoding = !0, requestAnimationFrame((() => {
                    this.element && "IMG" === this.element.tagName && this.element.decode().then((() => {
                        this.isDecoding = !1,
                        requestAnimationFrame((() => {
                            this.appendImage()
                        }))
                    })).catch((() => {
                        this.isDecoding = !1
                    }))
                }))) : (!this.placeholder || this.state !== f && this.state !== g || this.removePlaceholder(), this.appendImage()) : this.element && !this.element.parentNode && this.slide.container.appendChild(this.element)) : this.displayError()
            }
            activate()
            {
                this.instance.dispatch("contentActivate", {
                    content: this
                }).defaultPrevented || this.slide && (this.isImageContent() && this.isDecoding ? this.appendImage() : this.isError() && this.load(!1, !0))
            }
            deactivate()
            {
                this.instance.dispatch("contentDeactivate", {
                    content: this
                })
            }
            remove()
            {
                this.isAttached = !1,
                this.instance.dispatch("contentRemove", {
                    content: this
                }).defaultPrevented || this.element && this.element.parentNode && this.element.remove()
            }
            appendImage()
            {
                this.isAttached && (this.instance.dispatch("contentAppendImage", {
                    content: this
                }).defaultPrevented || this.slide && this.element && !this.element.parentNode && (this.slide.container.appendChild(this.element), !this.placeholder || this.state !== f && this.state !== g || this.removePlaceholder()))
            }
        }
        const K = .003;
        class J {
            constructor(e)
            {
                this.pswp = e,
                this.isClosed = !0,
                this._prepareOpen = this._prepareOpen.bind(this),
                this._thumbBounds = void 0,
                e.on("firstZoomPan", this._prepareOpen)
            }
            open()
            {
                this._prepareOpen(),
                this._start()
            }
            close()
            {
                if (this.isClosed || this.isClosing || this.isOpening)
                    return !1;
                const e = this.pswp.currSlide;
                return this.isOpen = !1, this.isOpening = !1, this.isClosing = !0, this._duration = this.pswp.options.hideAnimationDuration, e && e.currZoomLevel * e.width >= this.pswp.options.maxWidthToAnimate && (this._duration = 0), this._applyStartProps(), setTimeout((() => {
                    this._start()
                }), this._croppedZoom ? 30 : 0), !0
            }
            _prepareOpen()
            {
                if (this.pswp.off("firstZoomPan", this._prepareOpen), !this.isOpening) {
                    const e = this.pswp.currSlide;
                    this.isOpening = !0,
                    this.isClosing = !1,
                    this._duration = this.pswp.options.showAnimationDuration,
                    e && e.zoomLevels.initial * e.width >= this.pswp.options.maxWidthToAnimate && (this._duration = 0),
                    this._applyStartProps()
                }
            }
            _applyStartProps()
            {
                const {pswp: e} = this,
                    t = this.pswp.currSlide,
                    {options: i} = e;
                if ("fade" === i.showHideAnimationType ? (i.showHideOpacity = !0, this._thumbBounds = !1) : "none" === i.showHideAnimationType ? (i.showHideOpacity = !1, this._duration = 0, this._thumbBounds = !1) : this.isOpening && e._initialThumbBounds ? this._thumbBounds = e._initialThumbBounds : this._thumbBounds = this.pswp.getThumbBounds(), this._placeholder = t.getPlaceholderElement(), e.animations.stopAll(), this._useAnimation = this._duration > 50, this._animateZoom = Boolean(this._thumbBounds) && t.content && t.content.usePlaceholder() && (!this.isClosing || !e.mainScroll.isShifted()), this._animateZoom ? this._animateRootOpacity = i.showHideOpacity : (this._animateRootOpacity = !0, this.isOpening && (t.zoomAndPanToInitial(), t.applyCurrentZoomPan())), this._animateBgOpacity = !this._animateRootOpacity && this.pswp.options.bgOpacity > K, this._opacityElement = this._animateRootOpacity ? e.element : e.bg, !this._useAnimation)
                    return this._duration = 0, this._animateZoom = !1, this._animateBgOpacity = !1, this._animateRootOpacity = !0, void (this.isOpening && (e.element.style.opacity = String(K), e.applyBgOpacity(1)));
                this._animateZoom && this._thumbBounds && this._thumbBounds.innerRect ? (this._croppedZoom = !0, this._cropContainer1 = this.pswp.container, this._cropContainer2 = this.pswp.currSlide.holderElement, e.container.style.overflow = "hidden", e.container.style.width = e.viewportSize.x + "px") : this._croppedZoom = !1,
                this.isOpening ? (this._animateRootOpacity ? (e.element.style.opacity = String(K), e.applyBgOpacity(1)) : (this._animateBgOpacity && (e.bg.style.opacity = String(K)), e.element.style.opacity = "1"), this._animateZoom && (this._setClosedStateZoomPan(), this._placeholder && (this._placeholder.style.willChange = "transform", this._placeholder.style.opacity = String(K)))) : this.isClosing && (e.mainScroll.itemHolders[0].el.style.display = "none", e.mainScroll.itemHolders[2].el.style.display = "none", this._croppedZoom && 0 !== e.mainScroll.x && (e.mainScroll.resetPosition(), e.mainScroll.resize()))
            }
            _start()
            {
                this.isOpening && this._useAnimation && this._placeholder && "IMG" === this._placeholder.tagName ? new Promise((e => {
                    let t = !1,
                        i = !0;
                    var s;
                    (s = this._placeholder, "decode" in s ? s.decode() : s.complete ? Promise.resolve(s) : new Promise(((e, t) => {
                        s.onload = () => e(s),
                        s.onerror = t
                    }))).finally((() => {
                        t = !0,
                        i || e()
                    })),
                    setTimeout((() => {
                        i = !1,
                        t && e()
                    }), 50),
                    setTimeout(e, 250)
                })).finally((() => this._initiate())) : this._initiate()
            }
            _initiate()
            {
                this.pswp.element.style.setProperty("--pswp-transition-duration", this._duration + "ms"),
                this.pswp.dispatch(this.isOpening ? "openingAnimationStart" : "closingAnimationStart"),
                this.pswp.dispatch("initialZoom" + (this.isOpening ? "In" : "Out")),
                this.pswp.element.classList[this.isOpening ? "add" : "remove"]("pswp--ui-visible"),
                this.isOpening ? (this._placeholder && (this._placeholder.style.opacity = "1"), this._animateToOpenState()) : this.isClosing && this._animateToClosedState(),
                this._useAnimation || this._onAnimationComplete()
            }
            _onAnimationComplete()
            {
                const {pswp: e} = this;
                this.isOpen = this.isOpening,
                this.isClosed = this.isClosing,
                this.isOpening = !1,
                this.isClosing = !1,
                e.dispatch(this.isOpen ? "openingAnimationEnd" : "closingAnimationEnd"),
                e.dispatch("initialZoom" + (this.isOpen ? "InEnd" : "OutEnd")),
                this.isClosed ? e.destroy() : this.isOpen && (this._animateZoom && (e.container.style.overflow = "visible", e.container.style.width = "100%"), e.currSlide.applyCurrentZoomPan())
            }
            _animateToOpenState()
            {
                const {pswp: e} = this;
                this._animateZoom && (this._croppedZoom && (this._animateTo(this._cropContainer1, "transform", "translate3d(0,0,0)"), this._animateTo(this._cropContainer2, "transform", "none")), e.currSlide.zoomAndPanToInitial(), this._animateTo(e.currSlide.container, "transform", e.currSlide.getCurrentTransform())),
                this._animateBgOpacity && this._animateTo(e.bg, "opacity", String(e.options.bgOpacity)),
                this._animateRootOpacity && this._animateTo(e.element, "opacity", "1")
            }
            _animateToClosedState()
            {
                const {pswp: e} = this;
                this._animateZoom && this._setClosedStateZoomPan(!0),
                this._animateBgOpacity && e.bgOpacity > .01 && this._animateTo(e.bg, "opacity", "0"),
                this._animateRootOpacity && this._animateTo(e.element, "opacity", "0")
            }
            _setClosedStateZoomPan(e)
            {
                if (!this._thumbBounds)
                    return;
                const {pswp: t} = this,
                    {innerRect: i} = this._thumbBounds,
                    {currSlide: s, viewportSize: n} = t;
                if (this._croppedZoom) {
                    const t = -n.x + (this._thumbBounds.x - i.x) + i.w,
                        s = -n.y + (this._thumbBounds.y - i.y) + i.h,
                        o = n.x - i.w,
                        a = n.y - i.h;
                    e ? (this._animateTo(this._cropContainer1, "transform", h(t, s)), this._animateTo(this._cropContainer2, "transform", h(o, a))) : (c(this._cropContainer1, t, s), c(this._cropContainer2, o, a))
                }
                o(s.pan, i || this._thumbBounds),
                s.currZoomLevel = this._thumbBounds.w / s.width,
                e ? this._animateTo(s.container, "transform", s.getCurrentTransform()) : s.applyCurrentZoomPan()
            }
            _animateTo(e, t, i)
            {
                if (!this._duration)
                    return void (e.style[t] = i);
                const {animations: s} = this.pswp,
                    n = {
                        duration: this._duration,
                        easing: this.pswp.options.easing,
                        onComplete: () => {
                            s.activeAnimations.length || this._onAnimationComplete()
                        },
                        target: e
                    };
                n[t] = i,
                s.startTransition(n)
            }
        }
        class Q {
            constructor(e)
            {
                this.pswp = e,
                this.limit = Math.max(e.options.preload[0] + e.options.preload[1] + 1, 5),
                this._cachedItems = []
            }
            updateLazy(e)
            {
                const {pswp: t} = this;
                if (t.dispatch("lazyLoad").defaultPrevented)
                    return;
                const {preload: i} = t.options,
                    s = void 0 === e || e >= 0;
                let n;
                for (n = 0; n <= i[1]; n++)
                    this.loadSlideByIndex(t.currIndex + (s ? n : -n));
                for (n = 1; n <= i[0]; n++)
                    this.loadSlideByIndex(t.currIndex + (s ? -n : n))
            }
            loadSlideByIndex(e)
            {
                e = this.pswp.getLoopedIndex(e);
                let t = this.getContentByIndex(e);
                t || (t = function(e, t) {
                    const i = t.getItemData(e);
                    if (!t.dispatch("lazyLoadSlide", {
                        index: e,
                        itemData: i
                    }).defaultPrevented)
                        return function(e, t, i) {
                            const s = t.createContentFromData(e, i);
                            if (!s || !s.lazyLoad)
                                return;
                            const {options: n} = t,
                                o = b(n, t.viewportSize || y(n, t), e, i),
                                a = new x(n, e, -1);
                            return a.update(s.width, s.height, o), s.lazyLoad(), s.setDisplayedSize(Math.ceil(s.width * a.initial), Math.ceil(s.height * a.initial)), s
                        }(i, t, e)
                }(e, this.pswp), t && this.addToCache(t))
            }
            getContentBySlide(e)
            {
                let t = this.getContentByIndex(e.index);
                return t || (t = this.pswp.createContentFromData(e.data, e.index), t && this.addToCache(t)), t && t.setSlide(e), t
            }
            addToCache(e)
            {
                if (this.removeByIndex(e.index), this._cachedItems.push(e), this._cachedItems.length > this.limit) {
                    const e = this._cachedItems.findIndex((e => !e.isAttached && !e.hasSlide));
                    -1 !== e && this._cachedItems.splice(e, 1)[0].destroy()
                }
            }
            removeByIndex(e)
            {
                const t = this._cachedItems.findIndex((t => t.index === e));
                -1 !== t && this._cachedItems.splice(t, 1)
            }
            getContentByIndex(e)
            {
                return this._cachedItems.find((t => t.index === e))
            }
            destroy()
            {
                this._cachedItems.forEach((e => e.destroy())),
                this._cachedItems = null
            }
        }
        const ee = {
            allowPanToNext: !0,
            spacing: .1,
            loop: !0,
            pinchToClose: !0,
            closeOnVerticalDrag: !0,
            hideAnimationDuration: 333,
            showAnimationDuration: 333,
            zoomAnimationDuration: 333,
            escKey: !0,
            arrowKeys: !0,
            returnFocus: !0,
            maxWidthToAnimate: 4e3,
            clickToCloseNonZoomable: !0,
            imageClickAction: "zoom-or-close",
            bgClickAction: "close",
            tapAction: "toggle-controls",
            doubleTapAction: "zoom",
            indexIndicatorSep: " / ",
            preloaderDelay: 2e3,
            bgOpacity: .8,
            index: 0,
            errorMsg: "The image cannot be loaded",
            preload: [1, 2],
            easing: "cubic-bezier(.4,0,.22,1)"
        };
        class te extends class  extends class {
            constructor()
            {
                this._listeners = {},
                this._filters = {},
                this.pswp = void 0,
                this.options = void 0
            }
            addFilter(e, t, i=100)
            {
                this._filters[e] || (this._filters[e] = []),
                this._filters[e].push({
                    fn: t,
                    priority: i
                }),
                this._filters[e].sort(((e, t) => e.priority - t.priority)),
                this.pswp && this.pswp.addFilter(e, t, i)
            }
            removeFilter(e, t)
            {
                this._filters[e] && (this._filters[e] = this._filters[e].filter((e => e.fn !== t))),
                this.pswp && this.pswp.removeFilter(e, t)
            }
            applyFilters(e, ...t)
            {
                return this._filters[e] && this._filters[e].forEach((e => {
                    t[0] = e.fn.apply(this, t)
                })), t[0]
            }
            on(e, t)
            {
                this._listeners[e] || (this._listeners[e] = []),
                this._listeners[e].push(t),
                this.pswp && this.pswp.on(e, t)
            }
            off(e, t)
            {
                this._listeners[e] && (this._listeners[e] = this._listeners[e].filter((e => t !== e))),
                this.pswp && this.pswp.off(e, t)
            }
            dispatch(e, t)
            {
                if (this.pswp)
                    return this.pswp.dispatch(e, t);
                const i = new Y(e, t);
                return this._listeners ? (this._listeners[e] && this._listeners[e].forEach((e => {
                    e.call(this, i)
                })), i) : i
            }
        }
        {
            getNumItems()
            {
                let e;
                const {dataSource: t} = this.options;
                t ? "length" in t ? e = t.length : "gallery" in t && (t.items || (t.items = this._getGalleryDOMElements(t.gallery)), t.items && (e = t.items.length)) : e = 0;
                const i = this.dispatch("numItems", {
                    dataSource: t,
                    numItems: e
                });
                return this.applyFilters("numItems", i.numItems, t)
            }
            createContentFromData(e, t)
            {
                return new U(e, this, t)
            }
            getItemData(e)
            {
                const {dataSource: t} = this.options;
                let i;
                Array.isArray(t) ? i = t[e] : t && t.gallery && (t.items || (t.items = this._getGalleryDOMElements(t.gallery)), i = t.items[e]);
                let s = i;
                s instanceof Element && (s = this._domElementToItemData(s));
                const n = this.dispatch("itemData", {
                    itemData: s || {},
                    index: e
                });
                return this.applyFilters("itemData", n.itemData, e)
            }
            _getGalleryDOMElements(e)
            {
                return this.options.children || this.options.childSelector ? function(e, t, i=document) {
                    let s = [];
                    if (e instanceof Element)
                        s = [e];
                    else if (e instanceof NodeList || Array.isArray(e))
                        s = Array.from(e);
                    else {
                        const n = "string" == typeof e ? e : t;
                        n && (s = Array.from(i.querySelectorAll(n)))
                    }
                    return s
                }(this.options.children, this.options.childSelector, e) || [] : [e]
            }
            _domElementToItemData(e)
            {
                const t = {
                        element: e
                    },
                    i = "A" === e.tagName ? e : e.querySelector("a");
                if (i) {
                    t.src = i.dataset.pswpSrc || i.href,
                    i.dataset.pswpSrcset && (t.srcset = i.dataset.pswpSrcset),
                    t.width = parseInt(i.dataset.pswpWidth, 10),
                    t.height = parseInt(i.dataset.pswpHeight, 10),
                    t.w = t.width,
                    t.h = t.height,
                    i.dataset.pswpType && (t.type = i.dataset.pswpType);
                    const s = e.querySelector("img");
                    s && (t.msrc = s.currentSrc || s.src, t.alt = s.getAttribute("alt")),
                    (i.dataset.pswpCropped || i.dataset.cropped) && (t.thumbCropped = !0)
                }
                return this.applyFilters("domItemData", t, e, i)
            }
        }
        {
            constructor(e)
            {
                super(),
                this._prepareOptions(e),
                this.offset = {},
                this._prevViewportSize = {},
                this.viewportSize = {},
                this.bgOpacity = 1,
                this.topBar = void 0,
                this.events = new w,
                this.animations = new B,
                this.mainScroll = new M(this),
                this.gestures = new z(this),
                this.opener = new J(this),
                this.keyboard = new A(this),
                this.contentLoader = new Q(this)
            }
            init()
            {
                if (this.isOpen || this.isDestroying)
                    return;
                this.isOpen = !0,
                this.dispatch("init"),
                this.dispatch("beforeOpen"),
                this._createMainStructure();
                let e = "pswp--open";
                return this.gestures.supportsTouch && (e += " pswp--touch"), this.options.mainClass && (e += " " + this.options.mainClass), this.element.className += " " + e, this.currIndex = this.options.index || 0, this.potentialIndex = this.currIndex, this.dispatch("firstUpdate"), this.scrollWheel = new F(this), (Number.isNaN(this.currIndex) || this.currIndex < 0 || this.currIndex >= this.getNumItems()) && (this.currIndex = 0), this.gestures.supportsTouch || this.mouseDetected(), this.updateSize(), this.offset.y = window.pageYOffset, this._initialItemData = this.getItemData(this.currIndex), this.dispatch("gettingData", {
                    index: this.currIndex,
                    data: this._initialItemData,
                    slide: void 0
                }), this._initialThumbBounds = this.getThumbBounds(), this.dispatch("initialLayout"), this.on("openingAnimationEnd", (() => {
                    this.setContent(this.mainScroll.itemHolders[0], this.currIndex - 1),
                    this.setContent(this.mainScroll.itemHolders[2], this.currIndex + 1),
                    this.mainScroll.itemHolders[0].el.style.display = "block",
                    this.mainScroll.itemHolders[2].el.style.display = "block",
                    this.appendHeavy(),
                    this.contentLoader.updateLazy(),
                    this.events.add(window, "resize", this._handlePageResize.bind(this)),
                    this.events.add(window, "scroll", this._updatePageScrollOffset.bind(this)),
                    this.dispatch("bindEvents")
                })), this.setContent(this.mainScroll.itemHolders[1], this.currIndex), this.dispatch("change"), this.opener.open(), this.dispatch("afterInit"), !0
            }
            getLoopedIndex(e)
            {
                const t = this.getNumItems();
                return this.options.loop && (e > t - 1 && (e -= t), e < 0 && (e += t)), d(e, 0, t - 1)
            }
            appendHeavy()
            {
                this.mainScroll.itemHolders.forEach((e => {
                    e.slide && e.slide.appendHeavy()
                }))
            }
            goTo(e)
            {
                this.mainScroll.moveIndexBy(this.getLoopedIndex(e) - this.potentialIndex)
            }
            next()
            {
                this.goTo(this.potentialIndex + 1)
            }
            prev()
            {
                this.goTo(this.potentialIndex - 1)
            }
            zoomTo(...e)
            {
                this.currSlide.zoomTo(...e)
            }
            toggleZoom()
            {
                this.currSlide.toggleZoom()
            }
            close()
            {
                this.opener.isOpen && !this.isDestroying && (this.isDestroying = !0, this.dispatch("close"), this.events.removeAll(), this.opener.close())
            }
            destroy()
            {
                if (!this.isDestroying)
                    return this.options.showHideAnimationType = "none", void this.close();
                this.dispatch("destroy"),
                this.listeners = null,
                this.scrollWrap.ontouchmove = null,
                this.scrollWrap.ontouchend = null,
                this.element.remove(),
                this.mainScroll.itemHolders.forEach((e => {
                    e.slide && e.slide.destroy()
                })),
                this.contentLoader.destroy(),
                this.events.removeAll()
            }
            refreshSlideContent(e)
            {
                this.contentLoader.removeByIndex(e),
                this.mainScroll.itemHolders.forEach(((t, i) => {
                    let s = this.currSlide.index - 1 + i;
                    this.canLoop() && (s = this.getLoopedIndex(s)),
                    s === e && (this.setContent(t, e, !0), 1 === i && (this.currSlide = t.slide, t.slide.setIsActive(!0)))
                })),
                this.dispatch("change")
            }
            setContent(e, t, i)
            {
                if (this.canLoop() && (t = this.getLoopedIndex(t)), e.slide) {
                    if (e.slide.index === t && !i)
                        return;
                    e.slide.destroy(),
                    e.slide = null
                }
                if (!this.canLoop() && (t < 0 || t >= this.getNumItems()))
                    return;
                const s = this.getItemData(t);
                e.slide = new T(s, t, this),
                t === this.currIndex && (this.currSlide = e.slide),
                e.slide.append(e.el)
            }
            getViewportCenterPoint()
            {
                return {
                    x: this.viewportSize.x / 2,
                    y: this.viewportSize.y / 2
                }
            }
            updateSize(e)
            {
                if (this.isDestroying)
                    return;
                const t = y(this.options, this);
                !e && l(t, this._prevViewportSize) || (o(this._prevViewportSize, t), this.dispatch("beforeResize"), o(this.viewportSize, this._prevViewportSize), this._updatePageScrollOffset(), this.dispatch("viewportSize"), this.mainScroll.resize(this.opener.isOpen), !this.hasMouse && window.matchMedia("(any-hover: hover)").matches && this.mouseDetected(), this.dispatch("resize"))
            }
            applyBgOpacity(e)
            {
                this.bgOpacity = Math.max(e, 0),
                this.bg.style.opacity = String(this.bgOpacity * this.options.bgOpacity)
            }
            mouseDetected()
            {
                this.hasMouse || (this.hasMouse = !0, this.element.classList.add("pswp--has_mouse"))
            }
            _handlePageResize()
            {
                this.updateSize(),
                /iPhone|iPad|iPod/i.test(window.navigator.userAgent) && setTimeout((() => {
                    this.updateSize()
                }), 500)
            }
            _updatePageScrollOffset()
            {
                this.setScrollOffset(0, window.pageYOffset)
            }
            setScrollOffset(e, t)
            {
                this.offset.x = e,
                this.offset.y = t,
                this.dispatch("updateScrollOffset")
            }
            _createMainStructure()
            {
                this.element = n("pswp"),
                this.element.setAttribute("tabindex", "-1"),
                this.element.setAttribute("role", "dialog"),
                this.template = this.element,
                this.bg = n("pswp__bg", !1, this.element),
                this.scrollWrap = n("pswp__scroll-wrap", !1, this.element),
                this.container = n("pswp__container", !1, this.scrollWrap),
                this.mainScroll.appendHolders(),
                this.ui = new j(this),
                this.ui.init(),
                (this.options.appendToEl || document.body).appendChild(this.element)
            }
            getThumbBounds()
            {
                return function(e, t, i) {
                    const s = i.dispatch("thumbBounds", {
                        index: e,
                        itemData: t,
                        instance: i
                    });
                    if (s.thumbBounds)
                        return s.thumbBounds;
                    const {element: n} = t;
                    let o,
                        a;
                    if (n && !1 !== i.options.thumbSelector) {
                        const e = i.options.thumbSelector || "img";
                        a = n.matches(e) ? n : n.querySelector(e)
                    }
                    return a = i.applyFilters("thumbEl", a, t, e), a && (o = t.thumbCropped ? function(e, t, i) {
                        const s = e.getBoundingClientRect(),
                            n = s.width / t,
                            o = s.height / i,
                            a = n > o ? n : o,
                            r = (s.width - t * a) / 2,
                            l = (s.height - i * a) / 2,
                            d = {
                                x: s.left + r,
                                y: s.top + l,
                                w: t * a
                            };
                        return d.innerRect = {
                            w: s.width,
                            h: s.height,
                            x: r,
                            y: l
                        }, d
                    }(a, t.width || t.w, t.height || t.h) : function(e) {
                        const t = e.getBoundingClientRect();
                        return {
                            x: t.left,
                            y: t.top,
                            w: t.width
                        }
                    }(a)), i.applyFilters("thumbBounds", o, t, e)
                }(this.currIndex, this.currSlide ? this.currSlide.data : this._initialItemData, this)
            }
            canLoop()
            {
                return this.options.loop && this.getNumItems() > 2
            }
            _prepareOptions(e)
            {
                window.matchMedia("(prefers-reduced-motion), (update: slow)").matches && (e.showHideAnimationType = "none", e.zoomAnimationDuration = 0),
                this.options = {
                    ...ee,
                    ...e
                }
            }
        }
        function ie(e, t, i) {
            const s = document.createElement(t || "div");
            return e && (s.className = e), i && i.appendChild(s), s
        }
        function se(e, t, i) {
            e.style.width = "number" == typeof t ? t + "px" : t,
            e.style.height = "number" == typeof i ? i + "px" : i
        }
        const ne = "loading",
            oe = "loaded",
            ae = "error";
        function re(e, t, i=document) {
            let s = [];
            if (e instanceof Element)
                s = [e];
            else if (e instanceof NodeList || Array.isArray(e))
                s = Array.from(e);
            else {
                const n = "string" == typeof e ? e : t;
                n && (s = Array.from(i.querySelectorAll(n)))
            }
            return s
        }
        class le {
            constructor(e, t)
            {
                this.type = e,
                t && Object.assign(this, t)
            }
            preventDefault()
            {
                this.defaultPrevented = !0
            }
        }
        class de {
            constructor(e, t)
            {
                this.element = ie("pswp__img pswp__img--placeholder", e ? "img" : "", t),
                e && (this.element.decoding = "async", this.element.alt = "", this.element.src = e, this.element.setAttribute("role", "presentation")),
                this.element.setAttribute("aria-hiden", "true")
            }
            setDisplayedSize(e, t)
            {
                this.element && ("IMG" === this.element.tagName ? (se(this.element, 250, "auto"), this.element.style.transformOrigin = "0 0", this.element.style.transform = function(e, t, i) {
                    let s = "translate3d(0px,0px,0)";
                    return void 0 !== i && (s += " scale3d(" + i + "," + i + ",1)"), s
                }(0, 0, e / 250)) : se(this.element, e, t))
            }
            destroy()
            {
                this.element.parentNode && this.element.remove(),
                this.element = null
            }
        }
        class he {
            constructor(e, t, i)
            {
                this.instance = t,
                this.data = e,
                this.index = i,
                this.element = void 0,
                this.width = Number(this.data.w) || Number(this.data.width) || 0,
                this.height = Number(this.data.h) || Number(this.data.height) || 0,
                this.isAttached = !1,
                this.hasSlide = !1,
                this.state = "idle",
                this.data.type ? this.type = this.data.type : this.data.src ? this.type = "image" : this.type = "html",
                this.instance.dispatch("contentInit", {
                    content: this
                })
            }
            removePlaceholder()
            {
                this.placeholder && !this.keepPlaceholder() && setTimeout((() => {
                    this.placeholder && (this.placeholder.destroy(), this.placeholder = null)
                }), 500)
            }
            load(e, t)
            {
                if (!this.placeholder && this.slide && this.usePlaceholder()) {
                    const e = this.instance.applyFilters("placeholderSrc", !(!this.data.msrc || !this.slide.isFirstSlide) && this.data.msrc, this);
                    this.placeholder = new de(e, this.slide.container)
                }
                this.element && !t || this.instance.dispatch("contentLoad", {
                    content: this,
                    isLazy: e
                }).defaultPrevented || (this.isImageContent() ? this.loadImage(e) : (this.element = ie("pswp__content"), this.element.innerHTML = this.data.html || ""), t && this.slide && this.slide.updateContentSize(!0))
            }
            loadImage(e)
            {
                const t = ie("pswp__img", "img");
                this.element = t,
                this.instance.dispatch("contentLoadImage", {
                    content: this,
                    isLazy: e
                }).defaultPrevented || (this.data.srcset && (t.srcset = this.data.srcset), t.src = this.data.src, t.alt = this.data.alt || "", this.state = ne, t.complete ? this.onLoaded() : (t.onload = () => {
                    this.onLoaded()
                }, t.onerror = () => {
                    this.onError()
                }))
            }
            setSlide(e)
            {
                this.slide = e,
                this.hasSlide = !0,
                this.instance = e.pswp
            }
            onLoaded()
            {
                this.state = oe,
                this.slide && (this.instance.dispatch("loadComplete", {
                    slide: this.slide,
                    content: this
                }), this.slide.isActive && this.slide.heavyAppended && !this.element.parentNode && (this.slide.container.innerHTML = "", this.append(), this.slide.updateContentSize(!0)))
            }
            onError()
            {
                this.state = ae,
                this.slide && (this.displayError(), this.instance.dispatch("loadComplete", {
                    slide: this.slide,
                    isError: !0,
                    content: this
                }), this.instance.dispatch("loadError", {
                    slide: this.slide,
                    content: this
                }))
            }
            isLoading()
            {
                return this.instance.applyFilters("isContentLoading", this.state === ne, this)
            }
            isError()
            {
                return this.state === ae
            }
            isImageContent()
            {
                return "image" === this.type
            }
            setDisplayedSize(e, t)
            {
                if (this.element && (this.placeholder && this.placeholder.setDisplayedSize(e, t), !this.instance.dispatch("contentResize", {
                    content: this,
                    width: e,
                    height: t
                }).defaultPrevented && (se(this.element, e, t), this.isImageContent() && !this.isError()))) {
                    const i = this.element;
                    i.srcset && (!i.dataset.largestUsedSize || e > parseInt(i.dataset.largestUsedSize, 10)) && (i.sizes = e + "px", i.dataset.largestUsedSize = String(e)),
                    this.slide && this.instance.dispatch("imageSizeChange", {
                        slide: this.slide,
                        width: e,
                        height: t,
                        content: this
                    })
                }
            }
            isZoomable()
            {
                return this.instance.applyFilters("isContentZoomable", this.isImageContent() && this.state !== ae, this)
            }
            usePlaceholder()
            {
                return this.instance.applyFilters("useContentPlaceholder", this.isImageContent(), this)
            }
            lazyLoad()
            {
                this.instance.dispatch("contentLazyLoad", {
                    content: this
                }).defaultPrevented || this.load(!0)
            }
            keepPlaceholder()
            {
                return this.instance.applyFilters("isKeepingPlaceholder", this.isLoading(), this)
            }
            destroy()
            {
                this.hasSlide = !1,
                this.slide = null,
                this.instance.dispatch("contentDestroy", {
                    content: this
                }).defaultPrevented || (this.remove(), this.isImageContent() && this.element && (this.element.onload = null, this.element.onerror = null, this.element = null))
            }
            displayError()
            {
                if (this.slide) {
                    let e = ie("pswp__error-msg");
                    e.innerText = this.instance.options.errorMsg,
                    e = this.instance.applyFilters("contentErrorElement", e, this),
                    this.element = ie("pswp__content pswp__error-msg-container"),
                    this.element.appendChild(e),
                    this.slide.container.innerHTML = "",
                    this.slide.container.appendChild(this.element),
                    this.slide.updateContentSize(!0),
                    this.removePlaceholder()
                }
            }
            append()
            {
                this.isAttached = !0,
                this.state !== ae ? this.instance.dispatch("contentAppend", {
                    content: this
                }).defaultPrevented || (this.isImageContent() ? this.slide && !this.slide.isActive && "decode" in this.element ? (this.isDecoding = !0, requestAnimationFrame((() => {
                    this.element && "IMG" === this.element.tagName && this.element.decode().then((() => {
                        this.isDecoding = !1,
                        requestAnimationFrame((() => {
                            this.appendImage()
                        }))
                    })).catch((() => {
                        this.isDecoding = !1
                    }))
                }))) : (!this.placeholder || this.state !== oe && this.state !== ae || this.removePlaceholder(), this.appendImage()) : this.element && !this.element.parentNode && this.slide.container.appendChild(this.element)) : this.displayError()
            }
            activate()
            {
                this.instance.dispatch("contentActivate", {
                    content: this
                }).defaultPrevented || this.slide && (this.isImageContent() && this.isDecoding ? this.appendImage() : this.isError() && this.load(!1, !0))
            }
            deactivate()
            {
                this.instance.dispatch("contentDeactivate", {
                    content: this
                })
            }
            remove()
            {
                this.isAttached = !1,
                this.instance.dispatch("contentRemove", {
                    content: this
                }).defaultPrevented || this.element && this.element.parentNode && this.element.remove()
            }
            appendImage()
            {
                this.isAttached && (this.instance.dispatch("contentAppendImage", {
                    content: this
                }).defaultPrevented || this.slide && this.element && !this.element.parentNode && (this.slide.container.appendChild(this.element), !this.placeholder || this.state !== oe && this.state !== ae || this.removePlaceholder()))
            }
        }
        function ce(e, t, i, s, n) {
            let o;
            if (t.paddingFn)
                o = t.paddingFn(i, s, n)[e];
            else if (t.padding)
                o = t.padding[e];
            else {
                const i = "padding" + e[0].toUpperCase() + e.slice(1);
                t[i] && (o = t[i])
            }
            return o || 0
        }
        class pe {
            constructor(e, t, i, s)
            {
                this.pswp = s,
                this.options = e,
                this.itemData = t,
                this.index = i
            }
            update(e, t, i)
            {
                this.elementSize = {
                    x: e,
                    y: t
                },
                this.panAreaSize = i;
                const s = this.panAreaSize.x / this.elementSize.x,
                    n = this.panAreaSize.y / this.elementSize.y;
                this.fit = Math.min(1, s < n ? s : n),
                this.fill = Math.min(1, s > n ? s : n),
                this.vFill = Math.min(1, n),
                this.initial = this._getInitial(),
                this.secondary = this._getSecondary(),
                this.max = Math.max(this.initial, this.secondary, this._getMax()),
                this.min = Math.min(this.fit, this.initial, this.secondary),
                this.pswp && this.pswp.dispatch("zoomLevelsUpdate", {
                    zoomLevels: this,
                    slideData: this.itemData
                })
            }
            _parseZoomLevelOption(e)
            {
                const t = e + "ZoomLevel",
                    i = this.options[t];
                if (i)
                    return "function" == typeof i ? i(this) : "fill" === i ? this.fill : "fit" === i ? this.fit : Number(i)
            }
            _getSecondary()
            {
                let e = this._parseZoomLevelOption("secondary");
                return e || (e = Math.min(1, 3 * this.fit), e * this.elementSize.x > 4e3 && (e = 4e3 / this.elementSize.x), e)
            }
            _getInitial()
            {
                return this._parseZoomLevelOption("initial") || this.fit
            }
            _getMax()
            {
                return this._parseZoomLevelOption("max") || Math.max(1, 4 * this.fit)
            }
        }
        class ue extends class  extends class {
            constructor()
            {
                this._listeners = {},
                this._filters = {},
                this.pswp = void 0,
                this.options = void 0
            }
            addFilter(e, t, i=100)
            {
                this._filters[e] || (this._filters[e] = []),
                this._filters[e].push({
                    fn: t,
                    priority: i
                }),
                this._filters[e].sort(((e, t) => e.priority - t.priority)),
                this.pswp && this.pswp.addFilter(e, t, i)
            }
            removeFilter(e, t)
            {
                this._filters[e] && (this._filters[e] = this._filters[e].filter((e => e.fn !== t))),
                this.pswp && this.pswp.removeFilter(e, t)
            }
            applyFilters(e, ...t)
            {
                return this._filters[e] && this._filters[e].forEach((e => {
                    t[0] = e.fn.apply(this, t)
                })), t[0]
            }
            on(e, t)
            {
                this._listeners[e] || (this._listeners[e] = []),
                this._listeners[e].push(t),
                this.pswp && this.pswp.on(e, t)
            }
            off(e, t)
            {
                this._listeners[e] && (this._listeners[e] = this._listeners[e].filter((e => t !== e))),
                this.pswp && this.pswp.off(e, t)
            }
            dispatch(e, t)
            {
                if (this.pswp)
                    return this.pswp.dispatch(e, t);
                const i = new le(e, t);
                return this._listeners ? (this._listeners[e] && this._listeners[e].forEach((e => {
                    e.call(this, i)
                })), i) : i
            }
        }
        {
            getNumItems()
            {
                let e;
                const {dataSource: t} = this.options;
                t ? "length" in t ? e = t.length : "gallery" in t && (t.items || (t.items = this._getGalleryDOMElements(t.gallery)), t.items && (e = t.items.length)) : e = 0;
                const i = this.dispatch("numItems", {
                    dataSource: t,
                    numItems: e
                });
                return this.applyFilters("numItems", i.numItems, t)
            }
            createContentFromData(e, t)
            {
                return new he(e, this, t)
            }
            getItemData(e)
            {
                const {dataSource: t} = this.options;
                let i;
                Array.isArray(t) ? i = t[e] : t && t.gallery && (t.items || (t.items = this._getGalleryDOMElements(t.gallery)), i = t.items[e]);
                let s = i;
                s instanceof Element && (s = this._domElementToItemData(s));
                const n = this.dispatch("itemData", {
                    itemData: s || {},
                    index: e
                });
                return this.applyFilters("itemData", n.itemData, e)
            }
            _getGalleryDOMElements(e)
            {
                return this.options.children || this.options.childSelector ? re(this.options.children, this.options.childSelector, e) || [] : [e]
            }
            _domElementToItemData(e)
            {
                const t = {
                        element: e
                    },
                    i = "A" === e.tagName ? e : e.querySelector("a");
                if (i) {
                    t.src = i.dataset.pswpSrc || i.href,
                    i.dataset.pswpSrcset && (t.srcset = i.dataset.pswpSrcset),
                    t.width = parseInt(i.dataset.pswpWidth, 10),
                    t.height = parseInt(i.dataset.pswpHeight, 10),
                    t.w = t.width,
                    t.h = t.height,
                    i.dataset.pswpType && (t.type = i.dataset.pswpType);
                    const s = e.querySelector("img");
                    s && (t.msrc = s.currentSrc || s.src, t.alt = s.getAttribute("alt")),
                    (i.dataset.pswpCropped || i.dataset.cropped) && (t.thumbCropped = !0)
                }
                return this.applyFilters("domItemData", t, e, i)
            }
        }
        {
            constructor(e)
            {
                super(),
                this.options = e || {},
                this._uid = 0
            }
            init()
            {
                this.onThumbnailsClick = this.onThumbnailsClick.bind(this),
                re(this.options.gallery, this.options.gallerySelector).forEach((e => {
                    e.addEventListener("click", this.onThumbnailsClick, !1)
                }))
            }
            onThumbnailsClick(e)
            {
                if (function(e) {
                    if (2 === e.which || e.ctrlKey || e.metaKey || e.altKey || e.shiftKey)
                        return !0
                }(e) || window.pswp || !1 === window.navigator.onLine)
                    return;
                let t = {
                    x: e.clientX,
                    y: e.clientY
                };
                t.x || t.y || (t = null);
                let i = this.getClickedIndex(e);
                i = this.applyFilters("clickedIndex", i, e, this);
                const s = {
                    gallery: e.currentTarget
                };
                i >= 0 && (e.preventDefault(), this.loadAndOpen(i, s, t))
            }
            getClickedIndex(e)
            {
                if (this.options.getClickedIndexFn)
                    return this.options.getClickedIndexFn.call(this, e);
                const t = e.target,
                    i = re(this.options.children, this.options.childSelector, e.currentTarget).findIndex((e => e === t || e.contains(t)));
                return -1 !== i ? i : this.options.children || this.options.childSelector ? -1 : 0
            }
            loadAndOpen(e, t, i)
            {
                return !window.pswp && (this.options.index = e, this.options.initialPointerPos = i, this.shouldOpen = !0, this.preload(e, t), !0)
            }
            preload(e, t)
            {
                const {options: i} = this;
                t && (i.dataSource = t);
                const s = [],
                    n = typeof i.pswpModule;
                if ("function" == typeof (o = i.pswpModule) && o.prototype && o.prototype.goTo)
                    s.push(Promise.resolve(i.pswpModule));
                else {
                    if ("string" === n)
                        throw new Error("pswpModule as string is no longer supported");
                    if ("function" !== n)
                        throw new Error("pswpModule is not valid");
                    s.push(i.pswpModule())
                }
                var o;
                "function" == typeof i.openPromise && s.push(i.openPromise()),
                !1 !== i.preloadFirstSlide && e >= 0 && (this._preloadedContent = function(e, t) {
                    const i = t.getItemData(e);
                    if (!t.dispatch("lazyLoadSlide", {
                        index: e,
                        itemData: i
                    }).defaultPrevented)
                        return function(e, t, i) {
                            const s = t.createContentFromData(e, i);
                            if (!s || !s.lazyLoad)
                                return;
                            const {options: n} = t,
                                o = t.viewportSize || function(e, t) {
                                    if (e.getViewportSizeFn) {
                                        const i = e.getViewportSizeFn(e, t);
                                        if (i)
                                            return i
                                    }
                                    return {
                                        x: document.documentElement.clientWidth,
                                        y: window.innerHeight
                                    }
                                }(n, t),
                                a = function(e, t, i, s) {
                                    return {
                                        x: t.x - ce("left", e, t, i, s) - ce("right", e, t, i, s),
                                        y: t.y - ce("top", e, t, i, s) - ce("bottom", e, t, i, s)
                                    }
                                }(n, o, e, i),
                                r = new pe(n, e, -1);
                            return r.update(s.width, s.height, a), s.lazyLoad(), s.setDisplayedSize(Math.ceil(s.width * r.initial), Math.ceil(s.height * r.initial)), s
                        }(i, t, e)
                }(e, this));
                const a = ++this._uid;
                Promise.all(s).then((e => {
                    if (this.shouldOpen) {
                        const t = e[0];
                        this._openPhotoswipe(t, a)
                    }
                }))
            }
            _openPhotoswipe(e, t)
            {
                if (t !== this._uid && this.shouldOpen)
                    return;
                if (this.shouldOpen = !1, window.pswp)
                    return;
                const i = "object" == typeof e ? new e.default(this.options) : new e(this.options);
                this.pswp = i,
                window.pswp = i,
                Object.keys(this._listeners).forEach((e => {
                    this._listeners[e].forEach((t => {
                        i.on(e, t)
                    }))
                })),
                Object.keys(this._filters).forEach((e => {
                    this._filters[e].forEach((t => {
                        i.addFilter(e, t.fn, t.priority)
                    }))
                })),
                this._preloadedContent && (i.contentLoader.addToCache(this._preloadedContent), this._preloadedContent = null),
                i.on("destroy", (() => {
                    this.pswp = null,
                    window.pswp = null
                })),
                i.init()
            }
            destroy()
            {
                this.pswp && this.pswp.destroy(),
                this.shouldOpen = !1,
                this._listeners = null,
                re(this.options.gallery, this.options.gallerySelector).forEach((e => {
                    e.removeEventListener("click", this.onThumbnailsClick, !1)
                }))
            }
        }
        var me = i(473),
            fe = i.n(me);
        function ge(e) {
            return null !== e && "object" == typeof e && "constructor" in e && e.constructor === Object
        }
        function ve(e={}, t={}) {
            Object.keys(t).forEach((i => {
                void 0 === e[i] ? e[i] = t[i] : ge(t[i]) && ge(e[i]) && Object.keys(t[i]).length > 0 && ve(e[i], t[i])
            }))
        }
        const we = {
            body: {},
            addEventListener() {},
            removeEventListener() {},
            activeElement: {
                blur() {},
                nodeName: ""
            },
            querySelector: () => null,
            querySelectorAll: () => [],
            getElementById: () => null,
            createEvent: () => ({
                initEvent() {}
            }),
            createElement: () => ({
                children: [],
                childNodes: [],
                style: {},
                setAttribute() {},
                getElementsByTagName: () => []
            }),
            createElementNS: () => ({}),
            importNode: () => null,
            location: {
                hash: "",
                host: "",
                hostname: "",
                href: "",
                origin: "",
                pathname: "",
                protocol: "",
                search: ""
            }
        };
        function ye() {
            const e = "undefined" != typeof document ? document : {};
            return ve(e, we), e
        }
        const _e = {
            document: we,
            navigator: {
                userAgent: ""
            },
            location: {
                hash: "",
                host: "",
                hostname: "",
                href: "",
                origin: "",
                pathname: "",
                protocol: "",
                search: ""
            },
            history: {
                replaceState() {},
                pushState() {},
                go() {},
                back() {}
            },
            CustomEvent: function() {
                return this
            },
            addEventListener() {},
            removeEventListener() {},
            getComputedStyle: () => ({
                getPropertyValue: () => ""
            }),
            Image() {},
            Date() {},
            screen: {},
            setTimeout() {},
            clearTimeout() {},
            matchMedia: () => ({}),
            requestAnimationFrame: e => "undefined" == typeof setTimeout ? (e(), null) : setTimeout(e, 0),
            cancelAnimationFrame(e) {
                "undefined" != typeof setTimeout && clearTimeout(e)
            }
        };
        function be() {
            const e = "undefined" != typeof window ? window : {};
            return ve(e, _e), e
        }
        function Se(e, t=0) {
            return setTimeout(e, t)
        }
        function xe() {
            return Date.now()
        }
        function Te(e) {
            return "object" == typeof e && null !== e && e.constructor && "Object" === Object.prototype.toString.call(e).slice(8, -1)
        }
        function Ce(...e) {
            const t = Object(e[0]),
                i = ["__proto__", "constructor", "prototype"];
            for (let n = 1; n < e.length; n += 1) {
                const o = e[n];
                if (null != o && (s = o, !("undefined" != typeof window && void 0 !== window.HTMLElement ? s instanceof HTMLElement : s && (1 === s.nodeType || 11 === s.nodeType)))) {
                    const e = Object.keys(Object(o)).filter((e => i.indexOf(e) < 0));
                    for (let i = 0, s = e.length; i < s; i += 1) {
                        const s = e[i],
                            n = Object.getOwnPropertyDescriptor(o, s);
                        void 0 !== n && n.enumerable && (Te(t[s]) && Te(o[s]) ? o[s].__swiper__ ? t[s] = o[s] : Ce(t[s], o[s]) : !Te(t[s]) && Te(o[s]) ? (t[s] = {}, o[s].__swiper__ ? t[s] = o[s] : Ce(t[s], o[s])) : t[s] = o[s])
                    }
                }
            }
            var s;
            return t
        }
        function Pe(e, t, i) {
            e.style.setProperty(t, i)
        }
        function Ee({swiper: e, targetPosition: t, side: i}) {
            const s = be(),
                n = -e.translate;
            let o,
                a = null;
            const r = e.params.speed;
            e.wrapperEl.style.scrollSnapType = "none",
            s.cancelAnimationFrame(e.cssModeFrameID);
            const l = t > n ? "next" : "prev",
                d = (e, t) => "next" === l && e >= t || "prev" === l && e <= t,
                h = () => {
                    o = (new Date).getTime(),
                    null === a && (a = o);
                    const l = Math.max(Math.min((o - a) / r, 1), 0),
                        c = .5 - Math.cos(l * Math.PI) / 2;
                    let p = n + c * (t - n);
                    if (d(p, t) && (p = t), e.wrapperEl.scrollTo({
                        [i]: p
                    }), d(p, t))
                        return e.wrapperEl.style.overflow = "hidden", e.wrapperEl.style.scrollSnapType = "", setTimeout((() => {
                            e.wrapperEl.style.overflow = "",
                            e.wrapperEl.scrollTo({
                                [i]: p
                            })
                        })), void s.cancelAnimationFrame(e.cssModeFrameID);
                    e.cssModeFrameID = s.requestAnimationFrame(h)
                };
            h()
        }
        function Le(e, t="") {
            return [...e.children].filter((e => e.matches(t)))
        }
        function Ie(e, t=[]) {
            const i = document.createElement(e);
            return i.classList.add(...Array.isArray(t) ? t : [t]), i
        }
        function ze(e, t) {
            return be().getComputedStyle(e, null).getPropertyValue(t)
        }
        function Me(e) {
            let t,
                i = e;
            if (i) {
                for (t = 0; null !== (i = i.previousSibling);)
                    1 === i.nodeType && (t += 1);
                return t
            }
        }
        function Ae(e, t) {
            const i = [];
            let s = e.parentElement;
            for (; s;)
                t ? s.matches(t) && i.push(s) : i.push(s),
                s = s.parentElement;
            return i
        }
        function Oe(e, t, i) {
            const s = be();
            return i ? e["width" === t ? "offsetWidth" : "offsetHeight"] + parseFloat(s.getComputedStyle(e, null).getPropertyValue("width" === t ? "margin-right" : "margin-top")) + parseFloat(s.getComputedStyle(e, null).getPropertyValue("width" === t ? "margin-left" : "margin-bottom")) : e.offsetWidth
        }
        let ke,
            De,
            Be;
        function Fe() {
            return ke || (ke = function() {
                const e = be(),
                    t = ye();
                return {
                    smoothScroll: t.documentElement && "scrollBehavior" in t.documentElement.style,
                    touch: !!("ontouchstart" in e || e.DocumentTouch && t instanceof e.DocumentTouch)
                }
            }()), ke
        }
        const Ze = {
                on(e, t, i) {
                    const s = this;
                    if (!s.eventsListeners || s.destroyed)
                        return s;
                    if ("function" != typeof t)
                        return s;
                    const n = i ? "unshift" : "push";
                    return e.split(" ").forEach((e => {
                        s.eventsListeners[e] || (s.eventsListeners[e] = []),
                        s.eventsListeners[e][n](t)
                    })), s
                },
                once(e, t, i) {
                    const s = this;
                    if (!s.eventsListeners || s.destroyed)
                        return s;
                    if ("function" != typeof t)
                        return s;
                    function n(...i) {
                        s.off(e, n),
                        n.__emitterProxy && delete n.__emitterProxy,
                        t.apply(s, i)
                    }
                    return n.__emitterProxy = t, s.on(e, n, i)
                },
                onAny(e, t) {
                    const i = this;
                    if (!i.eventsListeners || i.destroyed)
                        return i;
                    if ("function" != typeof e)
                        return i;
                    const s = t ? "unshift" : "push";
                    return i.eventsAnyListeners.indexOf(e) < 0 && i.eventsAnyListeners[s](e), i
                },
                offAny(e) {
                    const t = this;
                    if (!t.eventsListeners || t.destroyed)
                        return t;
                    if (!t.eventsAnyListeners)
                        return t;
                    const i = t.eventsAnyListeners.indexOf(e);
                    return i >= 0 && t.eventsAnyListeners.splice(i, 1), t
                },
                off(e, t) {
                    const i = this;
                    return !i.eventsListeners || i.destroyed ? i : i.eventsListeners ? (e.split(" ").forEach((e => {
                        void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e] && i.eventsListeners[e].forEach(((s, n) => {
                            (s === t || s.__emitterProxy && s.__emitterProxy === t) && i.eventsListeners[e].splice(n, 1)
                        }))
                    })), i) : i
                },
                emit(...e) {
                    const t = this;
                    if (!t.eventsListeners || t.destroyed)
                        return t;
                    if (!t.eventsListeners)
                        return t;
                    let i,
                        s,
                        n;
                    return "string" == typeof e[0] || Array.isArray(e[0]) ? (i = e[0], s = e.slice(1, e.length), n = t) : (i = e[0].events, s = e[0].data, n = e[0].context || t), s.unshift(n), (Array.isArray(i) ? i : i.split(" ")).forEach((e => {
                        t.eventsAnyListeners && t.eventsAnyListeners.length && t.eventsAnyListeners.forEach((t => {
                            t.apply(n, [e, ...s])
                        })),
                        t.eventsListeners && t.eventsListeners[e] && t.eventsListeners[e].forEach((e => {
                            e.apply(n, s)
                        }))
                    })), t
                }
            },
            Ne = (e, t) => {
                if (!e || e.destroyed || !e.params)
                    return;
                const i = t.closest(e.isElement ? "swiper-slide" : `.${e.params.slideClass}`);
                if (i) {
                    const t = i.querySelector(`.${e.params.lazyPreloaderClass}`);
                    t && t.remove()
                }
            },
            He = (e, t) => {
                if (!e.slides[t])
                    return;
                const i = e.slides[t].querySelector('[loading="lazy"]');
                i && i.removeAttribute("loading")
            },
            Ge = e => {
                if (!e || e.destroyed || !e.params)
                    return;
                let t = e.params.lazyPreloadPrevNext;
                const i = e.slides.length;
                if (!i || !t || t < 0)
                    return;
                t = Math.min(t, i);
                const s = "auto" === e.params.slidesPerView ? e.slidesPerViewDynamic() : Math.ceil(e.params.slidesPerView),
                    n = e.activeIndex,
                    o = n + s - 1;
                if (e.params.rewind)
                    for (let s = n - t; s <= o + t; s += 1) {
                        const t = (s % i + i) % i;
                        t !== n && t > o && He(e, t)
                    }
                else
                    for (let s = Math.max(o - t, 0); s <= Math.min(o + t, i - 1); s += 1)
                        s !== n && s > o && He(e, s)
            },
            Re = {
                updateSize: function() {
                    const e = this;
                    let t,
                        i;
                    const s = e.el;
                    t = void 0 !== e.params.width && null !== e.params.width ? e.params.width : s.clientWidth,
                    i = void 0 !== e.params.height && null !== e.params.height ? e.params.height : s.clientHeight,
                    0 === t && e.isHorizontal() || 0 === i && e.isVertical() || (t = t - parseInt(ze(s, "padding-left") || 0, 10) - parseInt(ze(s, "padding-right") || 0, 10), i = i - parseInt(ze(s, "padding-top") || 0, 10) - parseInt(ze(s, "padding-bottom") || 0, 10), Number.isNaN(t) && (t = 0), Number.isNaN(i) && (i = 0), Object.assign(e, {
                        width: t,
                        height: i,
                        size: e.isHorizontal() ? t : i
                    }))
                },
                updateSlides: function() {
                    const e = this;
                    function t(t) {
                        return e.isHorizontal() ? t : {
                            width: "height",
                            "margin-top": "margin-left",
                            "margin-bottom ": "margin-right",
                            "margin-left": "margin-top",
                            "margin-right": "margin-bottom",
                            "padding-left": "padding-top",
                            "padding-right": "padding-bottom",
                            marginRight: "marginBottom"
                        }[t]
                    }
                    function i(e, i) {
                        return parseFloat(e.getPropertyValue(t(i)) || 0)
                    }
                    const s = e.params,
                        {wrapperEl: n, slidesEl: o, size: a, rtlTranslate: r, wrongRTL: l} = e,
                        d = e.virtual && s.virtual.enabled,
                        h = d ? e.virtual.slides.length : e.slides.length,
                        c = Le(o, `.${e.params.slideClass}, swiper-slide`),
                        p = d ? e.virtual.slides.length : c.length;
                    let u = [];
                    const m = [],
                        f = [];
                    let g = s.slidesOffsetBefore;
                    "function" == typeof g && (g = s.slidesOffsetBefore.call(e));
                    let v = s.slidesOffsetAfter;
                    "function" == typeof v && (v = s.slidesOffsetAfter.call(e));
                    const w = e.snapGrid.length,
                        y = e.slidesGrid.length;
                    let _ = s.spaceBetween,
                        b = -g,
                        S = 0,
                        x = 0;
                    if (void 0 === a)
                        return;
                    "string" == typeof _ && _.indexOf("%") >= 0 && (_ = parseFloat(_.replace("%", "")) / 100 * a),
                    e.virtualSize = -_,
                    c.forEach((e => {
                        r ? e.style.marginLeft = "" : e.style.marginRight = "",
                        e.style.marginBottom = "",
                        e.style.marginTop = ""
                    })),
                    s.centeredSlides && s.cssMode && (Pe(n, "--swiper-centered-offset-before", ""), Pe(n, "--swiper-centered-offset-after", ""));
                    const T = s.grid && s.grid.rows > 1 && e.grid;
                    let C;
                    T && e.grid.initSlides(p);
                    const P = "auto" === s.slidesPerView && s.breakpoints && Object.keys(s.breakpoints).filter((e => void 0 !== s.breakpoints[e].slidesPerView)).length > 0;
                    for (let n = 0; n < p; n += 1) {
                        let o;
                        if (C = 0, c[n] && (o = c[n]), T && e.grid.updateSlide(n, o, p, t), !c[n] || "none" !== ze(o, "display")) {
                            if ("auto" === s.slidesPerView) {
                                P && (c[n].style[t("width")] = "");
                                const a = getComputedStyle(o),
                                    r = o.style.transform,
                                    l = o.style.webkitTransform;
                                if (r && (o.style.transform = "none"), l && (o.style.webkitTransform = "none"), s.roundLengths)
                                    C = e.isHorizontal() ? Oe(o, "width", !0) : Oe(o, "height", !0);
                                else {
                                    const e = i(a, "width"),
                                        t = i(a, "padding-left"),
                                        s = i(a, "padding-right"),
                                        n = i(a, "margin-left"),
                                        r = i(a, "margin-right"),
                                        l = a.getPropertyValue("box-sizing");
                                    if (l && "border-box" === l)
                                        C = e + n + r;
                                    else {
                                        const {clientWidth: i, offsetWidth: a} = o;
                                        C = e + t + s + n + r + (a - i)
                                    }
                                }
                                r && (o.style.transform = r),
                                l && (o.style.webkitTransform = l),
                                s.roundLengths && (C = Math.floor(C))
                            } else
                                C = (a - (s.slidesPerView - 1) * _) / s.slidesPerView,
                                s.roundLengths && (C = Math.floor(C)),
                                c[n] && (c[n].style[t("width")] = `${C}px`);
                            c[n] && (c[n].swiperSlideSize = C),
                            f.push(C),
                            s.centeredSlides ? (b = b + C / 2 + S / 2 + _, 0 === S && 0 !== n && (b = b - a / 2 - _), 0 === n && (b = b - a / 2 - _), Math.abs(b) < .001 && (b = 0), s.roundLengths && (b = Math.floor(b)), x % s.slidesPerGroup == 0 && u.push(b), m.push(b)) : (s.roundLengths && (b = Math.floor(b)), (x - Math.min(e.params.slidesPerGroupSkip, x)) % e.params.slidesPerGroup == 0 && u.push(b), m.push(b), b = b + C + _),
                            e.virtualSize += C + _,
                            S = C,
                            x += 1
                        }
                    }
                    if (e.virtualSize = Math.max(e.virtualSize, a) + v, r && l && ("slide" === s.effect || "coverflow" === s.effect) && (n.style.width = `${e.virtualSize + s.spaceBetween}px`), s.setWrapperSize && (n.style[t("width")] = `${e.virtualSize + s.spaceBetween}px`), T && e.grid.updateWrapperSize(C, u, t), !s.centeredSlides) {
                        const t = [];
                        for (let i = 0; i < u.length; i += 1) {
                            let n = u[i];
                            s.roundLengths && (n = Math.floor(n)),
                            u[i] <= e.virtualSize - a && t.push(n)
                        }
                        u = t,
                        Math.floor(e.virtualSize - a) - Math.floor(u[u.length - 1]) > 1 && u.push(e.virtualSize - a)
                    }
                    if (d && s.loop) {
                        const t = f[0] + _;
                        if (s.slidesPerGroup > 1) {
                            const i = Math.ceil((e.virtual.slidesBefore + e.virtual.slidesAfter) / s.slidesPerGroup),
                                n = t * s.slidesPerGroup;
                            for (let e = 0; e < i; e += 1)
                                u.push(u[u.length - 1] + n)
                        }
                        for (let i = 0; i < e.virtual.slidesBefore + e.virtual.slidesAfter; i += 1)
                            1 === s.slidesPerGroup && u.push(u[u.length - 1] + t),
                            m.push(m[m.length - 1] + t),
                            e.virtualSize += t
                    }
                    if (0 === u.length && (u = [0]), 0 !== s.spaceBetween) {
                        const i = e.isHorizontal() && r ? "marginLeft" : t("marginRight");
                        c.filter(((e, t) => !(s.cssMode && !s.loop) || t !== c.length - 1)).forEach((e => {
                            e.style[i] = `${_}px`
                        }))
                    }
                    if (s.centeredSlides && s.centeredSlidesBounds) {
                        let e = 0;
                        f.forEach((t => {
                            e += t + (s.spaceBetween ? s.spaceBetween : 0)
                        })),
                        e -= s.spaceBetween;
                        const t = e - a;
                        u = u.map((e => e < 0 ? -g : e > t ? t + v : e))
                    }
                    if (s.centerInsufficientSlides) {
                        let e = 0;
                        if (f.forEach((t => {
                            e += t + (s.spaceBetween ? s.spaceBetween : 0)
                        })), e -= s.spaceBetween, e < a) {
                            const t = (a - e) / 2;
                            u.forEach(((e, i) => {
                                u[i] = e - t
                            })),
                            m.forEach(((e, i) => {
                                m[i] = e + t
                            }))
                        }
                    }
                    if (Object.assign(e, {
                        slides: c,
                        snapGrid: u,
                        slidesGrid: m,
                        slidesSizesGrid: f
                    }), s.centeredSlides && s.cssMode && !s.centeredSlidesBounds) {
                        Pe(n, "--swiper-centered-offset-before", -u[0] + "px"),
                        Pe(n, "--swiper-centered-offset-after", e.size / 2 - f[f.length - 1] / 2 + "px");
                        const t = -e.snapGrid[0],
                            i = -e.slidesGrid[0];
                        e.snapGrid = e.snapGrid.map((e => e + t)),
                        e.slidesGrid = e.slidesGrid.map((e => e + i))
                    }
                    if (p !== h && e.emit("slidesLengthChange"), u.length !== w && (e.params.watchOverflow && e.checkOverflow(), e.emit("snapGridLengthChange")), m.length !== y && e.emit("slidesGridLengthChange"), s.watchSlidesProgress && e.updateSlidesOffset(), !(d || s.cssMode || "slide" !== s.effect && "fade" !== s.effect)) {
                        const t = `${s.containerModifierClass}backface-hidden`,
                            i = e.el.classList.contains(t);
                        p <= s.maxBackfaceHiddenSlides ? i || e.el.classList.add(t) : i && e.el.classList.remove(t)
                    }
                },
                updateAutoHeight: function(e) {
                    const t = this,
                        i = [],
                        s = t.virtual && t.params.virtual.enabled;
                    let n,
                        o = 0;
                    "number" == typeof e ? t.setTransition(e) : !0 === e && t.setTransition(t.params.speed);
                    const a = e => s ? t.getSlideIndexByData(e) : t.slides[e];
                    if ("auto" !== t.params.slidesPerView && t.params.slidesPerView > 1)
                        if (t.params.centeredSlides)
                            (t.visibleSlides || []).forEach((e => {
                                i.push(e)
                            }));
                        else
                            for (n = 0; n < Math.ceil(t.params.slidesPerView); n += 1) {
                                const e = t.activeIndex + n;
                                if (e > t.slides.length && !s)
                                    break;
                                i.push(a(e))
                            }
                    else
                        i.push(a(t.activeIndex));
                    for (n = 0; n < i.length; n += 1)
                        if (void 0 !== i[n]) {
                            const e = i[n].offsetHeight;
                            o = e > o ? e : o
                        }
                    (o || 0 === o) && (t.wrapperEl.style.height = `${o}px`)
                },
                updateSlidesOffset: function() {
                    const e = this,
                        t = e.slides,
                        i = e.isElement ? e.isHorizontal() ? e.wrapperEl.offsetLeft : e.wrapperEl.offsetTop : 0;
                    for (let s = 0; s < t.length; s += 1)
                        t[s].swiperSlideOffset = (e.isHorizontal() ? t[s].offsetLeft : t[s].offsetTop) - i - e.cssOverflowAdjustment()
                },
                updateSlidesProgress: function(e=this && this.translate || 0) {
                    const t = this,
                        i = t.params,
                        {slides: s, rtlTranslate: n, snapGrid: o} = t;
                    if (0 === s.length)
                        return;
                    void 0 === s[0].swiperSlideOffset && t.updateSlidesOffset();
                    let a = -e;
                    n && (a = e),
                    s.forEach((e => {
                        e.classList.remove(i.slideVisibleClass)
                    })),
                    t.visibleSlidesIndexes = [],
                    t.visibleSlides = [];
                    for (let e = 0; e < s.length; e += 1) {
                        const r = s[e];
                        let l = r.swiperSlideOffset;
                        i.cssMode && i.centeredSlides && (l -= s[0].swiperSlideOffset);
                        const d = (a + (i.centeredSlides ? t.minTranslate() : 0) - l) / (r.swiperSlideSize + i.spaceBetween),
                            h = (a - o[0] + (i.centeredSlides ? t.minTranslate() : 0) - l) / (r.swiperSlideSize + i.spaceBetween),
                            c = -(a - l),
                            p = c + t.slidesSizesGrid[e];
                        (c >= 0 && c < t.size - 1 || p > 1 && p <= t.size || c <= 0 && p >= t.size) && (t.visibleSlides.push(r), t.visibleSlidesIndexes.push(e), s[e].classList.add(i.slideVisibleClass)),
                        r.progress = n ? -d : d,
                        r.originalProgress = n ? -h : h
                    }
                },
                updateProgress: function(e) {
                    const t = this;
                    if (void 0 === e) {
                        const i = t.rtlTranslate ? -1 : 1;
                        e = t && t.translate && t.translate * i || 0
                    }
                    const i = t.params,
                        s = t.maxTranslate() - t.minTranslate();
                    let {progress: n, isBeginning: o, isEnd: a, progressLoop: r} = t;
                    const l = o,
                        d = a;
                    if (0 === s)
                        n = 0,
                        o = !0,
                        a = !0;
                    else {
                        n = (e - t.minTranslate()) / s;
                        const i = Math.abs(e - t.minTranslate()) < 1,
                            r = Math.abs(e - t.maxTranslate()) < 1;
                        o = i || n <= 0,
                        a = r || n >= 1,
                        i && (n = 0),
                        r && (n = 1)
                    }
                    if (i.loop) {
                        const i = t.getSlideIndexByData(0),
                            s = t.getSlideIndexByData(t.slides.length - 1),
                            n = t.slidesGrid[i],
                            o = t.slidesGrid[s],
                            a = t.slidesGrid[t.slidesGrid.length - 1],
                            l = Math.abs(e);
                        r = l >= n ? (l - n) / a : (l + a - o) / a,
                        r > 1 && (r -= 1)
                    }
                    Object.assign(t, {
                        progress: n,
                        progressLoop: r,
                        isBeginning: o,
                        isEnd: a
                    }),
                    (i.watchSlidesProgress || i.centeredSlides && i.autoHeight) && t.updateSlidesProgress(e),
                    o && !l && t.emit("reachBeginning toEdge"),
                    a && !d && t.emit("reachEnd toEdge"),
                    (l && !o || d && !a) && t.emit("fromEdge"),
                    t.emit("progress", n)
                },
                updateSlidesClasses: function() {
                    const e = this,
                        {slides: t, params: i, slidesEl: s, activeIndex: n} = e,
                        o = e.virtual && i.virtual.enabled,
                        a = e => Le(s, `.${i.slideClass}${e}, swiper-slide${e}`)[0];
                    let r;
                    if (t.forEach((e => {
                        e.classList.remove(i.slideActiveClass, i.slideNextClass, i.slidePrevClass)
                    })), o)
                        if (i.loop) {
                            let t = n - e.virtual.slidesBefore;
                            t < 0 && (t = e.virtual.slides.length + t),
                            t >= e.virtual.slides.length && (t -= e.virtual.slides.length),
                            r = a(`[data-swiper-slide-index="${t}"]`)
                        } else
                            r = a(`[data-swiper-slide-index="${n}"]`);
                    else
                        r = t[n];
                    if (r) {
                        r.classList.add(i.slideActiveClass);
                        let e = function(e, t) {
                            const i = [];
                            for (; e.nextElementSibling;) {
                                const s = e.nextElementSibling;
                                t ? s.matches(t) && i.push(s) : i.push(s),
                                e = s
                            }
                            return i
                        }(r, `.${i.slideClass}, swiper-slide`)[0];
                        i.loop && !e && (e = t[0]),
                        e && e.classList.add(i.slideNextClass);
                        let s = function(e, t) {
                            const i = [];
                            for (; e.previousElementSibling;) {
                                const s = e.previousElementSibling;
                                t ? s.matches(t) && i.push(s) : i.push(s),
                                e = s
                            }
                            return i
                        }(r, `.${i.slideClass}, swiper-slide`)[0];
                        i.loop && 0 === !s && (s = t[t.length - 1]),
                        s && s.classList.add(i.slidePrevClass)
                    }
                    e.emitSlidesClasses()
                },
                updateActiveIndex: function(e) {
                    const t = this,
                        i = t.rtlTranslate ? t.translate : -t.translate,
                        {snapGrid: s, params: n, activeIndex: o, realIndex: a, snapIndex: r} = t;
                    let l,
                        d = e;
                    const h = e => {
                        let i = e - t.virtual.slidesBefore;
                        return i < 0 && (i = t.virtual.slides.length + i), i >= t.virtual.slides.length && (i -= t.virtual.slides.length), i
                    };
                    if (void 0 === d && (d = function(e) {
                        const {slidesGrid: t, params: i} = e,
                            s = e.rtlTranslate ? e.translate : -e.translate;
                        let n;
                        for (let e = 0; e < t.length; e += 1)
                            void 0 !== t[e + 1] ? s >= t[e] && s < t[e + 1] - (t[e + 1] - t[e]) / 2 ? n = e : s >= t[e] && s < t[e + 1] && (n = e + 1) : s >= t[e] && (n = e);
                        return i.normalizeSlideIndex && (n < 0 || void 0 === n) && (n = 0), n
                    }(t)), s.indexOf(i) >= 0)
                        l = s.indexOf(i);
                    else {
                        const e = Math.min(n.slidesPerGroupSkip, d);
                        l = e + Math.floor((d - e) / n.slidesPerGroup)
                    }
                    if (l >= s.length && (l = s.length - 1), d === o)
                        return l !== r && (t.snapIndex = l, t.emit("snapIndexChange")), void (t.params.loop && t.virtual && t.params.virtual.enabled && (t.realIndex = h(d)));
                    let c;
                    c = t.virtual && n.virtual.enabled && n.loop ? h(d) : t.slides[d] ? parseInt(t.slides[d].getAttribute("data-swiper-slide-index") || d, 10) : d,
                    Object.assign(t, {
                        previousSnapIndex: r,
                        snapIndex: l,
                        previousRealIndex: a,
                        realIndex: c,
                        previousIndex: o,
                        activeIndex: d
                    }),
                    t.initialized && Ge(t),
                    t.emit("activeIndexChange"),
                    t.emit("snapIndexChange"),
                    a !== c && t.emit("realIndexChange"),
                    (t.initialized || t.params.runCallbacksOnInit) && t.emit("slideChange")
                },
                updateClickedSlide: function(e) {
                    const t = this,
                        i = t.params,
                        s = e.closest(`.${i.slideClass}, swiper-slide`);
                    let n,
                        o = !1;
                    if (s)
                        for (let e = 0; e < t.slides.length; e += 1)
                            if (t.slides[e] === s) {
                                o = !0,
                                n = e;
                                break
                            }
                    if (!s || !o)
                        return t.clickedSlide = void 0, void (t.clickedIndex = void 0);
                    t.clickedSlide = s,
                    t.virtual && t.params.virtual.enabled ? t.clickedIndex = parseInt(s.getAttribute("data-swiper-slide-index"), 10) : t.clickedIndex = n,
                    i.slideToClickedSlide && void 0 !== t.clickedIndex && t.clickedIndex !== t.activeIndex && t.slideToClickedSlide()
                }
            };
        function Ve({swiper: e, runCallbacks: t, direction: i, step: s}) {
            const {activeIndex: n, previousIndex: o} = e;
            let a = i;
            if (a || (a = n > o ? "next" : n < o ? "prev" : "reset"), e.emit(`transition${s}`), t && n !== o) {
                if ("reset" === a)
                    return void e.emit(`slideResetTransition${s}`);
                e.emit(`slideChangeTransition${s}`),
                "next" === a ? e.emit(`slideNextTransition${s}`) : e.emit(`slidePrevTransition${s}`)
            }
        }
        const $e = {
            slideTo: function(e=0, t=this.params.speed, i=!0, s, n) {
                "string" == typeof e && (e = parseInt(e, 10));
                const o = this;
                let a = e;
                a < 0 && (a = 0);
                const {params: r, snapGrid: l, slidesGrid: d, previousIndex: h, activeIndex: c, rtlTranslate: p, wrapperEl: u, enabled: m} = o;
                if (o.animating && r.preventInteractionOnTransition || !m && !s && !n)
                    return !1;
                const f = Math.min(o.params.slidesPerGroupSkip, a);
                let g = f + Math.floor((a - f) / o.params.slidesPerGroup);
                g >= l.length && (g = l.length - 1);
                const v = -l[g];
                if (r.normalizeSlideIndex)
                    for (let e = 0; e < d.length; e += 1) {
                        const t = -Math.floor(100 * v),
                            i = Math.floor(100 * d[e]),
                            s = Math.floor(100 * d[e + 1]);
                        void 0 !== d[e + 1] ? t >= i && t < s - (s - i) / 2 ? a = e : t >= i && t < s && (a = e + 1) : t >= i && (a = e)
                    }
                if (o.initialized && a !== c) {
                    if (!o.allowSlideNext && v < o.translate && v < o.minTranslate())
                        return !1;
                    if (!o.allowSlidePrev && v > o.translate && v > o.maxTranslate() && (c || 0) !== a)
                        return !1
                }
                let w;
                if (a !== (h || 0) && i && o.emit("beforeSlideChangeStart"), o.updateProgress(v), w = a > c ? "next" : a < c ? "prev" : "reset", p && -v === o.translate || !p && v === o.translate)
                    return o.updateActiveIndex(a), r.autoHeight && o.updateAutoHeight(), o.updateSlidesClasses(), "slide" !== r.effect && o.setTranslate(v), "reset" !== w && (o.transitionStart(i, w), o.transitionEnd(i, w)), !1;
                if (r.cssMode) {
                    const e = o.isHorizontal(),
                        i = p ? v : -v;
                    if (0 === t) {
                        const t = o.virtual && o.params.virtual.enabled;
                        t && (o.wrapperEl.style.scrollSnapType = "none", o._immediateVirtual = !0),
                        t && !o._cssModeVirtualInitialSet && o.params.initialSlide > 0 ? (o._cssModeVirtualInitialSet = !0, requestAnimationFrame((() => {
                            u[e ? "scrollLeft" : "scrollTop"] = i
                        }))) : u[e ? "scrollLeft" : "scrollTop"] = i,
                        t && requestAnimationFrame((() => {
                            o.wrapperEl.style.scrollSnapType = "",
                            o._immediateVirtual = !1
                        }))
                    } else {
                        if (!o.support.smoothScroll)
                            return Ee({
                                swiper: o,
                                targetPosition: i,
                                side: e ? "left" : "top"
                            }), !0;
                        u.scrollTo({
                            [e ? "left" : "top"]: i,
                            behavior: "smooth"
                        })
                    }
                    return !0
                }
                return o.setTransition(t), o.setTranslate(v), o.updateActiveIndex(a), o.updateSlidesClasses(), o.emit("beforeTransitionStart", t, s), o.transitionStart(i, w), 0 === t ? o.transitionEnd(i, w) : o.animating || (o.animating = !0, o.onSlideToWrapperTransitionEnd || (o.onSlideToWrapperTransitionEnd = function(e) {
                    o && !o.destroyed && e.target === this && (o.wrapperEl.removeEventListener("transitionend", o.onSlideToWrapperTransitionEnd), o.onSlideToWrapperTransitionEnd = null, delete o.onSlideToWrapperTransitionEnd, o.transitionEnd(i, w))
                }), o.wrapperEl.addEventListener("transitionend", o.onSlideToWrapperTransitionEnd)), !0
            },
            slideToLoop: function(e=0, t=this.params.speed, i=!0, s) {
                "string" == typeof e && (e = parseInt(e, 10));
                const n = this;
                let o = e;
                return n.params.loop && (n.virtual && n.params.virtual.enabled ? o += n.virtual.slidesBefore : o = n.getSlideIndexByData(o)), n.slideTo(o, t, i, s)
            },
            slideNext: function(e=this.params.speed, t=!0, i) {
                const s = this,
                    {enabled: n, params: o, animating: a} = s;
                if (!n)
                    return s;
                let r = o.slidesPerGroup;
                "auto" === o.slidesPerView && 1 === o.slidesPerGroup && o.slidesPerGroupAuto && (r = Math.max(s.slidesPerViewDynamic("current", !0), 1));
                const l = s.activeIndex < o.slidesPerGroupSkip ? 1 : r,
                    d = s.virtual && o.virtual.enabled;
                if (o.loop) {
                    if (a && !d && o.loopPreventsSliding)
                        return !1;
                    s.loopFix({
                        direction: "next"
                    }),
                    s._clientLeft = s.wrapperEl.clientLeft
                }
                return o.rewind && s.isEnd ? s.slideTo(0, e, t, i) : s.slideTo(s.activeIndex + l, e, t, i)
            },
            slidePrev: function(e=this.params.speed, t=!0, i) {
                const s = this,
                    {params: n, snapGrid: o, slidesGrid: a, rtlTranslate: r, enabled: l, animating: d} = s;
                if (!l)
                    return s;
                const h = s.virtual && n.virtual.enabled;
                if (n.loop) {
                    if (d && !h && n.loopPreventsSliding)
                        return !1;
                    s.loopFix({
                        direction: "prev"
                    }),
                    s._clientLeft = s.wrapperEl.clientLeft
                }
                function c(e) {
                    return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e)
                }
                const p = c(r ? s.translate : -s.translate),
                    u = o.map((e => c(e)));
                let m = o[u.indexOf(p) - 1];
                if (void 0 === m && n.cssMode) {
                    let e;
                    o.forEach(((t, i) => {
                        p >= t && (e = i)
                    })),
                    void 0 !== e && (m = o[e > 0 ? e - 1 : e])
                }
                let f = 0;
                if (void 0 !== m && (f = a.indexOf(m), f < 0 && (f = s.activeIndex - 1), "auto" === n.slidesPerView && 1 === n.slidesPerGroup && n.slidesPerGroupAuto && (f = f - s.slidesPerViewDynamic("previous", !0) + 1, f = Math.max(f, 0))), n.rewind && s.isBeginning) {
                    const n = s.params.virtual && s.params.virtual.enabled && s.virtual ? s.virtual.slides.length - 1 : s.slides.length - 1;
                    return s.slideTo(n, e, t, i)
                }
                return s.slideTo(f, e, t, i)
            },
            slideReset: function(e=this.params.speed, t=!0, i) {
                return this.slideTo(this.activeIndex, e, t, i)
            },
            slideToClosest: function(e=this.params.speed, t=!0, i, s=.5) {
                const n = this;
                let o = n.activeIndex;
                const a = Math.min(n.params.slidesPerGroupSkip, o),
                    r = a + Math.floor((o - a) / n.params.slidesPerGroup),
                    l = n.rtlTranslate ? n.translate : -n.translate;
                if (l >= n.snapGrid[r]) {
                    const e = n.snapGrid[r];
                    l - e > (n.snapGrid[r + 1] - e) * s && (o += n.params.slidesPerGroup)
                } else {
                    const e = n.snapGrid[r - 1];
                    l - e <= (n.snapGrid[r] - e) * s && (o -= n.params.slidesPerGroup)
                }
                return o = Math.max(o, 0), o = Math.min(o, n.slidesGrid.length - 1), n.slideTo(o, e, t, i)
            },
            slideToClickedSlide: function() {
                const e = this,
                    {params: t, slidesEl: i} = e,
                    s = "auto" === t.slidesPerView ? e.slidesPerViewDynamic() : t.slidesPerView;
                let n,
                    o = e.clickedIndex;
                const a = e.isElement ? "swiper-slide" : `.${t.slideClass}`;
                if (t.loop) {
                    if (e.animating)
                        return;
                    n = parseInt(e.clickedSlide.getAttribute("data-swiper-slide-index"), 10),
                    t.centeredSlides ? o < e.loopedSlides - s / 2 || o > e.slides.length - e.loopedSlides + s / 2 ? (e.loopFix(), o = e.getSlideIndex(Le(i, `${a}[data-swiper-slide-index="${n}"]`)[0]), Se((() => {
                        e.slideTo(o)
                    }))) : e.slideTo(o) : o > e.slides.length - s ? (e.loopFix(), o = e.getSlideIndex(Le(i, `${a}[data-swiper-slide-index="${n}"]`)[0]), Se((() => {
                        e.slideTo(o)
                    }))) : e.slideTo(o)
                } else
                    e.slideTo(o)
            }
        };
        function qe(e) {
            const t = this,
                i = ye(),
                s = be(),
                n = t.touchEventsData;
            n.evCache.push(e);
            const {params: o, touches: a, enabled: r} = t;
            if (!r)
                return;
            if (!o.simulateTouch && "mouse" === e.pointerType)
                return;
            if (t.animating && o.preventInteractionOnTransition)
                return;
            !t.animating && o.cssMode && o.loop && t.loopFix();
            let l = e;
            l.originalEvent && (l = l.originalEvent);
            let d = l.target;
            if ("wrapper" === o.touchEventsTarget && !t.wrapperEl.contains(d))
                return;
            if ("which" in l && 3 === l.which)
                return;
            if ("button" in l && l.button > 0)
                return;
            if (n.isTouched && n.isMoved)
                return;
            const h = !!o.noSwipingClass && "" !== o.noSwipingClass,
                c = e.composedPath ? e.composedPath() : e.path;
            h && l.target && l.target.shadowRoot && c && (d = c[0]);
            const p = o.noSwipingSelector ? o.noSwipingSelector : `.${o.noSwipingClass}`,
                u = !(!l.target || !l.target.shadowRoot);
            if (o.noSwiping && (u ? function(e, t=this) {
                return function t(i) {
                    if (!i || i === ye() || i === be())
                        return null;
                    i.assignedSlot && (i = i.assignedSlot);
                    const s = i.closest(e);
                    return s || i.getRootNode ? s || t(i.getRootNode().host) : null
                }(t)
            }(p, d) : d.closest(p)))
                return void (t.allowClick = !0);
            if (o.swipeHandler && !d.closest(o.swipeHandler))
                return;
            a.currentX = l.pageX,
            a.currentY = l.pageY;
            const m = a.currentX,
                f = a.currentY,
                g = o.edgeSwipeDetection || o.iOSEdgeSwipeDetection,
                v = o.edgeSwipeThreshold || o.iOSEdgeSwipeThreshold;
            if (g && (m <= v || m >= s.innerWidth - v)) {
                if ("prevent" !== g)
                    return;
                e.preventDefault()
            }
            Object.assign(n, {
                isTouched: !0,
                isMoved: !1,
                allowTouchCallbacks: !0,
                isScrolling: void 0,
                startMoving: void 0
            }),
            a.startX = m,
            a.startY = f,
            n.touchStartTime = xe(),
            t.allowClick = !0,
            t.updateSize(),
            t.swipeDirection = void 0,
            o.threshold > 0 && (n.allowThresholdMove = !1);
            let w = !0;
            d.matches(n.focusableElements) && (w = !1, "SELECT" === d.nodeName && (n.isTouched = !1)),
            i.activeElement && i.activeElement.matches(n.focusableElements) && i.activeElement !== d && i.activeElement.blur();
            const y = w && t.allowTouchMove && o.touchStartPreventDefault;
            !o.touchStartForcePreventDefault && !y || d.isContentEditable || l.preventDefault(),
            t.params.freeMode && t.params.freeMode.enabled && t.freeMode && t.animating && !o.cssMode && t.freeMode.onTouchStart(),
            t.emit("touchStart", l)
        }
        function We(e) {
            const t = ye(),
                i = this,
                s = i.touchEventsData,
                {params: n, touches: o, rtlTranslate: a, enabled: r} = i;
            if (!r)
                return;
            if (!n.simulateTouch && "mouse" === e.pointerType)
                return;
            let l = e;
            if (l.originalEvent && (l = l.originalEvent), !s.isTouched)
                return void (s.startMoving && s.isScrolling && i.emit("touchMoveOpposite", l));
            const d = s.evCache.findIndex((e => e.pointerId === l.pointerId));
            d >= 0 && (s.evCache[d] = l);
            const h = s.evCache.length > 1 ? s.evCache[0] : l,
                c = h.pageX,
                p = h.pageY;
            if (l.preventedByNestedSwiper)
                return o.startX = c, void (o.startY = p);
            if (!i.allowTouchMove)
                return l.target.matches(s.focusableElements) || (i.allowClick = !1), void (s.isTouched && (Object.assign(o, {
                    startX: c,
                    startY: p,
                    prevX: i.touches.currentX,
                    prevY: i.touches.currentY,
                    currentX: c,
                    currentY: p
                }), s.touchStartTime = xe()));
            if (n.touchReleaseOnEdges && !n.loop)
                if (i.isVertical()) {
                    if (p < o.startY && i.translate <= i.maxTranslate() || p > o.startY && i.translate >= i.minTranslate())
                        return s.isTouched = !1, void (s.isMoved = !1)
                } else if (c < o.startX && i.translate <= i.maxTranslate() || c > o.startX && i.translate >= i.minTranslate())
                    return;
            if (t.activeElement && l.target === t.activeElement && l.target.matches(s.focusableElements))
                return s.isMoved = !0, void (i.allowClick = !1);
            if (s.allowTouchCallbacks && i.emit("touchMove", l), l.targetTouches && l.targetTouches.length > 1)
                return;
            o.currentX = c,
            o.currentY = p;
            const u = o.currentX - o.startX,
                m = o.currentY - o.startY;
            if (i.params.threshold && Math.sqrt(u ** 2 + m ** 2) < i.params.threshold)
                return;
            if (void 0 === s.isScrolling) {
                let e;
                i.isHorizontal() && o.currentY === o.startY || i.isVertical() && o.currentX === o.startX ? s.isScrolling = !1 : u * u + m * m >= 25 && (e = 180 * Math.atan2(Math.abs(m), Math.abs(u)) / Math.PI, s.isScrolling = i.isHorizontal() ? e > n.touchAngle : 90 - e > n.touchAngle)
            }
            if (s.isScrolling && i.emit("touchMoveOpposite", l), void 0 === s.startMoving && (o.currentX === o.startX && o.currentY === o.startY || (s.startMoving = !0)), s.isScrolling || i.zoom && i.params.zoom && i.params.zoom.enabled && s.evCache.length > 1)
                return void (s.isTouched = !1);
            if (!s.startMoving)
                return;
            i.allowClick = !1,
            !n.cssMode && l.cancelable && l.preventDefault(),
            n.touchMoveStopPropagation && !n.nested && l.stopPropagation();
            let f = i.isHorizontal() ? u : m,
                g = i.isHorizontal() ? o.currentX - o.previousX : o.currentY - o.previousY;
            n.oneWayMovement && (f = Math.abs(f) * (a ? 1 : -1), g = Math.abs(g) * (a ? 1 : -1)),
            o.diff = f,
            f *= n.touchRatio,
            a && (f = -f, g = -g);
            const v = i.touchesDirection;
            i.swipeDirection = f > 0 ? "prev" : "next",
            i.touchesDirection = g > 0 ? "prev" : "next";
            const w = i.params.loop && !n.cssMode;
            if (!s.isMoved) {
                if (w && i.loopFix({
                    direction: i.swipeDirection
                }), s.startTranslate = i.getTranslate(), i.setTransition(0), i.animating) {
                    const e = new window.CustomEvent("transitionend", {
                        bubbles: !0,
                        cancelable: !0
                    });
                    i.wrapperEl.dispatchEvent(e)
                }
                s.allowMomentumBounce = !1,
                !n.grabCursor || !0 !== i.allowSlideNext && !0 !== i.allowSlidePrev || i.setGrabCursor(!0),
                i.emit("sliderFirstMove", l)
            }
            let y;
            s.isMoved && v !== i.touchesDirection && w && Math.abs(f) >= 1 && (i.loopFix({
                direction: i.swipeDirection,
                setTranslate: !0
            }), y = !0),
            i.emit("sliderMove", l),
            s.isMoved = !0,
            s.currentTranslate = f + s.startTranslate;
            let _ = !0,
                b = n.resistanceRatio;
            if (n.touchReleaseOnEdges && (b = 0), f > 0 ? (w && !y && s.currentTranslate > (n.centeredSlides ? i.minTranslate() - i.size / 2 : i.minTranslate()) && i.loopFix({
                direction: "prev",
                setTranslate: !0,
                activeSlideIndex: 0
            }), s.currentTranslate > i.minTranslate() && (_ = !1, n.resistance && (s.currentTranslate = i.minTranslate() - 1 + (-i.minTranslate() + s.startTranslate + f) ** b))) : f < 0 && (w && !y && s.currentTranslate < (n.centeredSlides ? i.maxTranslate() + i.size / 2 : i.maxTranslate()) && i.loopFix({
                direction: "next",
                setTranslate: !0,
                activeSlideIndex: i.slides.length - ("auto" === n.slidesPerView ? i.slidesPerViewDynamic() : Math.ceil(parseFloat(n.slidesPerView, 10)))
            }), s.currentTranslate < i.maxTranslate() && (_ = !1, n.resistance && (s.currentTranslate = i.maxTranslate() + 1 - (i.maxTranslate() - s.startTranslate - f) ** b))), _ && (l.preventedByNestedSwiper = !0), !i.allowSlideNext && "next" === i.swipeDirection && s.currentTranslate < s.startTranslate && (s.currentTranslate = s.startTranslate), !i.allowSlidePrev && "prev" === i.swipeDirection && s.currentTranslate > s.startTranslate && (s.currentTranslate = s.startTranslate), i.allowSlidePrev || i.allowSlideNext || (s.currentTranslate = s.startTranslate), n.threshold > 0) {
                if (!(Math.abs(f) > n.threshold || s.allowThresholdMove))
                    return void (s.currentTranslate = s.startTranslate);
                if (!s.allowThresholdMove)
                    return s.allowThresholdMove = !0, o.startX = o.currentX, o.startY = o.currentY, s.currentTranslate = s.startTranslate, void (o.diff = i.isHorizontal() ? o.currentX - o.startX : o.currentY - o.startY)
            }
            n.followFinger && !n.cssMode && ((n.freeMode && n.freeMode.enabled && i.freeMode || n.watchSlidesProgress) && (i.updateActiveIndex(), i.updateSlidesClasses()), i.params.freeMode && n.freeMode.enabled && i.freeMode && i.freeMode.onTouchMove(), i.updateProgress(s.currentTranslate), i.setTranslate(s.currentTranslate))
        }
        function je(e) {
            const t = this,
                i = t.touchEventsData,
                s = i.evCache.findIndex((t => t.pointerId === e.pointerId));
            if (s >= 0 && i.evCache.splice(s, 1), ["pointercancel", "pointerout", "pointerleave"].includes(e.type) && ("pointercancel" !== e.type || !t.browser.isSafari && !t.browser.isWebView))
                return;
            const {params: n, touches: o, rtlTranslate: a, slidesGrid: r, enabled: l} = t;
            if (!l)
                return;
            if (!n.simulateTouch && "mouse" === e.pointerType)
                return;
            let d = e;
            if (d.originalEvent && (d = d.originalEvent), i.allowTouchCallbacks && t.emit("touchEnd", d), i.allowTouchCallbacks = !1, !i.isTouched)
                return i.isMoved && n.grabCursor && t.setGrabCursor(!1), i.isMoved = !1, void (i.startMoving = !1);
            n.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
            const h = xe(),
                c = h - i.touchStartTime;
            if (t.allowClick) {
                const e = d.path || d.composedPath && d.composedPath();
                t.updateClickedSlide(e && e[0] || d.target),
                t.emit("tap click", d),
                c < 300 && h - i.lastClickTime < 300 && t.emit("doubleTap doubleClick", d)
            }
            if (i.lastClickTime = xe(), Se((() => {
                t.destroyed || (t.allowClick = !0)
            })), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === o.diff || i.currentTranslate === i.startTranslate)
                return i.isTouched = !1, i.isMoved = !1, void (i.startMoving = !1);
            let p;
            if (i.isTouched = !1, i.isMoved = !1, i.startMoving = !1, p = n.followFinger ? a ? t.translate : -t.translate : -i.currentTranslate, n.cssMode)
                return;
            if (t.params.freeMode && n.freeMode.enabled)
                return void t.freeMode.onTouchEnd({
                    currentPos: p
                });
            let u = 0,
                m = t.slidesSizesGrid[0];
            for (let e = 0; e < r.length; e += e < n.slidesPerGroupSkip ? 1 : n.slidesPerGroup) {
                const t = e < n.slidesPerGroupSkip - 1 ? 1 : n.slidesPerGroup;
                void 0 !== r[e + t] ? p >= r[e] && p < r[e + t] && (u = e, m = r[e + t] - r[e]) : p >= r[e] && (u = e, m = r[r.length - 1] - r[r.length - 2])
            }
            let f = null,
                g = null;
            n.rewind && (t.isBeginning ? g = t.params.virtual && t.params.virtual.enabled && t.virtual ? t.virtual.slides.length - 1 : t.slides.length - 1 : t.isEnd && (f = 0));
            const v = (p - r[u]) / m,
                w = u < n.slidesPerGroupSkip - 1 ? 1 : n.slidesPerGroup;
            if (c > n.longSwipesMs) {
                if (!n.longSwipes)
                    return void t.slideTo(t.activeIndex);
                "next" === t.swipeDirection && (v >= n.longSwipesRatio ? t.slideTo(n.rewind && t.isEnd ? f : u + w) : t.slideTo(u)),
                "prev" === t.swipeDirection && (v > 1 - n.longSwipesRatio ? t.slideTo(u + w) : null !== g && v < 0 && Math.abs(v) > n.longSwipesRatio ? t.slideTo(g) : t.slideTo(u))
            } else {
                if (!n.shortSwipes)
                    return void t.slideTo(t.activeIndex);
                !t.navigation || d.target !== t.navigation.nextEl && d.target !== t.navigation.prevEl ? ("next" === t.swipeDirection && t.slideTo(null !== f ? f : u + w), "prev" === t.swipeDirection && t.slideTo(null !== g ? g : u)) : d.target === t.navigation.nextEl ? t.slideTo(u + w) : t.slideTo(u)
            }
        }
        function Ye() {
            const e = this,
                {params: t, el: i} = e;
            if (i && 0 === i.offsetWidth)
                return;
            t.breakpoints && e.setBreakpoint();
            const {allowSlideNext: s, allowSlidePrev: n, snapGrid: o} = e,
                a = e.virtual && e.params.virtual.enabled;
            e.allowSlideNext = !0,
            e.allowSlidePrev = !0,
            e.updateSize(),
            e.updateSlides(),
            e.updateSlidesClasses();
            const r = a && t.loop;
            !("auto" === t.slidesPerView || t.slidesPerView > 1) || !e.isEnd || e.isBeginning || e.params.centeredSlides || r ? e.params.loop && !a ? e.slideToLoop(e.realIndex, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0) : e.slideTo(e.slides.length - 1, 0, !1, !0),
            e.autoplay && e.autoplay.running && e.autoplay.paused && (clearTimeout(e.autoplay.resizeTimeout), e.autoplay.resizeTimeout = setTimeout((() => {
                e.autoplay && e.autoplay.running && e.autoplay.paused && e.autoplay.resume()
            }), 500)),
            e.allowSlidePrev = n,
            e.allowSlideNext = s,
            e.params.watchOverflow && o !== e.snapGrid && e.checkOverflow()
        }
        function Xe(e) {
            const t = this;
            t.enabled && (t.allowClick || (t.params.preventClicks && e.preventDefault(), t.params.preventClicksPropagation && t.animating && (e.stopPropagation(), e.stopImmediatePropagation())))
        }
        function Ue() {
            const e = this,
                {wrapperEl: t, rtlTranslate: i, enabled: s} = e;
            if (!s)
                return;
            let n;
            e.previousTranslate = e.translate,
            e.isHorizontal() ? e.translate = -t.scrollLeft : e.translate = -t.scrollTop,
            0 === e.translate && (e.translate = 0),
            e.updateActiveIndex(),
            e.updateSlidesClasses();
            const o = e.maxTranslate() - e.minTranslate();
            n = 0 === o ? 0 : (e.translate - e.minTranslate()) / o,
            n !== e.progress && e.updateProgress(i ? -e.translate : e.translate),
            e.emit("setTranslate", e.translate, !1)
        }
        function Ke(e) {
            Ne(this, e.target),
            this.update()
        }
        let Je = !1;
        function Qe() {}
        const et = (e, t) => {
                const i = ye(),
                    {params: s, el: n, wrapperEl: o, device: a} = e,
                    r = !!s.nested,
                    l = "on" === t ? "addEventListener" : "removeEventListener",
                    d = t;
                n[l]("pointerdown", e.onTouchStart, {
                    passive: !1
                }),
                i[l]("pointermove", e.onTouchMove, {
                    passive: !1,
                    capture: r
                }),
                i[l]("pointerup", e.onTouchEnd, {
                    passive: !0
                }),
                i[l]("pointercancel", e.onTouchEnd, {
                    passive: !0
                }),
                i[l]("pointerout", e.onTouchEnd, {
                    passive: !0
                }),
                i[l]("pointerleave", e.onTouchEnd, {
                    passive: !0
                }),
                (s.preventClicks || s.preventClicksPropagation) && n[l]("click", e.onClick, !0),
                s.cssMode && o[l]("scroll", e.onScroll),
                s.updateOnWindowResize ? e[d](a.ios || a.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", Ye, !0) : e[d]("observerUpdate", Ye, !0),
                n[l]("load", e.onLoad, {
                    capture: !0
                })
            },
            tt = (e, t) => e.grid && t.grid && t.grid.rows > 1,
            it = {
                init: !0,
                direction: "horizontal",
                oneWayMovement: !1,
                touchEventsTarget: "wrapper",
                initialSlide: 0,
                speed: 300,
                cssMode: !1,
                updateOnWindowResize: !0,
                resizeObserver: !0,
                nested: !1,
                createElements: !1,
                enabled: !0,
                focusableElements: "input, select, option, textarea, button, video, label",
                width: null,
                height: null,
                preventInteractionOnTransition: !1,
                userAgent: null,
                url: null,
                edgeSwipeDetection: !1,
                edgeSwipeThreshold: 20,
                autoHeight: !1,
                setWrapperSize: !1,
                virtualTranslate: !1,
                effect: "slide",
                breakpoints: void 0,
                breakpointsBase: "window",
                spaceBetween: 0,
                slidesPerView: 1,
                slidesPerGroup: 1,
                slidesPerGroupSkip: 0,
                slidesPerGroupAuto: !1,
                centeredSlides: !1,
                centeredSlidesBounds: !1,
                slidesOffsetBefore: 0,
                slidesOffsetAfter: 0,
                normalizeSlideIndex: !0,
                centerInsufficientSlides: !1,
                watchOverflow: !0,
                roundLengths: !1,
                touchRatio: 1,
                touchAngle: 45,
                simulateTouch: !0,
                shortSwipes: !0,
                longSwipes: !0,
                longSwipesRatio: .5,
                longSwipesMs: 300,
                followFinger: !0,
                allowTouchMove: !0,
                threshold: 5,
                touchMoveStopPropagation: !1,
                touchStartPreventDefault: !0,
                touchStartForcePreventDefault: !1,
                touchReleaseOnEdges: !1,
                uniqueNavElements: !0,
                resistance: !0,
                resistanceRatio: .85,
                watchSlidesProgress: !1,
                grabCursor: !1,
                preventClicks: !0,
                preventClicksPropagation: !0,
                slideToClickedSlide: !1,
                loop: !1,
                loopedSlides: null,
                loopPreventsSliding: !0,
                rewind: !1,
                allowSlidePrev: !0,
                allowSlideNext: !0,
                swipeHandler: null,
                noSwiping: !0,
                noSwipingClass: "swiper-no-swiping",
                noSwipingSelector: null,
                passiveListeners: !0,
                maxBackfaceHiddenSlides: 10,
                containerModifierClass: "swiper-",
                slideClass: "swiper-slide",
                slideActiveClass: "swiper-slide-active",
                slideVisibleClass: "swiper-slide-visible",
                slideNextClass: "swiper-slide-next",
                slidePrevClass: "swiper-slide-prev",
                wrapperClass: "swiper-wrapper",
                lazyPreloaderClass: "swiper-lazy-preloader",
                lazyPreloadPrevNext: 0,
                runCallbacksOnInit: !0,
                _emitClasses: !1
            };
        function st(e, t) {
            return function(i={}) {
                const s = Object.keys(i)[0],
                    n = i[s];
                "object" == typeof n && null !== n ? (["navigation", "pagination", "scrollbar"].indexOf(s) >= 0 && !0 === e[s] && (e[s] = {
                    auto: !0
                }), s in e && "enabled" in n ? (!0 === e[s] && (e[s] = {
                    enabled: !0
                }), "object" != typeof e[s] || "enabled" in e[s] || (e[s].enabled = !0), e[s] || (e[s] = {
                    enabled: !1
                }), Ce(t, i)) : Ce(t, i)) : Ce(t, i)
            }
        }
        const nt = {
                eventsEmitter: Ze,
                update: Re,
                translate: {
                    getTranslate: function(e=(this.isHorizontal() ? "x" : "y")) {
                        const {params: t, rtlTranslate: i, translate: s, wrapperEl: n} = this;
                        if (t.virtualTranslate)
                            return i ? -s : s;
                        if (t.cssMode)
                            return s;
                        let o = function(e, t="x") {
                            const i = be();
                            let s,
                                n,
                                o;
                            const a = function(e) {
                                const t = be();
                                let i;
                                return t.getComputedStyle && (i = t.getComputedStyle(e, null)), !i && e.currentStyle && (i = e.currentStyle), i || (i = e.style), i
                            }(e);
                            return i.WebKitCSSMatrix ? (n = a.transform || a.webkitTransform, n.split(",").length > 6 && (n = n.split(", ").map((e => e.replace(",", "."))).join(", ")), o = new i.WebKitCSSMatrix("none" === n ? "" : n)) : (o = a.MozTransform || a.OTransform || a.MsTransform || a.msTransform || a.transform || a.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,"), s = o.toString().split(",")), "x" === t && (n = i.WebKitCSSMatrix ? o.m41 : 16 === s.length ? parseFloat(s[12]) : parseFloat(s[4])), "y" === t && (n = i.WebKitCSSMatrix ? o.m42 : 16 === s.length ? parseFloat(s[13]) : parseFloat(s[5])), n || 0
                        }(n, e);
                        return o += this.cssOverflowAdjustment(), i && (o = -o), o || 0
                    },
                    setTranslate: function(e, t) {
                        const i = this,
                            {rtlTranslate: s, params: n, wrapperEl: o, progress: a} = i;
                        let r,
                            l = 0,
                            d = 0;
                        i.isHorizontal() ? l = s ? -e : e : d = e,
                        n.roundLengths && (l = Math.floor(l), d = Math.floor(d)),
                        i.previousTranslate = i.translate,
                        i.translate = i.isHorizontal() ? l : d,
                        n.cssMode ? o[i.isHorizontal() ? "scrollLeft" : "scrollTop"] = i.isHorizontal() ? -l : -d : n.virtualTranslate || (i.isHorizontal() ? l -= i.cssOverflowAdjustment() : d -= i.cssOverflowAdjustment(), o.style.transform = `translate3d(${l}px, ${d}px, 0px)`);
                        const h = i.maxTranslate() - i.minTranslate();
                        r = 0 === h ? 0 : (e - i.minTranslate()) / h,
                        r !== a && i.updateProgress(e),
                        i.emit("setTranslate", i.translate, t)
                    },
                    minTranslate: function() {
                        return -this.snapGrid[0]
                    },
                    maxTranslate: function() {
                        return -this.snapGrid[this.snapGrid.length - 1]
                    },
                    translateTo: function(e=0, t=this.params.speed, i=!0, s=!0, n) {
                        const o = this,
                            {params: a, wrapperEl: r} = o;
                        if (o.animating && a.preventInteractionOnTransition)
                            return !1;
                        const l = o.minTranslate(),
                            d = o.maxTranslate();
                        let h;
                        if (h = s && e > l ? l : s && e < d ? d : e, o.updateProgress(h), a.cssMode) {
                            const e = o.isHorizontal();
                            if (0 === t)
                                r[e ? "scrollLeft" : "scrollTop"] = -h;
                            else {
                                if (!o.support.smoothScroll)
                                    return Ee({
                                        swiper: o,
                                        targetPosition: -h,
                                        side: e ? "left" : "top"
                                    }), !0;
                                r.scrollTo({
                                    [e ? "left" : "top"]: -h,
                                    behavior: "smooth"
                                })
                            }
                            return !0
                        }
                        return 0 === t ? (o.setTransition(0), o.setTranslate(h), i && (o.emit("beforeTransitionStart", t, n), o.emit("transitionEnd"))) : (o.setTransition(t), o.setTranslate(h), i && (o.emit("beforeTransitionStart", t, n), o.emit("transitionStart")), o.animating || (o.animating = !0, o.onTranslateToWrapperTransitionEnd || (o.onTranslateToWrapperTransitionEnd = function(e) {
                            o && !o.destroyed && e.target === this && (o.wrapperEl.removeEventListener("transitionend", o.onTranslateToWrapperTransitionEnd), o.onTranslateToWrapperTransitionEnd = null, delete o.onTranslateToWrapperTransitionEnd, i && o.emit("transitionEnd"))
                        }), o.wrapperEl.addEventListener("transitionend", o.onTranslateToWrapperTransitionEnd))), !0
                    }
                },
                transition: {
                    setTransition: function(e, t) {
                        const i = this;
                        i.params.cssMode || (i.wrapperEl.style.transitionDuration = `${e}ms`),
                        i.emit("setTransition", e, t)
                    },
                    transitionStart: function(e=!0, t) {
                        const i = this,
                            {params: s} = i;
                        s.cssMode || (s.autoHeight && i.updateAutoHeight(), Ve({
                            swiper: i,
                            runCallbacks: e,
                            direction: t,
                            step: "Start"
                        }))
                    },
                    transitionEnd: function(e=!0, t) {
                        const i = this,
                            {params: s} = i;
                        i.animating = !1,
                        s.cssMode || (i.setTransition(0), Ve({
                            swiper: i,
                            runCallbacks: e,
                            direction: t,
                            step: "End"
                        }))
                    }
                },
                slide: $e,
                loop: {
                    loopCreate: function(e) {
                        const t = this,
                            {params: i, slidesEl: s} = t;
                        !i.loop || t.virtual && t.params.virtual.enabled || (Le(s, `.${i.slideClass}, swiper-slide`).forEach(((e, t) => {
                            e.setAttribute("data-swiper-slide-index", t)
                        })), t.loopFix({
                            slideRealIndex: e,
                            direction: i.centeredSlides ? void 0 : "next"
                        }))
                    },
                    loopFix: function({slideRealIndex: e, slideTo: t=!0, direction: i, setTranslate: s, activeSlideIndex: n, byController: o, byMousewheel: a}={}) {
                        const r = this;
                        if (!r.params.loop)
                            return;
                        r.emit("beforeLoopFix");
                        const {slides: l, allowSlidePrev: d, allowSlideNext: h, slidesEl: c, params: p} = r;
                        if (r.allowSlidePrev = !0, r.allowSlideNext = !0, r.virtual && p.virtual.enabled)
                            return t && (p.centeredSlides || 0 !== r.snapIndex ? p.centeredSlides && r.snapIndex < p.slidesPerView ? r.slideTo(r.virtual.slides.length + r.snapIndex, 0, !1, !0) : r.snapIndex === r.snapGrid.length - 1 && r.slideTo(r.virtual.slidesBefore, 0, !1, !0) : r.slideTo(r.virtual.slides.length, 0, !1, !0)), r.allowSlidePrev = d, r.allowSlideNext = h, void r.emit("loopFix");
                        const u = "auto" === p.slidesPerView ? r.slidesPerViewDynamic() : Math.ceil(parseFloat(p.slidesPerView, 10));
                        let m = p.loopedSlides || u;
                        m % p.slidesPerGroup != 0 && (m += p.slidesPerGroup - m % p.slidesPerGroup),
                        r.loopedSlides = m;
                        const f = [],
                            g = [];
                        let v = r.activeIndex;
                        void 0 === n ? n = r.getSlideIndex(r.slides.filter((e => e.classList.contains(p.slideActiveClass)))[0]) : v = n;
                        const w = "next" === i || !i,
                            y = "prev" === i || !i;
                        let _ = 0,
                            b = 0;
                        if (n < m) {
                            _ = Math.max(m - n, p.slidesPerGroup);
                            for (let e = 0; e < m - n; e += 1) {
                                const t = e - Math.floor(e / l.length) * l.length;
                                f.push(l.length - t - 1)
                            }
                        } else if (n > r.slides.length - 2 * m) {
                            b = Math.max(n - (r.slides.length - 2 * m), p.slidesPerGroup);
                            for (let e = 0; e < b; e += 1) {
                                const t = e - Math.floor(e / l.length) * l.length;
                                g.push(t)
                            }
                        }
                        if (y && f.forEach((e => {
                            c.prepend(r.slides[e])
                        })), w && g.forEach((e => {
                            c.append(r.slides[e])
                        })), r.recalcSlides(), p.watchSlidesProgress && r.updateSlidesOffset(), t)
                            if (f.length > 0 && y)
                                if (void 0 === e) {
                                    const e = r.slidesGrid[v],
                                        t = r.slidesGrid[v + _] - e;
                                    a ? r.setTranslate(r.translate - t) : (r.slideTo(v + _, 0, !1, !0), s && (r.touches[r.isHorizontal() ? "startX" : "startY"] += t))
                                } else
                                    s && r.slideToLoop(e, 0, !1, !0);
                            else if (g.length > 0 && w)
                                if (void 0 === e) {
                                    const e = r.slidesGrid[v],
                                        t = r.slidesGrid[v - b] - e;
                                    a ? r.setTranslate(r.translate - t) : (r.slideTo(v - b, 0, !1, !0), s && (r.touches[r.isHorizontal() ? "startX" : "startY"] += t))
                                } else
                                    r.slideToLoop(e, 0, !1, !0);
                        if (r.allowSlidePrev = d, r.allowSlideNext = h, r.controller && r.controller.control && !o) {
                            const t = {
                                slideRealIndex: e,
                                slideTo: !1,
                                direction: i,
                                setTranslate: s,
                                activeSlideIndex: n,
                                byController: !0
                            };
                            Array.isArray(r.controller.control) ? r.controller.control.forEach((e => {
                                !e.destroyed && e.params.loop && e.loopFix(t)
                            })) : r.controller.control instanceof r.constructor && r.controller.control.params.loop && r.controller.control.loopFix(t)
                        }
                        r.emit("loopFix")
                    },
                    loopDestroy: function() {
                        const e = this,
                            {params: t, slidesEl: i} = e;
                        if (!t.loop || e.virtual && e.params.virtual.enabled)
                            return;
                        e.recalcSlides();
                        const s = [];
                        e.slides.forEach((e => {
                            const t = void 0 === e.swiperSlideIndex ? 1 * e.getAttribute("data-swiper-slide-index") : e.swiperSlideIndex;
                            s[t] = e
                        })),
                        e.slides.forEach((e => {
                            e.removeAttribute("data-swiper-slide-index")
                        })),
                        s.forEach((e => {
                            i.append(e)
                        })),
                        e.recalcSlides(),
                        e.slideTo(e.realIndex, 0)
                    }
                },
                grabCursor: {
                    setGrabCursor: function(e) {
                        const t = this;
                        if (!t.params.simulateTouch || t.params.watchOverflow && t.isLocked || t.params.cssMode)
                            return;
                        const i = "container" === t.params.touchEventsTarget ? t.el : t.wrapperEl;
                        t.isElement && (t.__preventObserver__ = !0),
                        i.style.cursor = "move",
                        i.style.cursor = e ? "grabbing" : "grab",
                        t.isElement && requestAnimationFrame((() => {
                            t.__preventObserver__ = !1
                        }))
                    },
                    unsetGrabCursor: function() {
                        const e = this;
                        e.params.watchOverflow && e.isLocked || e.params.cssMode || (e.isElement && (e.__preventObserver__ = !0), e["container" === e.params.touchEventsTarget ? "el" : "wrapperEl"].style.cursor = "", e.isElement && requestAnimationFrame((() => {
                            e.__preventObserver__ = !1
                        })))
                    }
                },
                events: {
                    attachEvents: function() {
                        const e = this,
                            t = ye(),
                            {params: i} = e;
                        e.onTouchStart = qe.bind(e),
                        e.onTouchMove = We.bind(e),
                        e.onTouchEnd = je.bind(e),
                        i.cssMode && (e.onScroll = Ue.bind(e)),
                        e.onClick = Xe.bind(e),
                        e.onLoad = Ke.bind(e),
                        Je || (t.addEventListener("touchstart", Qe), Je = !0),
                        et(e, "on")
                    },
                    detachEvents: function() {
                        et(this, "off")
                    }
                },
                breakpoints: {
                    setBreakpoint: function() {
                        const e = this,
                            {realIndex: t, initialized: i, params: s, el: n} = e,
                            o = s.breakpoints;
                        if (!o || o && 0 === Object.keys(o).length)
                            return;
                        const a = e.getBreakpoint(o, e.params.breakpointsBase, e.el);
                        if (!a || e.currentBreakpoint === a)
                            return;
                        const r = (a in o ? o[a] : void 0) || e.originalParams,
                            l = tt(e, s),
                            d = tt(e, r),
                            h = s.enabled;
                        l && !d ? (n.classList.remove(`${s.containerModifierClass}grid`, `${s.containerModifierClass}grid-column`), e.emitContainerClasses()) : !l && d && (n.classList.add(`${s.containerModifierClass}grid`), (r.grid.fill && "column" === r.grid.fill || !r.grid.fill && "column" === s.grid.fill) && n.classList.add(`${s.containerModifierClass}grid-column`), e.emitContainerClasses()),
                        ["navigation", "pagination", "scrollbar"].forEach((t => {
                            const i = s[t] && s[t].enabled,
                                n = r[t] && r[t].enabled;
                            i && !n && e[t].disable(),
                            !i && n && e[t].enable()
                        }));
                        const c = r.direction && r.direction !== s.direction,
                            p = s.loop && (r.slidesPerView !== s.slidesPerView || c);
                        c && i && e.changeDirection(),
                        Ce(e.params, r);
                        const u = e.params.enabled;
                        Object.assign(e, {
                            allowTouchMove: e.params.allowTouchMove,
                            allowSlideNext: e.params.allowSlideNext,
                            allowSlidePrev: e.params.allowSlidePrev
                        }),
                        h && !u ? e.disable() : !h && u && e.enable(),
                        e.currentBreakpoint = a,
                        e.emit("_beforeBreakpoint", r),
                        p && i && (e.loopDestroy(), e.loopCreate(t), e.updateSlides()),
                        e.emit("breakpoint", r)
                    },
                    getBreakpoint: function(e, t="window", i) {
                        if (!e || "container" === t && !i)
                            return;
                        let s = !1;
                        const n = be(),
                            o = "window" === t ? n.innerHeight : i.clientHeight,
                            a = Object.keys(e).map((e => {
                                if ("string" == typeof e && 0 === e.indexOf("@")) {
                                    const t = parseFloat(e.substr(1));
                                    return {
                                        value: o * t,
                                        point: e
                                    }
                                }
                                return {
                                    value: e,
                                    point: e
                                }
                            }));
                        a.sort(((e, t) => parseInt(e.value, 10) - parseInt(t.value, 10)));
                        for (let e = 0; e < a.length; e += 1) {
                            const {point: o, value: r} = a[e];
                            "window" === t ? n.matchMedia(`(min-width: ${r}px)`).matches && (s = o) : r <= i.clientWidth && (s = o)
                        }
                        return s || "max"
                    }
                },
                checkOverflow: {
                    checkOverflow: function() {
                        const e = this,
                            {isLocked: t, params: i} = e,
                            {slidesOffsetBefore: s} = i;
                        if (s) {
                            const t = e.slides.length - 1,
                                i = e.slidesGrid[t] + e.slidesSizesGrid[t] + 2 * s;
                            e.isLocked = e.size > i
                        } else
                            e.isLocked = 1 === e.snapGrid.length;
                        !0 === i.allowSlideNext && (e.allowSlideNext = !e.isLocked),
                        !0 === i.allowSlidePrev && (e.allowSlidePrev = !e.isLocked),
                        t && t !== e.isLocked && (e.isEnd = !1),
                        t !== e.isLocked && e.emit(e.isLocked ? "lock" : "unlock")
                    }
                },
                classes: {
                    addClasses: function() {
                        const e = this,
                            {classNames: t, params: i, rtl: s, el: n, device: o} = e,
                            a = function(e, t) {
                                const i = [];
                                return e.forEach((e => {
                                    "object" == typeof e ? Object.keys(e).forEach((s => {
                                        e[s] && i.push(t + s)
                                    })) : "string" == typeof e && i.push(t + e)
                                })), i
                            }(["initialized", i.direction, {
                                "free-mode": e.params.freeMode && i.freeMode.enabled
                            }, {
                                autoheight: i.autoHeight
                            }, {
                                rtl: s
                            }, {
                                grid: i.grid && i.grid.rows > 1
                            }, {
                                "grid-column": i.grid && i.grid.rows > 1 && "column" === i.grid.fill
                            }, {
                                android: o.android
                            }, {
                                ios: o.ios
                            }, {
                                "css-mode": i.cssMode
                            }, {
                                centered: i.cssMode && i.centeredSlides
                            }, {
                                "watch-progress": i.watchSlidesProgress
                            }], i.containerModifierClass);
                        t.push(...a),
                        n.classList.add(...t),
                        e.emitContainerClasses()
                    },
                    removeClasses: function() {
                        const {el: e, classNames: t} = this;
                        e.classList.remove(...t),
                        this.emitContainerClasses()
                    }
                }
            },
            ot = {};
        class at {
            constructor(...e)
            {
                let t,
                    i;
                1 === e.length && e[0].constructor && "Object" === Object.prototype.toString.call(e[0]).slice(8, -1) ? i = e[0] : [t, i] = e,
                i || (i = {}),
                i = Ce({}, i),
                t && !i.el && (i.el = t);
                const s = ye();
                if (i.el && "string" == typeof i.el && s.querySelectorAll(i.el).length > 1) {
                    const e = [];
                    return s.querySelectorAll(i.el).forEach((t => {
                        const s = Ce({}, i, {
                            el: t
                        });
                        e.push(new at(s))
                    })), e
                }
                const n = this;
                n.__swiper__ = !0,
                n.support = Fe(),
                n.device = function(e={}) {
                    return De || (De = function({userAgent: e}={}) {
                        const t = Fe(),
                            i = be(),
                            s = i.navigator.platform,
                            n = e || i.navigator.userAgent,
                            o = {
                                ios: !1,
                                android: !1
                            },
                            a = i.screen.width,
                            r = i.screen.height,
                            l = n.match(/(Android);?[\s\/]+([\d.]+)?/);
                        let d = n.match(/(iPad).*OS\s([\d_]+)/);
                        const h = n.match(/(iPod)(.*OS\s([\d_]+))?/),
                            c = !d && n.match(/(iPhone\sOS|iOS)\s([\d_]+)/),
                            p = "Win32" === s;
                        let u = "MacIntel" === s;
                        return !d && u && t.touch && ["1024x1366", "1366x1024", "834x1194", "1194x834", "834x1112", "1112x834", "768x1024", "1024x768", "820x1180", "1180x820", "810x1080", "1080x810"].indexOf(`${a}x${r}`) >= 0 && (d = n.match(/(Version)\/([\d.]+)/), d || (d = [0, 1, "13_0_0"]), u = !1), l && !p && (o.os = "android", o.android = !0), (d || c || h) && (o.os = "ios", o.ios = !0), o
                    }(e)), De
                }({
                    userAgent: i.userAgent
                }),
                n.browser = (Be || (Be = function() {
                    const e = be();
                    let t = !1;
                    function i() {
                        const t = e.navigator.userAgent.toLowerCase();
                        return t.indexOf("safari") >= 0 && t.indexOf("chrome") < 0 && t.indexOf("android") < 0
                    }
                    if (i()) {
                        const i = String(e.navigator.userAgent);
                        if (i.includes("Version/")) {
                            const [e, s] = i.split("Version/")[1].split(" ")[0].split(".").map((e => Number(e)));
                            t = e < 16 || 16 === e && s < 2
                        }
                    }
                    return {
                        isSafari: t || i(),
                        needPerspectiveFix: t,
                        isWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(e.navigator.userAgent)
                    }
                }()), Be),
                n.eventsListeners = {},
                n.eventsAnyListeners = [],
                n.modules = [...n.__modules__],
                i.modules && Array.isArray(i.modules) && n.modules.push(...i.modules);
                const o = {};
                n.modules.forEach((e => {
                    e({
                        params: i,
                        swiper: n,
                        extendParams: st(i, o),
                        on: n.on.bind(n),
                        once: n.once.bind(n),
                        off: n.off.bind(n),
                        emit: n.emit.bind(n)
                    })
                }));
                const a = Ce({}, it, o);
                return n.params = Ce({}, a, ot, i), n.originalParams = Ce({}, n.params), n.passedParams = Ce({}, i), n.params && n.params.on && Object.keys(n.params.on).forEach((e => {
                    n.on(e, n.params.on[e])
                })), n.params && n.params.onAny && n.onAny(n.params.onAny), Object.assign(n, {
                    enabled: n.params.enabled,
                    el: t,
                    classNames: [],
                    slides: [],
                    slidesGrid: [],
                    snapGrid: [],
                    slidesSizesGrid: [],
                    isHorizontal: () => "horizontal" === n.params.direction,
                    isVertical: () => "vertical" === n.params.direction,
                    activeIndex: 0,
                    realIndex: 0,
                    isBeginning: !0,
                    isEnd: !1,
                    translate: 0,
                    previousTranslate: 0,
                    progress: 0,
                    velocity: 0,
                    animating: !1,
                    cssOverflowAdjustment() {
                        return Math.trunc(this.translate / 2 ** 23) * 2 ** 23
                    },
                    allowSlideNext: n.params.allowSlideNext,
                    allowSlidePrev: n.params.allowSlidePrev,
                    touchEventsData: {
                        isTouched: void 0,
                        isMoved: void 0,
                        allowTouchCallbacks: void 0,
                        touchStartTime: void 0,
                        isScrolling: void 0,
                        currentTranslate: void 0,
                        startTranslate: void 0,
                        allowThresholdMove: void 0,
                        focusableElements: n.params.focusableElements,
                        lastClickTime: 0,
                        clickTimeout: void 0,
                        velocities: [],
                        allowMomentumBounce: void 0,
                        startMoving: void 0,
                        evCache: []
                    },
                    allowClick: !0,
                    allowTouchMove: n.params.allowTouchMove,
                    touches: {
                        startX: 0,
                        startY: 0,
                        currentX: 0,
                        currentY: 0,
                        diff: 0
                    },
                    imagesToLoad: [],
                    imagesLoaded: 0
                }), n.emit("_swiper"), n.params.init && n.init(), n
            }
            getSlideIndex(e)
            {
                const {slidesEl: t, params: i} = this,
                    s = Me(Le(t, `.${i.slideClass}, swiper-slide`)[0]);
                return Me(e) - s
            }
            getSlideIndexByData(e)
            {
                return this.getSlideIndex(this.slides.filter((t => 1 * t.getAttribute("data-swiper-slide-index") === e))[0])
            }
            recalcSlides()
            {
                const {slidesEl: e, params: t} = this;
                this.slides = Le(e, `.${t.slideClass}, swiper-slide`)
            }
            enable()
            {
                const e = this;
                e.enabled || (e.enabled = !0, e.params.grabCursor && e.setGrabCursor(), e.emit("enable"))
            }
            disable()
            {
                const e = this;
                e.enabled && (e.enabled = !1, e.params.grabCursor && e.unsetGrabCursor(), e.emit("disable"))
            }
            setProgress(e, t)
            {
                const i = this;
                e = Math.min(Math.max(e, 0), 1);
                const s = i.minTranslate(),
                    n = (i.maxTranslate() - s) * e + s;
                i.translateTo(n, void 0 === t ? 0 : t),
                i.updateActiveIndex(),
                i.updateSlidesClasses()
            }
            emitContainerClasses()
            {
                const e = this;
                if (!e.params._emitClasses || !e.el)
                    return;
                const t = e.el.className.split(" ").filter((t => 0 === t.indexOf("swiper") || 0 === t.indexOf(e.params.containerModifierClass)));
                e.emit("_containerClasses", t.join(" "))
            }
            getSlideClasses(e)
            {
                const t = this;
                return t.destroyed ? "" : e.className.split(" ").filter((e => 0 === e.indexOf("swiper-slide") || 0 === e.indexOf(t.params.slideClass))).join(" ")
            }
            emitSlidesClasses()
            {
                const e = this;
                if (!e.params._emitClasses || !e.el)
                    return;
                const t = [];
                e.slides.forEach((i => {
                    const s = e.getSlideClasses(i);
                    t.push({
                        slideEl: i,
                        classNames: s
                    }),
                    e.emit("_slideClass", i, s)
                })),
                e.emit("_slideClasses", t)
            }
            slidesPerViewDynamic(e="current", t=!1)
            {
                const {params: i, slides: s, slidesGrid: n, slidesSizesGrid: o, size: a, activeIndex: r} = this;
                let l = 1;
                if (i.centeredSlides) {
                    let e,
                        t = s[r].swiperSlideSize;
                    for (let i = r + 1; i < s.length; i += 1)
                        s[i] && !e && (t += s[i].swiperSlideSize, l += 1, t > a && (e = !0));
                    for (let i = r - 1; i >= 0; i -= 1)
                        s[i] && !e && (t += s[i].swiperSlideSize, l += 1, t > a && (e = !0))
                } else if ("current" === e)
                    for (let e = r + 1; e < s.length; e += 1)
                        (t ? n[e] + o[e] - n[r] < a : n[e] - n[r] < a) && (l += 1);
                else
                    for (let e = r - 1; e >= 0; e -= 1)
                        n[r] - n[e] < a && (l += 1);
                return l
            }
            update()
            {
                const e = this;
                if (!e || e.destroyed)
                    return;
                const {snapGrid: t, params: i} = e;
                function s() {
                    const t = e.rtlTranslate ? -1 * e.translate : e.translate,
                        i = Math.min(Math.max(t, e.maxTranslate()), e.minTranslate());
                    e.setTranslate(i),
                    e.updateActiveIndex(),
                    e.updateSlidesClasses()
                }
                let n;
                i.breakpoints && e.setBreakpoint(),
                [...e.el.querySelectorAll('[loading="lazy"]')].forEach((t => {
                    t.complete && Ne(e, t)
                })),
                e.updateSize(),
                e.updateSlides(),
                e.updateProgress(),
                e.updateSlidesClasses(),
                e.params.freeMode && e.params.freeMode.enabled ? (s(), e.params.autoHeight && e.updateAutoHeight()) : (n = ("auto" === e.params.slidesPerView || e.params.slidesPerView > 1) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0), n || s()),
                i.watchOverflow && t !== e.snapGrid && e.checkOverflow(),
                e.emit("update")
            }
            changeDirection(e, t=!0)
            {
                const i = this,
                    s = i.params.direction;
                return e || (e = "horizontal" === s ? "vertical" : "horizontal"), e === s || "horizontal" !== e && "vertical" !== e || (i.el.classList.remove(`${i.params.containerModifierClass}${s}`), i.el.classList.add(`${i.params.containerModifierClass}${e}`), i.emitContainerClasses(), i.params.direction = e, i.slides.forEach((t => {
                    "vertical" === e ? t.style.width = "" : t.style.height = ""
                })), i.emit("changeDirection"), t && i.update()), i
            }
            changeLanguageDirection(e)
            {
                const t = this;
                t.rtl && "rtl" === e || !t.rtl && "ltr" === e || (t.rtl = "rtl" === e, t.rtlTranslate = "horizontal" === t.params.direction && t.rtl, t.rtl ? (t.el.classList.add(`${t.params.containerModifierClass}rtl`), t.el.dir = "rtl") : (t.el.classList.remove(`${t.params.containerModifierClass}rtl`), t.el.dir = "ltr"), t.update())
            }
            mount(e)
            {
                const t = this;
                if (t.mounted)
                    return !0;
                let i = e || t.params.el;
                if ("string" == typeof i && (i = document.querySelector(i)), !i)
                    return !1;
                i.swiper = t,
                i.shadowEl && (t.isElement = !0);
                const s = () => `.${(t.params.wrapperClass || "").trim().split(" ").join(".")}`;
                let n = i && i.shadowRoot && i.shadowRoot.querySelector ? i.shadowRoot.querySelector(s()) : Le(i, s())[0];
                return !n && t.params.createElements && (n = Ie("div", t.params.wrapperClass), i.append(n), Le(i, `.${t.params.slideClass}`).forEach((e => {
                    n.append(e)
                }))), Object.assign(t, {
                    el: i,
                    wrapperEl: n,
                    slidesEl: t.isElement ? i : n,
                    mounted: !0,
                    rtl: "rtl" === i.dir.toLowerCase() || "rtl" === ze(i, "direction"),
                    rtlTranslate: "horizontal" === t.params.direction && ("rtl" === i.dir.toLowerCase() || "rtl" === ze(i, "direction")),
                    wrongRTL: "-webkit-box" === ze(n, "display")
                }), !0
            }
            init(e)
            {
                const t = this;
                return t.initialized || !1 === t.mount(e) || (t.emit("beforeInit"), t.params.breakpoints && t.setBreakpoint(), t.addClasses(), t.updateSize(), t.updateSlides(), t.params.watchOverflow && t.checkOverflow(), t.params.grabCursor && t.enabled && t.setGrabCursor(), t.params.loop && t.virtual && t.params.virtual.enabled ? t.slideTo(t.params.initialSlide + t.virtual.slidesBefore, 0, t.params.runCallbacksOnInit, !1, !0) : t.slideTo(t.params.initialSlide, 0, t.params.runCallbacksOnInit, !1, !0), t.params.loop && t.loopCreate(), t.attachEvents(), [...t.el.querySelectorAll('[loading="lazy"]')].forEach((e => {
                    e.complete ? Ne(t, e) : e.addEventListener("load", (e => {
                        Ne(t, e.target)
                    }))
                })), Ge(t), t.initialized = !0, Ge(t), t.emit("init"), t.emit("afterInit")), t
            }
            destroy(e=!0, t=!0)
            {
                const i = this,
                    {params: s, el: n, wrapperEl: o, slides: a} = i;
                return void 0 === i.params || i.destroyed || (i.emit("beforeDestroy"), i.initialized = !1, i.detachEvents(), s.loop && i.loopDestroy(), t && (i.removeClasses(), n.removeAttribute("style"), o.removeAttribute("style"), a && a.length && a.forEach((e => {
                    e.classList.remove(s.slideVisibleClass, s.slideActiveClass, s.slideNextClass, s.slidePrevClass),
                    e.removeAttribute("style"),
                    e.removeAttribute("data-swiper-slide-index")
                }))), i.emit("destroy"), Object.keys(i.eventsListeners).forEach((e => {
                    i.off(e)
                })), !1 !== e && (i.el.swiper = null, function(e) {
                    const t = e;
                    Object.keys(t).forEach((e => {
                        try {
                            t[e] = null
                        } catch (e) {}
                        try {
                            delete t[e]
                        } catch (e) {}
                    }))
                }(i)), i.destroyed = !0), null
            }
            static extendDefaults(e)
            {
                Ce(ot, e)
            }
            static get extendedDefaults()
            {
                return ot
            }
            static get defaults()
            {
                return it
            }
            static installModule(e)
            {
                at.prototype.__modules__ || (at.prototype.__modules__ = []);
                const t = at.prototype.__modules__;
                "function" == typeof e && t.indexOf(e) < 0 && t.push(e)
            }
            static use(e)
            {
                return Array.isArray(e) ? (e.forEach((e => at.installModule(e))), at) : (at.installModule(e), at)
            }
        }
        Object.keys(nt).forEach((e => {
            Object.keys(nt[e]).forEach((t => {
                at.prototype[t] = nt[e][t]
            }))
        })),
        at.use([function({swiper: e, on: t, emit: i}) {
            const s = be();
            let n = null,
                o = null;
            const a = () => {
                    e && !e.destroyed && e.initialized && (i("beforeResize"), i("resize"))
                },
                r = () => {
                    e && !e.destroyed && e.initialized && i("orientationchange")
                };
            t("init", (() => {
                e.params.resizeObserver && void 0 !== s.ResizeObserver ? e && !e.destroyed && e.initialized && (n = new ResizeObserver((t => {
                    o = s.requestAnimationFrame((() => {
                        const {width: i, height: s} = e;
                        let n = i,
                            o = s;
                        t.forEach((({contentBoxSize: t, contentRect: i, target: s}) => {
                            s && s !== e.el || (n = i ? i.width : (t[0] || t).inlineSize, o = i ? i.height : (t[0] || t).blockSize)
                        })),
                        n === i && o === s || a()
                    }))
                })), n.observe(e.el)) : (s.addEventListener("resize", a), s.addEventListener("orientationchange", r))
            })),
            t("destroy", (() => {
                o && s.cancelAnimationFrame(o),
                n && n.unobserve && e.el && (n.unobserve(e.el), n = null),
                s.removeEventListener("resize", a),
                s.removeEventListener("orientationchange", r)
            }))
        }, function({swiper: e, extendParams: t, on: i, emit: s}) {
            const n = [],
                o = be(),
                a = (t, i={}) => {
                    const a = new (o.MutationObserver || o.WebkitMutationObserver)((t => {
                        if (e.__preventObserver__)
                            return;
                        if (1 === t.length)
                            return void s("observerUpdate", t[0]);
                        const i = function() {
                            s("observerUpdate", t[0])
                        };
                        o.requestAnimationFrame ? o.requestAnimationFrame(i) : o.setTimeout(i, 0)
                    }));
                    a.observe(t, {
                        attributes: void 0 === i.attributes || i.attributes,
                        childList: void 0 === i.childList || i.childList,
                        characterData: void 0 === i.characterData || i.characterData
                    }),
                    n.push(a)
                };
            t({
                observer: !1,
                observeParents: !1,
                observeSlideChildren: !1
            }),
            i("init", (() => {
                if (e.params.observer) {
                    if (e.params.observeParents) {
                        const t = Ae(e.el);
                        for (let e = 0; e < t.length; e += 1)
                            a(t[e])
                    }
                    a(e.el, {
                        childList: e.params.observeSlideChildren
                    }),
                    a(e.wrapperEl, {
                        attributes: !1
                    })
                }
            })),
            i("destroy", (() => {
                n.forEach((e => {
                    e.disconnect()
                })),
                n.splice(0, n.length)
            }))
        }]);
        const rt = at;
        function lt(e, t, i, s) {
            return e.params.createElements && Object.keys(s).forEach((n => {
                if (!i[n] && !0 === i.auto) {
                    let o = Le(e.el, `.${s[n]}`)[0];
                    o || (o = Ie("div", s[n]), o.className = s[n], e.el.append(o)),
                    i[n] = o,
                    t[n] = o
                }
            })), i
        }
        function dt({swiper: e, extendParams: t, on: i, emit: s}) {
            t({
                navigation: {
                    nextEl: null,
                    prevEl: null,
                    hideOnClick: !1,
                    disabledClass: "swiper-button-disabled",
                    hiddenClass: "swiper-button-hidden",
                    lockClass: "swiper-button-lock",
                    navigationDisabledClass: "swiper-navigation-disabled"
                }
            }),
            e.navigation = {
                nextEl: null,
                prevEl: null
            };
            const n = e => (Array.isArray(e) || (e = [e].filter((e => !!e))), e);
            function o(t) {
                let i;
                return t && "string" == typeof t && e.isElement && (i = e.el.shadowRoot.querySelector(t), i) ? i : (t && ("string" == typeof t && (i = [...document.querySelectorAll(t)]), e.params.uniqueNavElements && "string" == typeof t && i.length > 1 && 1 === e.el.querySelectorAll(t).length && (i = e.el.querySelector(t))), t && !i ? t : i)
            }
            function a(t, i) {
                const s = e.params.navigation;
                (t = n(t)).forEach((t => {
                    t && (t.classList[i ? "add" : "remove"](...s.disabledClass.split(" ")), "BUTTON" === t.tagName && (t.disabled = i), e.params.watchOverflow && e.enabled && t.classList[e.isLocked ? "add" : "remove"](s.lockClass))
                }))
            }
            function r() {
                const {nextEl: t, prevEl: i} = e.navigation;
                if (e.params.loop)
                    return a(i, !1), void a(t, !1);
                a(i, e.isBeginning && !e.params.rewind),
                a(t, e.isEnd && !e.params.rewind)
            }
            function l(t) {
                t.preventDefault(),
                (!e.isBeginning || e.params.loop || e.params.rewind) && (e.slidePrev(), s("navigationPrev"))
            }
            function d(t) {
                t.preventDefault(),
                (!e.isEnd || e.params.loop || e.params.rewind) && (e.slideNext(), s("navigationNext"))
            }
            function h() {
                const t = e.params.navigation;
                if (e.params.navigation = lt(e, e.originalParams.navigation, e.params.navigation, {
                    nextEl: "swiper-button-next",
                    prevEl: "swiper-button-prev"
                }), !t.nextEl && !t.prevEl)
                    return;
                let i = o(t.nextEl),
                    s = o(t.prevEl);
                Object.assign(e.navigation, {
                    nextEl: i,
                    prevEl: s
                }),
                i = n(i),
                s = n(s);
                const a = (i, s) => {
                    i && i.addEventListener("click", "next" === s ? d : l),
                    !e.enabled && i && i.classList.add(...t.lockClass.split(" "))
                };
                i.forEach((e => a(e, "next"))),
                s.forEach((e => a(e, "prev")))
            }
            function c() {
                let {nextEl: t, prevEl: i} = e.navigation;
                t = n(t),
                i = n(i);
                const s = (t, i) => {
                    t.removeEventListener("click", "next" === i ? d : l),
                    t.classList.remove(...e.params.navigation.disabledClass.split(" "))
                };
                t.forEach((e => s(e, "next"))),
                i.forEach((e => s(e, "prev")))
            }
            i("init", (() => {
                !1 === e.params.navigation.enabled ? p() : (h(), r())
            })),
            i("toEdge fromEdge lock unlock", (() => {
                r()
            })),
            i("destroy", (() => {
                c()
            })),
            i("enable disable", (() => {
                let {nextEl: t, prevEl: i} = e.navigation;
                t = n(t),
                i = n(i),
                [...t, ...i].filter((e => !!e)).forEach((t => t.classList[e.enabled ? "remove" : "add"](e.params.navigation.lockClass)))
            })),
            i("click", ((t, i) => {
                let {nextEl: o, prevEl: a} = e.navigation;
                o = n(o),
                a = n(a);
                const r = i.target;
                if (e.params.navigation.hideOnClick && !a.includes(r) && !o.includes(r)) {
                    if (e.pagination && e.params.pagination && e.params.pagination.clickable && (e.pagination.el === r || e.pagination.el.contains(r)))
                        return;
                    let t;
                    o.length ? t = o[0].classList.contains(e.params.navigation.hiddenClass) : a.length && (t = a[0].classList.contains(e.params.navigation.hiddenClass)),
                    s(!0 === t ? "navigationShow" : "navigationHide"),
                    [...o, ...a].filter((e => !!e)).forEach((t => t.classList.toggle(e.params.navigation.hiddenClass)))
                }
            }));
            const p = () => {
                e.el.classList.add(...e.params.navigation.navigationDisabledClass.split(" ")),
                c()
            };
            Object.assign(e.navigation, {
                enable: () => {
                    e.el.classList.remove(...e.params.navigation.navigationDisabledClass.split(" ")),
                    h(),
                    r()
                },
                disable: p,
                update: r,
                init: h,
                destroy: c
            })
        }
        function ht(e="") {
            return `.${e.trim().replace(/([\.:!+\/])/g, "\\$1").replace(/ /g, ".")}`
        }
        function ct({swiper: e, extendParams: t, on: i, emit: s}) {
            const n = "swiper-pagination";
            let o;
            t({
                pagination: {
                    el: null,
                    bulletElement: "span",
                    clickable: !1,
                    hideOnClick: !1,
                    renderBullet: null,
                    renderProgressbar: null,
                    renderFraction: null,
                    renderCustom: null,
                    progressbarOpposite: !1,
                    type: "bullets",
                    dynamicBullets: !1,
                    dynamicMainBullets: 1,
                    formatFractionCurrent: e => e,
                    formatFractionTotal: e => e,
                    bulletClass: `${n}-bullet`,
                    bulletActiveClass: `${n}-bullet-active`,
                    modifierClass: `${n}-`,
                    currentClass: `${n}-current`,
                    totalClass: `${n}-total`,
                    hiddenClass: `${n}-hidden`,
                    progressbarFillClass: `${n}-progressbar-fill`,
                    progressbarOppositeClass: `${n}-progressbar-opposite`,
                    clickableClass: `${n}-clickable`,
                    lockClass: `${n}-lock`,
                    horizontalClass: `${n}-horizontal`,
                    verticalClass: `${n}-vertical`,
                    paginationDisabledClass: `${n}-disabled`
                }
            }),
            e.pagination = {
                el: null,
                bullets: []
            };
            let a = 0;
            const r = e => (Array.isArray(e) || (e = [e].filter((e => !!e))), e);
            function l() {
                return !e.params.pagination.el || !e.pagination.el || Array.isArray(e.pagination.el) && 0 === e.pagination.el.length
            }
            function d(t, i) {
                const {bulletActiveClass: s} = e.params.pagination;
                t && (t = t[("prev" === i ? "previous" : "next") + "ElementSibling"]) && (t.classList.add(`${s}-${i}`), (t = t[("prev" === i ? "previous" : "next") + "ElementSibling"]) && t.classList.add(`${s}-${i}-${i}`))
            }
            function h(t) {
                const i = t.target.closest(ht(e.params.pagination.bulletClass));
                if (!i)
                    return;
                t.preventDefault();
                const s = Me(i) * e.params.slidesPerGroup;
                if (e.params.loop) {
                    if (e.realIndex === s)
                        return;
                    const t = e.getSlideIndexByData(s),
                        i = e.getSlideIndexByData(e.realIndex);
                    t > e.slides.length - e.loopedSlides && e.loopFix({
                        direction: t > i ? "next" : "prev",
                        activeSlideIndex: t,
                        slideTo: !1
                    }),
                    e.slideToLoop(s)
                } else
                    e.slideTo(s)
            }
            function c() {
                const t = e.rtl,
                    i = e.params.pagination;
                if (l())
                    return;
                let n,
                    h,
                    c = e.pagination.el;
                c = r(c);
                const p = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length,
                    u = e.params.loop ? Math.ceil(p / e.params.slidesPerGroup) : e.snapGrid.length;
                if (e.params.loop ? (h = e.previousRealIndex || 0, n = e.params.slidesPerGroup > 1 ? Math.floor(e.realIndex / e.params.slidesPerGroup) : e.realIndex) : void 0 !== e.snapIndex ? (n = e.snapIndex, h = e.previousSnapIndex) : (h = e.previousIndex || 0, n = e.activeIndex || 0), "bullets" === i.type && e.pagination.bullets && e.pagination.bullets.length > 0) {
                    const s = e.pagination.bullets;
                    let r,
                        l,
                        p;
                    if (i.dynamicBullets && (o = Oe(s[0], e.isHorizontal() ? "width" : "height", !0), c.forEach((t => {
                        t.style[e.isHorizontal() ? "width" : "height"] = o * (i.dynamicMainBullets + 4) + "px"
                    })), i.dynamicMainBullets > 1 && void 0 !== h && (a += n - (h || 0), a > i.dynamicMainBullets - 1 ? a = i.dynamicMainBullets - 1 : a < 0 && (a = 0)), r = Math.max(n - a, 0), l = r + (Math.min(s.length, i.dynamicMainBullets) - 1), p = (l + r) / 2), s.forEach((e => {
                        const t = [...["", "-next", "-next-next", "-prev", "-prev-prev", "-main"].map((e => `${i.bulletActiveClass}${e}`))].map((e => "string" == typeof e && e.includes(" ") ? e.split(" ") : e)).flat();
                        e.classList.remove(...t)
                    })), c.length > 1)
                        s.forEach((e => {
                            const t = Me(e);
                            t === n && e.classList.add(...i.bulletActiveClass.split(" ")),
                            i.dynamicBullets && (t >= r && t <= l && e.classList.add(...`${i.bulletActiveClass}-main`.split(" ")), t === r && d(e, "prev"), t === l && d(e, "next"))
                        }));
                    else {
                        const e = s[n];
                        if (e && e.classList.add(...i.bulletActiveClass.split(" ")), i.dynamicBullets) {
                            const e = s[r],
                                t = s[l];
                            for (let e = r; e <= l; e += 1)
                                s[e] && s[e].classList.add(...`${i.bulletActiveClass}-main`.split(" "));
                            d(e, "prev"),
                            d(t, "next")
                        }
                    }
                    if (i.dynamicBullets) {
                        const n = Math.min(s.length, i.dynamicMainBullets + 4),
                            a = (o * n - o) / 2 - p * o,
                            r = t ? "right" : "left";
                        s.forEach((t => {
                            t.style[e.isHorizontal() ? r : "top"] = `${a}px`
                        }))
                    }
                }
                c.forEach(((t, o) => {
                    if ("fraction" === i.type && (t.querySelectorAll(ht(i.currentClass)).forEach((e => {
                        e.textContent = i.formatFractionCurrent(n + 1)
                    })), t.querySelectorAll(ht(i.totalClass)).forEach((e => {
                        e.textContent = i.formatFractionTotal(u)
                    }))), "progressbar" === i.type) {
                        let s;
                        s = i.progressbarOpposite ? e.isHorizontal() ? "vertical" : "horizontal" : e.isHorizontal() ? "horizontal" : "vertical";
                        const o = (n + 1) / u;
                        let a = 1,
                            r = 1;
                        "horizontal" === s ? a = o : r = o,
                        t.querySelectorAll(ht(i.progressbarFillClass)).forEach((t => {
                            t.style.transform = `translate3d(0,0,0) scaleX(${a}) scaleY(${r})`,
                            t.style.transitionDuration = `${e.params.speed}ms`
                        }))
                    }
                    "custom" === i.type && i.renderCustom ? (t.innerHTML = i.renderCustom(e, n + 1, u), 0 === o && s("paginationRender", t)) : (0 === o && s("paginationRender", t), s("paginationUpdate", t)),
                    e.params.watchOverflow && e.enabled && t.classList[e.isLocked ? "add" : "remove"](i.lockClass)
                }))
            }
            function p() {
                const t = e.params.pagination;
                if (l())
                    return;
                const i = e.virtual && e.params.virtual.enabled ? e.virtual.slides.length : e.slides.length;
                let n = e.pagination.el;
                n = r(n);
                let o = "";
                if ("bullets" === t.type) {
                    let s = e.params.loop ? Math.ceil(i / e.params.slidesPerGroup) : e.snapGrid.length;
                    e.params.freeMode && e.params.freeMode.enabled && s > i && (s = i);
                    for (let i = 0; i < s; i += 1)
                        t.renderBullet ? o += t.renderBullet.call(e, i, t.bulletClass) : o += `<${t.bulletElement} class="${t.bulletClass}"></${t.bulletElement}>`
                }
                "fraction" === t.type && (o = t.renderFraction ? t.renderFraction.call(e, t.currentClass, t.totalClass) : `<span class="${t.currentClass}"></span> / <span class="${t.totalClass}"></span>`),
                "progressbar" === t.type && (o = t.renderProgressbar ? t.renderProgressbar.call(e, t.progressbarFillClass) : `<span class="${t.progressbarFillClass}"></span>`),
                e.pagination.bullets = [],
                n.forEach((i => {
                    "custom" !== t.type && (i.innerHTML = o || ""),
                    "bullets" === t.type && e.pagination.bullets.push(...i.querySelectorAll(ht(t.bulletClass)))
                })),
                "custom" !== t.type && s("paginationRender", n[0])
            }
            function u() {
                e.params.pagination = lt(e, e.originalParams.pagination, e.params.pagination, {
                    el: "swiper-pagination"
                });
                const t = e.params.pagination;
                if (!t.el)
                    return;
                let i;
                "string" == typeof t.el && e.isElement && (i = e.el.shadowRoot.querySelector(t.el)),
                i || "string" != typeof t.el || (i = [...document.querySelectorAll(t.el)]),
                i || (i = t.el),
                i && 0 !== i.length && (e.params.uniqueNavElements && "string" == typeof t.el && Array.isArray(i) && i.length > 1 && (i = [...e.el.querySelectorAll(t.el)], i.length > 1 && (i = i.filter((t => Ae(t, ".swiper")[0] === e.el))[0])), Array.isArray(i) && 1 === i.length && (i = i[0]), Object.assign(e.pagination, {
                    el: i
                }), i = r(i), i.forEach((i => {
                    "bullets" === t.type && t.clickable && i.classList.add(t.clickableClass),
                    i.classList.add(t.modifierClass + t.type),
                    i.classList.add(e.isHorizontal() ? t.horizontalClass : t.verticalClass),
                    "bullets" === t.type && t.dynamicBullets && (i.classList.add(`${t.modifierClass}${t.type}-dynamic`), a = 0, t.dynamicMainBullets < 1 && (t.dynamicMainBullets = 1)),
                    "progressbar" === t.type && t.progressbarOpposite && i.classList.add(t.progressbarOppositeClass),
                    t.clickable && i.addEventListener("click", h),
                    e.enabled || i.classList.add(t.lockClass)
                })))
            }
            function m() {
                const t = e.params.pagination;
                if (l())
                    return;
                let i = e.pagination.el;
                i && (i = r(i), i.forEach((i => {
                    i.classList.remove(t.hiddenClass),
                    i.classList.remove(t.modifierClass + t.type),
                    i.classList.remove(e.isHorizontal() ? t.horizontalClass : t.verticalClass),
                    t.clickable && i.removeEventListener("click", h)
                }))),
                e.pagination.bullets && e.pagination.bullets.forEach((e => e.classList.remove(...t.bulletActiveClass.split(" "))))
            }
            i("changeDirection", (() => {
                if (!e.pagination || !e.pagination.el)
                    return;
                const t = e.params.pagination;
                let {el: i} = e.pagination;
                i = r(i),
                i.forEach((i => {
                    i.classList.remove(t.horizontalClass, t.verticalClass),
                    i.classList.add(e.isHorizontal() ? t.horizontalClass : t.verticalClass)
                }))
            })),
            i("init", (() => {
                !1 === e.params.pagination.enabled ? f() : (u(), p(), c())
            })),
            i("activeIndexChange", (() => {
                void 0 === e.snapIndex && c()
            })),
            i("snapIndexChange", (() => {
                c()
            })),
            i("snapGridLengthChange", (() => {
                p(),
                c()
            })),
            i("destroy", (() => {
                m()
            })),
            i("enable disable", (() => {
                let {el: t} = e.pagination;
                t && (t = r(t), t.forEach((t => t.classList[e.enabled ? "remove" : "add"](e.params.pagination.lockClass))))
            })),
            i("lock unlock", (() => {
                c()
            })),
            i("click", ((t, i) => {
                const n = i.target;
                let {el: o} = e.pagination;
                if (Array.isArray(o) || (o = [o].filter((e => !!e))), e.params.pagination.el && e.params.pagination.hideOnClick && o && o.length > 0 && !n.classList.contains(e.params.pagination.bulletClass)) {
                    if (e.navigation && (e.navigation.nextEl && n === e.navigation.nextEl || e.navigation.prevEl && n === e.navigation.prevEl))
                        return;
                    const t = o[0].classList.contains(e.params.pagination.hiddenClass);
                    s(!0 === t ? "paginationShow" : "paginationHide"),
                    o.forEach((t => t.classList.toggle(e.params.pagination.hiddenClass)))
                }
            }));
            const f = () => {
                e.el.classList.add(e.params.pagination.paginationDisabledClass);
                let {el: t} = e.pagination;
                t && (t = r(t), t.forEach((t => t.classList.add(e.params.pagination.paginationDisabledClass)))),
                m()
            };
            Object.assign(e.pagination, {
                enable: () => {
                    e.el.classList.remove(e.params.pagination.paginationDisabledClass);
                    let {el: t} = e.pagination;
                    t && (t = r(t), t.forEach((t => t.classList.remove(e.params.pagination.paginationDisabledClass)))),
                    u(),
                    p(),
                    c()
                },
                disable: f,
                render: p,
                update: c,
                init: u,
                destroy: m
            })
        }
        !function() {
            document.querySelectorAll(".button[data-toggle='collapse']").forEach((e => {
                e.addEventListener("click", (e => {
                    e.preventDefault();
                    var t = e.currentTarget.getAttribute("aria-expanded");
                    e.currentTarget.setAttribute("aria-expanded", "true" == t ? "false" : "true");
                    var i = e.currentTarget.getAttribute("data-target"),
                        s = document.querySelectorAll(i);
                    s.length && s[0].classList.toggle("show")
                }))
            }));
            const e = document.getElementById("menu-button"),
                i = document.getElementById("menu-button-2"),
                s = document.getElementById("main-header"),
                n = document.getElementsByClassName("sticky-header");
            let o = "";
            function a(e) {
                e.stopPropagation(),
                s.classList.contains("opened") ? (s.classList.remove("opened"), s.classList.add("closed")) : (s.classList.remove("closed"), s.classList.add("opened"))
            }
            e.addEventListener("click", a),
            i.addEventListener("click", a),
            n.length && (o = n[0].offsetTop, window.onscroll = function() {
                window.pageYOffset > o ? n[0].classList.add("sticky") : n[0].classList.remove("sticky")
            }),
            new (t())(s, {
                tolerance: {
                    down: 10,
                    up: 20
                },
                offset: 200
            }).init(),
            document.querySelectorAll(".js-clickcard").forEach((e => {
                e.style.cursor = "pointer",
                e.addEventListener("click", (t => {
                    let i = e.querySelectorAll("h2 a, h3 a, .card-title a")[0];
                    i !== t.target && i.click()
                }))
            })),
            new ue({
                gallery: ".pswp-gallery",
                children: "a",
                pswpModule: te
            }).init();
            var r = document.getElementById("exp_button"),
                l = document.getElementById("exp_elem_list"),
                d = document.getElementById("multi_select");
            d && document.addEventListener("click", (function(e) {
                d.contains(e.target) ? e.target == r && (e.preventDefault(), r.setAttribute("aria-expanded", "true"), l.classList.toggle("hidden")) : l.classList.add("hidden")
            }));
            var h = document.getElementById("filter_button"),
                c = document.getElementById("filter-nav-network");
            function p() {
                window.dataLayer.push(arguments)
            }
            h && c && h.addEventListener("click", (function(e) {
                var t = "false" === c.getAttribute("aria-hidden");
                c.setAttribute("aria-hidden", t),
                h.setAttribute("aria-expanded", !t)
            })),
            new (fe())((function() {
                var e,
                    t,
                    i,
                    s,
                    n;
                e = window,
                t = document,
                i = "script",
                e.GoogleAnalyticsObject = "ga",
                e.ga = e.ga || function() {
                    (e.ga.q = e.ga.q || []).push(arguments)
                },
                e.ga.l = 1 * new Date,
                s = t.createElement(i),
                n = t.getElementsByTagName(i)[0],
                s.async = 1,
                s.src = "//www.google-analytics.com/analytics.js",
                n.parentNode.insertBefore(s, n),
                ga("create", "UA-35147081-1", "auto"),
                ga("send", "pageview")
            }), !0),
            window.dataLayer = window.dataLayer || [],
            p("js", new Date),
            p("config", "G-0NLT78JMSP"),
            new rt(".swiper", {
                modules: [dt, ct],
                autoHeight: !0,
                direction: "horizontal",
                loop: !0,
                pagination: {
                    el: ".swiper-pagination"
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev"
                },
                scrollbar: {
                    el: ".swiper-scrollbar"
                }
            });
            var u = document.querySelectorAll(".tm-video-thumbnail .play-button");
            u.length && u.forEach((function(e) {
                e.dataset.videoId && e.addEventListener("click", (function(e) {
                    e.preventDefault,
                    function(e) {
                        const t = '<div class="video-modal"><div class="close-button"></div><div class="video-container"><iframe src="https://www.youtube.com/embed/' + e + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div></div>',
                            i = document.getElementById("content");
                        i.insertAdjacentHTML("beforeend", t),
                        i.querySelector(".video-modal").addEventListener("click", (function(e) {
                            this.remove()
                        }))
                    }(this.dataset.videoId)
                }), !1)
            }))
        }()
    })(), s
})()));
//# sourceMappingURL=home.js.map
